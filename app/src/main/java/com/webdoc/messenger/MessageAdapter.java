package com.webdoc.messenger;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sinch.android.rtc.messaging.Message;
import com.webdoc.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

public class MessageAdapter extends BaseAdapter {






    public static final int DIRECTION_INCOMING = 0;

    public static final int DIRECTION_OUTGOING = 1;

    private List<Pair<Message, Integer>> mMessages;
    private List<String> mDate;

    private SimpleDateFormat mFormatterD;
    private SimpleDateFormat mFormatterT;

    private LayoutInflater mInflater;

    Activity a;
    String recipient;
    String sender;



    public MessageAdapter(Activity activity, String recipient, String sender) {
        a = activity;
        this.recipient = recipient;
        this.sender = sender;
        mInflater = activity.getLayoutInflater();
        mMessages = new ArrayList<Pair<Message, Integer>>();
        mDate = new ArrayList<String>();
        mFormatterD = new SimpleDateFormat("E MMM dd yyyy");
        mFormatterT = new SimpleDateFormat("HH:mm");
    }

    public void addMessage(Message message, int direction) {
        //Toast.makeText(a, "testing12345", Toast.LENGTH_SHORT).show();
        if (!message.getTextBody().contains("<System Message From Patient>") && !message.getTextBody().contains("<System Message From Doctor>")) {
            String a = message.getTimestamp()+"";
            mDate.add(a);
            mMessages.add(new Pair(message, direction));
            notifyDataSetChanged();
        } else if (message.getTextBody().contains("<System Message From Doctor>")) {
        } else {
            String m;
            Notification n;
            m = message.getTextBody().replaceAll("<System Message From Patient>", "");

            PendingIntent openApp = PendingIntent.getActivity(a.getApplicationContext(), 0, a.getIntent(),
                    PendingIntent.FLAG_UPDATE_CURRENT);
            n = new Notification.Builder(a.getApplicationContext())
                    .setContentTitle("Appointment Booked!")
                    .setContentText("Your Appointment Has Been Booked")
                    .setStyle(new Notification.BigTextStyle().bigText(m))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(openApp)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_VIBRATE)
                    .build();

            NotificationManager notificationManager = (NotificationManager) a.getSystemService(NOTIFICATION_SERVICE);

            notificationManager.notify(0, n);

            new AudioPlayer(a.getApplicationContext()).playMsg();
        }
    }

    @Override
    public int getCount() {
        return mMessages.size();
    }

    @Override
    public Pair<Message, Integer> getItem(int i) {
        return mMessages.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int i) {
        return mMessages.get(i).second;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        int direction = getItemViewType(i);

        if (convertView == null) {
            int res = 0;
            if (direction == DIRECTION_INCOMING) {
                res = R.layout._message_left;
            } else if (direction == DIRECTION_OUTGOING) {
                res = R.layout._message_right;
            }
            convertView = mInflater.inflate(res, viewGroup, false);
        }

        Message message = mMessages.get(i).first;
        String b = mDate.get(i);
        String name = message.getSenderId();

        TextView txtSender = (TextView) convertView.findViewById(R.id.txtSender);
        TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtTime = (TextView) convertView.findViewById(R.id.txtTime);

        if (name.toLowerCase().equals(recipient.toLowerCase()) || message.getSenderId().toString().toLowerCase().equals(sender.toLowerCase())) {
            String[] parts = b.split(" ");

            txtSender.setText(name);
            txtMessage.setText(message.getTextBody());

            txtDate.setText(parts[0]+" "+parts[1]+" "+parts[2]+" "+parts[5]);

            txtTime.setText(parts[3].substring(0,5));
        }
        return convertView;
    }
}