package com.webdoc.messenger;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.webdoc.AsyncResponse;
import com.webdoc.DBHelper;
import com.webdoc.GetNetwork;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.WCFHandler;
import com.webdoc._Splash;
import com.webdoc.fragment.FragmentDoctorList;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.goncalves.pugnotification.notification.PugNotification;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.webdoc._MainActivity.getToolbarNavigationIcon;


public class Messenger extends BaseActivity implements MessageClientListener {
    View HOME, logoView;

    boolean isBooleanValueForJar = false;
    int[] count = {0};
    Boolean[] makeCall = {false};
    int finalVideoCount, finalAudioCount;

    private static final String TAG = Messenger.class.getSimpleName();

    boolean isCorporate = false;
    String companyName = "", companyID = "";
    JSONArray corp, companyList;
    String cnic, empno;

    private MessageAdapter mMessageAdapter;
    private EditText mTxtTextBody;
    String recipient, me;

    String jarr;
    WCFHandler wcf;
    DBHelper DB;

    TextView upperBar;
    boolean fromNotification = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._messaging);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        DB = new DBHelper(this);
        wcf = new WCFHandler(this);

        findViewById(R.id.message_body).setVisibility(View.VISIBLE);

        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        HOME = getToolbarNavigationIcon(topToolBar);

        // topToolBar.setLogo(R.drawable.logotoolbar);
        // topToolBar.setLogoDescription("TEST");

        /*getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);*/

        //Toast.makeText(getApplicationContext(),getIntent().getStringExtra("Name"),Toast.LENGTH_SHORT).show();

        upperBar = ((TextView) findViewById(R.id.dName));
        upperBar.setText(getIntent().getStringExtra("Name"));

        recipient = getIntent().getStringExtra("UserName");
        if (upperBar.getText().toString().equals("")) {
            fromNotification = true;
            ((TextView) findViewById(R.id.dName)).setText(recipient);
        }
        me = userName();

        //Toast.makeText(getApplicationContext(), "Line # 96", Toast.LENGTH_SHORT).show();

        MySingleton.getInstance().setRecipient(recipient);
        List<Integer> datalist = DB.getAllNotifications_(recipient);

        for (int i = 0; i < datalist.size(); i++) {
            PugNotification.with(getApplicationContext()).cancel(datalist.get(i));
            DB.deleteNotification_(datalist.get(i) + "");
        }

        mTxtTextBody = (EditText) findViewById(R.id.txtTextBody);

        mMessageAdapter = new MessageAdapter(this, recipient, userName());
        ListView messagesList = (ListView) findViewById(R.id.lstMessages);

        loadInfo();
        messagesList.setAdapter(mMessageAdapter);

        if (!MySingleton.getInstance(this).isConnectedButton(this))
            ((Button) findViewById(R.id.m_btnSend)).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        //super.onNewIntent(intent);
        ((TextView) findViewById(R.id.dName)).setText(intent.getStringExtra("Name"));

        recipient = intent.getStringExtra("UserName");

        MySingleton.getInstance().setRecipient(recipient);
        List<Integer> datalist = DB.getAllNotifications_(recipient);

        for (int i = 0; i < datalist.size(); i++) {
            PugNotification.with(getApplicationContext()).cancel(datalist.get(i));
            DB.deleteNotification_(datalist.get(i) + "");
        }
        mMessageAdapter = new MessageAdapter(this, recipient, MySingleton.getInstance().getUserId());
        ListView messagesList = (ListView) findViewById(R.id.lstMessages);

        loadInfo();

        messagesList.setAdapter(mMessageAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        try {
            FragmentDoctorList.getInstance().onViewCreated(false);
        } catch (NullPointerException e) {
            //startActivity(new Intent(this,_MainActivity.class).putExtra("ListAvailable",a));
            startActivity(new Intent(this, _Splash.class).putExtra("check", false));
        }
        finish();
    }

    @Override
    public void onDestroy() {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().removeMessageClientListener(this);
        }
        super.onDestroy();
    }

    @Override
    public void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            //Toast.makeText(getApplicationContext(), "Line # 179", Toast.LENGTH_SHORT).show();
            getSinchServiceInterface().startClient(userName());
        }
        //Toast.makeText(getApplicationContext(), getSinchServiceInterface().getUserName(), Toast.LENGTH_SHORT).show();

        getSinchServiceInterface().addMessageClientListener(this);
        //Toast.makeText(getApplicationContext(),getSinchServiceInterface().getUserName(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected() {
    }

    private void sendMessage() {
        //recipient = getIntent().getStringExtra("UserName");
        final String textBody = mTxtTextBody.getText().toString();
        if (recipient.isEmpty()) {
            Toast.makeText(this, "No recipient added", Toast.LENGTH_SHORT).show();
            return;
        }
        if (textBody.isEmpty()) {
            Toast.makeText(this, "No text message", Toast.LENGTH_SHORT).show();
            return;
        }

        GetNetwork async = new GetNetwork(Messenger.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                Boolean a = (Boolean) result;
                if (!a) {
                    new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                } else {
                    getSinchServiceInterface().setCalleeNAME(recipient);
                    getSinchServiceInterface().sendMessage(recipient, textBody);
                    mTxtTextBody.setText("");
                }
            }
        }, "");
        async.execute();

        /*if (!MySingleton.getInstance().isConnectedNetwork(Messenger.this)) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("No Network")
                    .show();
            return;
        }

        getSinchServiceInterface().sendMessage(recipient, textBody);
        mTxtTextBody.setText("");*/
    }

    @Override
    public void onIncomingMessage(MessageClient client, Message message) {
        if (message.getSenderId().equals(MySingleton.getInstance().getUserId()) && message.getTextBody().equals("<Initialize>"))
            return;
        if (message.getSenderId().equals(recipient)) {
            mMessageAdapter.addMessage(message, MessageAdapter.DIRECTION_INCOMING);

            String id = message.getMessageId();
            String body = message.getTextBody();
            String recipient = message.getRecipientIds() + "";
            recipient = recipient.replace("[", "").replace("]", "");
            String sender = message.getSenderId();
            String timestamp = message.getTimestamp() + "";
            String direction = MessageAdapter.DIRECTION_INCOMING + "";

            DB.insertMessage(id, body, recipient, sender, timestamp, direction);
        }
    }

    @Override
    public void onMessageSent(MessageClient client, Message message, String recipientId) {
        if (recipient.equals(MySingleton.getInstance().getUserId()) && message.getTextBody().equals("<Initialize>"))
            return;
        mMessageAdapter.addMessage(message, MessageAdapter.DIRECTION_OUTGOING);

        String id = message.getMessageId();
        String body = message.getTextBody();
        String recipient = message.getRecipientIds() + "";
        recipient = recipient.replace("[", "").replace("]", "");
        String sender = message.getSenderId();
        String timestamp = message.getTimestamp() + "";
        String direction = MessageAdapter.DIRECTION_OUTGOING + "";

        DB.insertMessage(id, body, recipient, sender, timestamp, direction);
    }

    @Override
    public void onShouldSendPushData(MessageClient client, Message message, List<PushPair> pushPairs) {
        // Left blank intentionally
    }

    @Override
    public void onMessageFailed(MessageClient client, Message message,
                                MessageFailureInfo failureInfo) {
        StringBuilder sb = new StringBuilder();
        sb.append("Sending failed: ")
                .append(failureInfo.getSinchError().getMessage());

        Toast.makeText(this, sb.toString(), Toast.LENGTH_LONG).show();
        Log.d(TAG, sb.toString());
    }

    @Override
    public void onMessageDelivered(MessageClient client, MessageDeliveryInfo deliveryInfo) {
        Log.d(TAG, "onDelivered");
    }

    public void sendMessage(View view) {
        sendMessage();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.video_call, menu);
        if (MySingleton.getInstance().getDoctorStatus().equals("0") || MySingleton.getInstance().getDoctorStatus().equals("2") || fromNotification) {
            MenuItem item = menu.findItem(R.id.action_vCall);
            item.setIcon(null);
            MenuItem item1 = menu.findItem(R.id.action_aCall);
            item1.setIcon(null);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        if (MySingleton.getInstance().getDoctorStatus().equals("0") || MySingleton.getInstance().getDoctorStatus().equals("2")) {
            return true;
        }

        String result;

        GetNetwork async = new GetNetwork(Messenger.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                Boolean a = (Boolean) result;
                if (!a) {
                    new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                } else {
                    /*getSinchServiceInterface().sendMessage(recipient, textBody);
                    mTxtTextBody.setText("");*/
                    //noinspection SimplifiableIfStatement
                    AfterLimitAvailable(id);
                }
            }
        }, "");
        async.execute();

        /*if (!MySingleton.getInstance().isConnectedNetwork(Messenger.this)) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("No Network")
                    .show();
            return true;
        }*/

        /*else
            new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Doctor is busy at the moment")
                    .show();*/

        return super.onOptionsItemSelected(item);

    }

    private void loadInfo() {

        DBHelper DB = new DBHelper(this);
        Message m;

        List<List<String>> messages = DB.getAllMessages();

        for (int i = 0; i < messages.size(); i++) {
            String id = messages.get(i).get(0);
            String body = messages.get(i).get(1);
            String recipient_ = messages.get(i).get(2);
            String sender = messages.get(i).get(3);
            String timestamp = messages.get(i).get(4);

            if ((recipient.equals(recipient_) && (me.equals(sender) || me.equals(recipient_))) || recipient.equals(sender) && (me.equals(sender) || me.equals(recipient_))) {
                m = newMessage(id, body, recipient_, sender, timestamp); //final String id, final String body, final String receipient, final String sender, final String timeStamp
                String a = m.getTimestamp() + "";
                mMessageAdapter.addMessage(m, Integer.parseInt(messages.get(i).get(5)));
            }
        }

    }

    Date finalD;
    SimpleDateFormat mFormatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy", Locale.US);

    public Message newMessage(final String id, final String body, final String receipient, final String sender, final String timeStamp) {

        try {
            Date d1 = (Date) mFormatter.parse(timeStamp);
            finalD = d1;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Message m = new Message() {
            @Override
            public String getMessageId() {
                return id;
            }

            @Override
            public Map<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getTextBody() {
                return body;
            }

            @Override
            public List<String> getRecipientIds() {
                return new ArrayList<>(Arrays.asList(receipient));
            }

            @Override
            public String getSenderId() {
                return sender;
            }

            @Override
            public Date getTimestamp() {
                return finalD;
            }
        };
        return m;
    }

    public void AfterLimitAvailable(int id) {
        if (MySingleton.getInstance().getDoctorStatus().equals("1") || !fromNotification) {

            corp = MySingleton.getInstance().getArray(7);
            companyList = MySingleton.getInstance().getArray(11);

            if (corp == null) {
                onBackPressed();
                return;
            }
            try {
                if (!corp.getString(3).equals("")) {
                    isCorporate = true;
                    companyName = corp.getString(2);
                    cnic = corp.getString(4);
                    empno = corp.getString(3);

                    for (int i = 0; i < companyList.length(); i++) {
                        JSONArray inner = companyList.getJSONArray(i);
                        if (inner.get(1).toString().equals(companyName)) {
                            companyID = inner.get(0).toString();
                            break;
                        }
                    }
                }
            } catch (JSONException e) {
            }

            String function__ = "";

            //getcallcountGeneric/{email}/{type}/{cnic}/{employeNo}/{companyIdd}
            if (id == R.id.action_vCall) {
                if (isCorporate) {
                    function__ = "getcallcountGeneric/" + userName() + "/Video/" + cnic + "/" + empno + "/" + companyID;
                } else {
                    function__ = "getcallcountGeneric/" + userName() + "/Video/" + "-1" + "/" + "-1" + "/" + "-1";
                }

                final GetSingle asyncTask = new GetSingle(Messenger.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String _boolean_ = ((String) result).replace("\"", "").split(",")[0];
                        String _callFromWhere_ = ((String) result).replace("\"", "").split(",")[1];

                        Boolean ans = Boolean.valueOf(_boolean_);
                        DataVideoCall(ans, _callFromWhere_);
                    }
                }, function__ + "__" + "1");
                asyncTask.execute();

            }

            if (id == R.id.action_aCall) {
                if (isCorporate) {
                    function__ = "getcallcountGeneric/" + userName() + "/Voice/" + cnic + "/" + empno + "/" + companyID;
                } else {
                    function__ = "getcallcountGeneric/" + userName() + "/Voice/" + "-1" + "/" + "-1" + "/" + "-1";
                }

                final GetSingle asyncTask = new GetSingle(Messenger.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String _boolean_ = ((String) result).replace("\"", "").split(",")[0];
                        String _callFromWhere_ = ((String) result).replace("\"", "").split(",")[1];

                        Boolean ans = Boolean.valueOf(_boolean_);
                        DataAudioCall(ans, _callFromWhere_);
                    }
                }, function__ + "__" + "1");
                asyncTask.execute();
            }
        }
    }

    public void DataVideoCall(Boolean ans, String callFrom) {
        if (ans) {
            if (getSinchServiceInterface() == null) {
                new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something Went Wrong")
                        .setContentText("please try again in a few moments")
                        .show();
                return;
            }
            getSinchServiceInterface().setCalleeNAME(recipient);
            Call call = getSinchServiceInterface().callUserVideo(recipient);

            String callId = call.getCallId();

            Intent callScreen = new Intent(getApplicationContext(), CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("Name", getIntent().getStringExtra("Name"));
            callScreen.putExtra("isCorporate?", isCorporate);
            callScreen.putExtra("companyName?", companyName);
            callScreen.putExtra("Cnic", cnic);
            callScreen.putExtra("Empno", empno);
            callScreen.putExtra("callFrom?", callFrom);
            startActivity(callScreen);
        } else
            new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Video Call Unsuccessful")
                    .setContentText("No Credit")
                    .show();
    }

    public void DataAudioCall(Boolean ans, String callFrom) {
        if (ans) {
            if (getSinchServiceInterface() == null) {
                new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something Went Wrong")
                        .setContentText("please try again in a few moments")
                        .show();
                return;
            }
            getSinchServiceInterface().setCalleeNAME(recipient);
            Call call = getSinchServiceInterface().callUserVoice(recipient);

            String callId = call.getCallId();

            Intent callScreen = new Intent(getApplicationContext(), CallScreenActivity.class);
            callScreen.putExtra(SinchService.CALL_ID, callId);
            callScreen.putExtra("Name", getIntent().getStringExtra("Name"));
            callScreen.putExtra("isCorporate?", isCorporate);
            callScreen.putExtra("companyName?", companyName);
            callScreen.putExtra("Cnic", cnic);
            callScreen.putExtra("Empno", empno);
            callScreen.putExtra("callFrom?", callFrom);
            startActivity(callScreen);
        } else
            new SweetAlertDialog(Messenger.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Audio Call Unsuccessful")
                    .setContentText("No Credit")
                    .show();
    }


    private String userName() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        return uname;
    }

}
