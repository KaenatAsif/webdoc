package com.webdoc.messenger;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.MissingGCMException;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.sinch.android.rtc.messaging.WritableMessage;
import com.sinch.android.rtc.video.VideoController;
import com.webdoc.AsyncResponse;
import com.webdoc.DBHelper;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._Splash;

import java.util.List;

import br.com.goncalves.pugnotification.notification.PugNotification;

public class SinchService extends Service {

    /*private static final String APP_KEY = "89122446-2044-41ca-b7de-b5fd9cc22708";
    private static final String APP_SECRET = "60jzHnq6yEWAcZytAJSRtg==";
    private static final String ENVIRONMENT = "sandbox.sinch.com";*/

    private static final String APP_KEY = "0799ac52-aa22-4c6e-85a7-53b1c9a041ab";
    private static final String APP_SECRET = "A1j8Zp811EqX/WEtBDB7JA==";
    private static final String ENVIRONMENT = "clientapi.sinch.com";

    public static final String CALL_ID = "CALL_ID";
    private static final String TAG = SinchService.class.getSimpleName();


    private PersistedSettings mSettings;
    private final SinchServiceInterface mServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient = null;
    private String mUserId;
    private StartFailedListener mListener;
    DBHelper DB;

    Intent intent;
    ActivityManager am;
    List<ActivityManager.RunningTaskInfo> taskInfo;
    String classname;
    Boolean _b_;

    void dbMethod(String name) {
        //Toast.makeText(this, result.getMessageResult().toString(), Toast.LENGTH_SHORT).show();

        if (DB.getData(name)) {
            DB.messagesRead(name, "0");
        } else {
            DB.insertPatient(name);
        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new PersistedSettings(getApplicationContext());

        registerReceiver(myReceiver, new IntentFilter("my.awersome.string.name"));
        DB = new DBHelper(this);
        intent = new Intent(getApplicationContext(), Messenger.class);

        am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);


       /* /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/

    }

    private final BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PugNotification.with(getApplicationContext()).cancel(intent.getIntExtra("id", 0));
            DB.deleteNotification_(intent.getIntExtra("id", 0) + "");
        }
    };

    private void start(String userName) {
        if (mSinchClient == null) {
            mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(userName)
                    .applicationKey(APP_KEY)
                    .applicationSecret(APP_SECRET)
                    .environmentHost(ENVIRONMENT).build();

            mSinchClient.setSupportMessaging(true);
            mSinchClient.setSupportCalling(true);

            try {
                mSinchClient.setSupportManagedPush(true);
                mSinchClient.setPushNotificationDisplayName(userName);

            } catch (MissingGCMException e) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
            mSinchClient.startListeningOnActiveConnection();
            mSinchClient.checkManifest();

            mSinchClient.addSinchClientListener(new MySinchClientListener());
            //mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());

            mSinchClient.getMessageClient().addMessageClientListener(new MessageClientListener() {
                @Override
                public void onIncomingMessage(MessageClient messageClient, Message message) {
                    String id = message.getMessageId();
                    String body = message.getTextBody();

                    String recipient = message.getRecipientIds() + "";
                    recipient = recipient.replace("[", "").replace("]", "");

                    String sender = message.getSenderId();
                    String timestamp = message.getTimestamp() + "";

                    String direction = MessageAdapter.DIRECTION_INCOMING + "";

                    showNofication(sender);

                    dbMethod(sender);
                    DB.insertMessage(id, body, recipient, sender, timestamp, direction);
                }

                @Override
                public void onMessageSent(MessageClient messageClient, Message message, String s) {
                    String id = message.getMessageId();
                    String body = message.getTextBody();

                    String recipient = message.getRecipientIds() + "";
                    recipient = recipient.replace("[", "").replace("]", "");

                    String sender = message.getSenderId();
                    String timestamp = message.getTimestamp() + "";

                    String direction = MessageAdapter.DIRECTION_OUTGOING + "";

                    if (recipient.equals(MySingleton.getInstance().getUserId()) && body.equals("<Initialize>"))
                        return;

                    DB.insertMessage(id, body, recipient, sender, timestamp, direction);
                }

                @Override
                public void onMessageFailed(MessageClient messageClient, Message message, MessageFailureInfo messageFailureInfo) {

                }

                @Override
                public void onMessageDelivered(MessageClient messageClient, MessageDeliveryInfo messageDeliveryInfo) {

                }

                @Override
                public void onShouldSendPushData(MessageClient messageClient, Message message, List<PushPair> list) {

                }
            });

            mSinchClient.start();

        }
    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminateGracefully();
        }
        super.onDestroy();
    }

    private void stop(final Context c) {
        if (mSinchClient != null) {
            mSettings.logout();
            MySingleton.getInstance().showLoadingPopup(c, "Logging Out - Please Wait");
            final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    MySingleton.getInstance().dismissLoadingPopup();
                    mSinchClient = null;
                    ((Activity) c).startActivity(new Intent(c, _Splash.class));
                    ((Activity) c).finish();
                }
            }, mSinchClient);
            asyncTask.execute();
            /*mSinchClient.terminate();
            mSinchClient = null;*/
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminateGracefully();
            mSettings.logout();
            mSinchClient = null;
        }
    }


    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mServiceInterface;
    }

    public class SinchServiceInterface extends Binder {

        public void setCalleeNAME(String n) {
            mSinchClient.setPushNotificationDisplayName(n);
        }

        public Call callUserVideo(String userId) {
            return mSinchClient.getCallClient().callUserVideo(userId);
        }

        public Call callUserVoice(String userId) {
            return mSinchClient.getCallClient().callUser(userId);
        }

        public String getUserName() {
            return mUserId;
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {
            mUserId = userName;
            mSettings.setUsername(userName);
            start(userName);
        }

        public void stopClient(Context c) {
            stop(c);
        }

        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public void sendMessage(String recipientUserId, String textBody) {
            SinchService.this.sendMessage(recipientUserId, textBody);
        }

        public void addMessageClientListener(MessageClientListener listener) {
            SinchService.this.addMessageClientListener(listener);
        }

        public void removeMessageClientListener(MessageClientListener listener) {
            SinchService.this.removeMessageClientListener(listener);
        }

        public Call getCall(String callId) {
            return mSinchClient.getCallClient().getCall(callId);
        }

        public VideoController getVideoController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getVideoController();
        }

        public AudioController getAudioController() {
            if (!isStarted()) {
                return null;
            }
            return mSinchClient.getAudioController();
        }

        public NotificationResult relayRemotePushNotificationPayload(Intent intent) {
            if (mSinchClient == null && !mSettings.getUsername().isEmpty()) {
                start(mSettings.getUsername());
            } else if (mSinchClient == null && mSettings.getUsername().isEmpty()) {
                Log.e(TAG, "Can't start a SinchClient as no username is available, unable to relay push.");
                return null;
            }
            return mSinchClient.relayRemotePushNotificationPayload(intent);
        }
    }

    public interface StartFailedListener {

        void onStartFailed(SinchError error);

        void onStarted();
    }

    private class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminateGracefully();
            mSinchClient = null;
            //start();
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                                                      ClientRegistration clientRegistration) {
        }
    }

    /*private class SinchCallClientListener implements CallClientListener {

        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            Log.d(TAG, "Incoming call");
            *//*Intent intent = new Intent(SinchService.this, IncomingCallScreenActivity.class);
            intent.putExtra(CALL_ID, call.getCallId());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            SinchService.this.startActivity(intent);*//*
        }
    }*/

    public void sendMessage(String recipientUserId, String textBody) {
        if (isStarted()) {
            WritableMessage message = new WritableMessage(recipientUserId, textBody);
            mSinchClient.getMessageClient().send(message);
        }
    }

    public void addMessageClientListener(MessageClientListener listener) {
        if (mSinchClient != null) {
            mSinchClient.getMessageClient().addMessageClientListener(listener);
        }
    }

    public void removeMessageClientListener(MessageClientListener listener) {
        if (mSinchClient != null) {
            mSinchClient.getMessageClient().removeMessageClientListener(listener);
        }
    }


    private class PersistedSettings {

        private SharedPreferences mStore;

        private static final String PREF_KEY = "Sinch";

        public PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        public String getUsername() {
            return mStore.getString("Username", "");
        }

        public void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.commit();
        }

        public void logout() {
            SharedPreferences.Editor editor = mStore.edit();
            editor.clear().remove("Username");
            editor.commit();
        }
    }

    void showNofication(String sender) {

        taskInfo = am.getRunningTasks(1);
        classname = taskInfo.get(0).topActivity.getClassName();
        _b_ = classname.equals("com.webdoc.messenger.Messenger");
        if (!_b_ || !MySingleton.getInstance().checkRecipient(sender)) {
            int count = 0;
            String a = sender;

            DB.insertNotification_("", a, "Message");
            count = DB.getNotification_(a, "Message");

            String _name_ = MySingleton.getInstance().getNameFromUserName(sender);

            intent.putExtra("Name", _name_);
            intent.putExtra("UserName", sender);
            intent.putExtra("Status", "0");
            intent.putExtra("MyName", mUserId);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pi = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent stopIntent = new Intent("my.awersome.string.name");
            stopIntent.putExtra("id", count);
            PendingIntent stopPi = PendingIntent.getBroadcast(getApplicationContext(), 4, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            PugNotification.with(getApplicationContext())
                    .load()
                    .identifier(count)
                    .title("Message")
                    .message(sender)
                    .bigTextStyle("From : " + sender)
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.mipmap.ic_launcher)
                    .priority(Notification.PRIORITY_HIGH)
                    .autoCancel(true)
                    .flags(Notification.DEFAULT_VIBRATE)
                    .vibrate(new long[]{100})
                    .dismiss(stopPi)
                    .click(pi)
                    .simple()
                    .build();

            new AudioPlayer(getApplicationContext()).playMsg();

        }
    }

}
