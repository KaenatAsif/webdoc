package com.webdoc.messenger;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;
import com.webdoc.AsyncResponse;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.WCFHandler;
import com.webdoc.__PopupRating;
import com.webdoc.fragment.FragmentDoctorList;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class CallScreenActivity extends BaseActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor proximitySensor;

    Boolean isCorporate;
    String companyName;
    String Cnic, Empno;

    @Override
    public void onSensorChanged(SensorEvent event) {
        // Toast.makeText(this,"AS",Toast.LENGTH_SHORT).show();
        // TODO Auto-generated method stub
        WindowManager.LayoutParams params = this.getWindow().getAttributes();

        if (event.values[0] == 0) {
            //TODO Store original brightness value
            params.screenBrightness = 0.004f;
            this.getWindow().setAttributes(params);
            enableDisableViewGroup((ViewGroup) findViewById(R.id.MAIN).getParent(), false);
            Log.e("onSensorChanged", "NEAR");

        } else {
            //TODO Store original brightness value
            params.screenBrightness = -1.0f;
            this.getWindow().setAttributes(params);
            enableDisableViewGroup((ViewGroup) findViewById(R.id.MAIN).getParent(), true);
            Log.e("onSensorChanged", "FAR");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Toast.makeText(this,"ASD",Toast.LENGTH_SHORT).show();
    }

    static final String TAG = CallScreenActivity.class.getSimpleName();
    static final String ADDED_LISTENER = "addedListener";

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private boolean mAddedListener = false;
    private boolean mVideoViewsAdded = false;

    private TextView mCallDuration;
    private TextView mCallState;
    private TextView mCallerName;

    boolean popuped_ = false;

    AudioController audioController;
    boolean voiceState = true, speakerState = true;
    VideoController vc;
    ImageButton muteButton, endCallButton, TopchangerSpeaker, changerSpeaker, changeCamera;

    int duration_ = 570000;
    boolean NoPressed = false;

    Call _call_;

    private MusicIntentReceiver myReceiver;

    private class MusicIntentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
                int state = intent.getIntExtra("state", -1);
                switch (state) {
                    case 0:
                        if (isBluetoothHeadsetConnected()) {
                            turnOnBluetoothListener();
                            return;
                        }
                        speaker(true);
                        Log.d(TAG, "Headset is unplugged");
                        Toast.makeText(getApplicationContext(), "Headset is unplugged", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        if (isBluetoothHeadsetConnected()) {
                            turnOnBluetoothListener();
                            return;
                        }
                        speaker(false);
                        Log.d(TAG, "Headset is plugged");
                        Toast.makeText(getApplicationContext(), "Headset is plugged", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Log.d(TAG, "I have no idea what the headset state is");
                }
            }
        }
    }

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        //endCall();
    }*/

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._callscreen);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = (TextView) findViewById(R.id.callDuration);
        mCallerName = (TextView) findViewById(R.id.remoteUser);
        mCallState = (TextView) findViewById(R.id.callState);
        muteButton = (ImageButton) findViewById(R.id.muteButton);
        endCallButton = (ImageButton) findViewById(R.id.hangupButton);
        TopchangerSpeaker = (ImageButton) findViewById(R.id.callscreen_topChangeSpeaker);
        changerSpeaker = (ImageButton) findViewById(R.id.changeSpeaker);
        changeCamera = (ImageButton) findViewById(R.id.changeButton);

        endCallButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });
        muteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (voiceState) {
                    /*if (isBluetoothHeadsetConnected()) {
                        turnOnBluetoothListener();
                        return;
                    }*/
                    mute();
                    voiceState = false;
                    muteButton.setImageResource(R.drawable.__unmute);
                } else {
                    /*if (isBluetoothHeadsetConnected()) {
                        turnOffBluetoothListener();
                        return;
                    }*/
                    mute();
                    voiceState = true;
                    muteButton.setImageResource(R.drawable.__mute);
                }
            }
        });

        TopchangerSpeaker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBluetoothHeadsetConnected()) {
                    return;
                }
                if (speakerState) {
                    speaker(false);
                } else {
                    speaker(true);
                }
            }
        });

        changerSpeaker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isBluetoothHeadsetConnected()) {
                    return;
                }
                if (speakerState) {
                    speaker(false);
                } else {
                    speaker(true);
                }
            }
        });

        changeCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                vc.toggleCaptureDevicePosition();
            }
        });
        mCallId = getIntent().getStringExtra(SinchService.CALL_ID);

        myReceiver = new MusicIntentReceiver();
        IntentFilter filter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        registerReceiver(myReceiver, filter);

        if (isBluetoothHeadsetConnected()) {
            turnOnBluetoothListener();
        } else {
            mute(false);
        }

        isCorporate = getIntent().getBooleanExtra("isCorporate?", false);
        companyName = getIntent().getStringExtra("companyName?");
        Cnic = getIntent().getStringExtra("Cnic");
        Empno = getIntent().getStringExtra("Empno");
    }

    void mute() {
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        // get original mode
        int originalMode = audioManager.getMode();
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        // change mute
        boolean state = !audioManager.isMicrophoneMute();
        audioManager.setMicrophoneMute(state);
        // set mode back
        audioManager.setMode(originalMode);
    }

    void mute(boolean a) {
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        // get original mode
        int originalMode = audioManager.getMode();
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        // change mute
        boolean state = !audioManager.isMicrophoneMute();
        audioManager.setMicrophoneMute(a);
        // set mode back
        audioManager.setMode(originalMode);
    }

    /*localAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
 localAudioManager.setMode(0);
 localAudioManager.setBluetoothScoOn(true);
 localAudioManager.startBluetoothSco();
 localAudioManager.setMode(AudioManager.MODE_IN_CALL);*/

    void speaker(boolean state) {
        AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        // get original mode
        int originalMode = audioManager.getMode();
        audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        // change mute
        audioManager.setSpeakerphoneOn(state);
        //audioManager.setMicrophoneMute(state);
        // set mode back
        audioManager.setMode(originalMode);

        if (state) {
            TopchangerSpeaker.setImageResource(R.drawable.__speakeron);
            changerSpeaker.setImageResource(R.drawable.__speakeron);
            speakerState = true;
        } else {
            TopchangerSpeaker.setImageResource(R.drawable.__speakerof);
            changerSpeaker.setImageResource(R.drawable.__speakerof);
            speakerState = false;
        }
    }

    @Override
    public void onServiceConnected() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            if (!mAddedListener) {
                call.addCallListener(new SinchCallListener());
                mAddedListener = true;
            }
        } else {
            Log.e(TAG, "Started with invalid callId, aborting.");
            finish();
        }

        updateUI();
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallerName.setText(call.getRemoteUserId());
            mCallState.setText(call.getState().toString());
            if (call.getState() == CallState.ESTABLISHED) {
                addVideoViews();
            } else {
                if (call.getState() != CallState.INITIATING)
                    muteButton.setVisibility(View.VISIBLE);
                endCallButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mDurationTask.cancel();
        mTimer.cancel();
        removeVideoViews();

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        try {
            if (myReceiver != null) {
                unregisterReceiver(myReceiver);
                myReceiver = null;
            }
        } catch (IllegalArgumentException e) {
            System.out.println("asdasdas");
        }
        if (_call_ != null) {
            if (!_call_.getDetails().isVideoOffered())
                sensorManager.unregisterListener(CallScreenActivity.this);
        }
        MySingleton.getInstance().setInCall(false);

        try {
            FragmentDoctorList.getInstance().onViewCreated(false);
        } catch (NullPointerException e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        updateUI();
    }

    void loop_() {
        if (!popuped_) {
            try {
                duration_ = 600000;
                final SweetAlertDialog pDialog = new SweetAlertDialog(CallScreenActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                pDialog.setTitleText("Alert!");
                pDialog.setContentText("10 min call is ending soon,if you wish to continue click YES.Be sure you will be charged another credit");
                pDialog.setCustomImage(R.drawable.logowebdoc1);
                pDialog.setConfirmText("Yes");
                pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        NoPressed = false;
                    }
                });
                pDialog.setCancelText("No");
                pDialog.showCancelButton(true);
                pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        NoPressed = true;
                    }
                });
                pDialog.show();
                callPopup();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        if (pDialog.isShowing() || NoPressed)
                            endCall();
                    }
                }, 30000);

            } catch (WindowManager.BadTokenException e) {

            }
            //popuped_=true;
        }
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private String formatTimespan(int totalSeconds) {
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallDuration.setText(formatTimespan(call.getDetails().getDuration()));
        }
    }

    private void addVideoViews() {
        if (mVideoViewsAdded || getSinchServiceInterface() == null || !_call_.getDetails().isVideoOffered()) {
            return; //early
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                endCall();
            }
        }, 1800000);

        callPopup();

        vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            RelativeLayout localView = (RelativeLayout) findViewById(R.id.localVideo);
            localView.addView(vc.getLocalView());
            /*localView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vc.toggleCaptureDevicePosition();
                }
            });*/

            LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
            view.addView(vc.getRemoteView());
            mVideoViewsAdded = true;
        }
        /*new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(!mVideoViewsAdded){
                    vc = getSinchServiceInterface().getVideoController();
                    if (vc != null) {
                        RelativeLayout localView = (RelativeLayout) findViewById(R.id.localVideo);
                        localView.addView(vc.getLocalView());
            *//*localView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    vc.toggleCaptureDevicePosition();
                }
            });*//*

                        LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
                        view.addView(vc.getRemoteView());
                        mVideoViewsAdded = true;
                    }
                }
            }
        }, 1000);*/
    }

    private void removeVideoViews() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
            view.removeView(vc.getRemoteView());

            RelativeLayout localView = (RelativeLayout) findViewById(R.id.localVideo);
            localView.removeView(vc.getLocalView());
            mVideoViewsAdded = false;
        }
    }

    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            if (isBluetoothHeadsetConnected()) {
                turnOffBluetoothListener();
            } else {
                setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            }
            String endMsg = "Call ended: " + call.getDetails().toString();
            //Toast.makeText(CallScreenActivity.this, endMsg, Toast.LENGTH_LONG).show();

            boolean isVideo = call.getDetails().isVideoOffered();

            int min = Integer.parseInt(mCallDuration.getText().toString().split(":")[0]);

            call.hangup();
            endCall();

            MySingleton.getInstance().setDoctorStatus("2");

            if (cause.toString().equals("HUNG_UP")) {
                WCFHandler wcf = new WCFHandler(getApplicationContext());
                String result;
                if (isVideo) {
                    if (min == 0) {
                    } else {
                        min = min / 10;
                        min++;
                        String function__ = "";
                        if (isCorporate) {
                            JSONArray companyList = MySingleton.getInstance().getArray(11);
                            String CompanyID = "";
                            try {
                                for (int i = 0; i < companyList.length(); i++) {
                                    JSONArray inner = companyList.getJSONArray(i);
                                    if (inner.get(1).toString().equals(companyName)) {
                                        CompanyID = inner.get(0).toString();
                                    }
                                }
                            } catch (JSONException e) {
                            }

                            //updatecallcountGeneric/{email}/{voice}/{video}/{cnic}/{employeNo}/{companyIdd}
                            function__ = "UpdateCallCountGeneric/" + MySingleton.getInstance().getUserId() + "/" + 0 + "/" + min + "/" + Cnic + "/" + Empno + "/" + CompanyID + "/" + getIntent().getStringExtra("callFrom?");
                        } else {

                            function__ = "UpdateCallCountGeneric/" + MySingleton.getInstance().getUserId() + "/" + 0 + "/" + min + "/" + "-1" + "/" + "-1" + "/" + "-1/" + getIntent().getStringExtra("callFrom?");
                        }
                        final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {
                                String resultkl = "";
                            }
                        }, Uri.encode(function__) + "__" + "1");
                        asyncTask.execute();
                    }
                } else {
                    if (min == 0) {
                    } else {
                        min = min / 10;
                        min++;
                        String function__ = "";
                        if (isCorporate) {
                            JSONArray companyList = MySingleton.getInstance().getArray(11);
                            String CompanyID = "";
                            try {
                                for (int i = 0; i < companyList.length(); i++) {
                                    JSONArray inner = companyList.getJSONArray(i);
                                    if (inner.get(1).toString().equals(companyName)) {
                                        CompanyID = inner.get(0).toString();
                                    }
                                }
                            } catch (JSONException e) {
                            }

                            //updatecallcountGeneric/{email}/{voice}/{video}/{cnic}/{employeNo}/{companyIdd}

                            function__ = "UpdateCallCountGeneric/" + MySingleton.getInstance().getUserId() + "/" + min + "/" + 0 + "/" + Cnic + "/" + Empno + "/" + CompanyID + getIntent().getStringExtra("callFrom?");
                        } else
                            function__ = "UpdateCallCountGeneric/" + MySingleton.getInstance().getUserId() + "/" + min + "/" + 0 + "/" + "-1" + "/" + "-1" + "/" + "-1" + getIntent().getStringExtra("callFrom?");
                        final GetProfile asyncTask = new GetProfile(getApplicationContext(), new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {
                                String resultkl = "";
                            }
                        }, Uri.encode(function__) + "__" + "1");
                        asyncTask.execute();
                    }
                }

                //endCall();

                FragmentDoctorList.changeStatus(mCallerName.getText() + "");

                Intent i_ = new Intent(getApplicationContext(), __PopupRating.class);
                i_.putExtra("name", getIntent().getStringExtra("Name"));
                i_.putExtra("DName", mCallerName.getText());
                startActivity(i_);
            } else if (cause.toString().equals("CANCELED") || cause.toString().equals("NO_ANSWER") || cause.toString().equals("DENIED")) {
                getSinchServiceInterface().sendMessage(mCallerName.getText() + "", "You missed a call from\n" + getSinchServiceInterface().getUserName());
            } else if (cause.toString().equals("FAILURE")) {
                new SweetAlertDialog(CallScreenActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something went wrong!")
                        .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                "If the problem persists, please contact us at support@webdoc.com.pk")
                        .show();
            }
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
            _call_ = call;
            mAudioPlayer.stopProgressTone();
            mCallState.setText(call.getState().toString());
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

            /*audioController = getSinchServiceInterface().getAudioController();
            audioController.enableSpeaker();*/

            /*AudioManager audiomgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audiomgr.setMode(AudioManager.STREAM_VOICE_CALL);
            audiomgr.setSpeakerphoneOn(true);*/


            Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());

            if (call.getDetails().isVideoOffered()) {
                muteButton.setVisibility(View.VISIBLE);
                changeCamera.setVisibility(View.VISIBLE);
                TopchangerSpeaker.setVisibility(View.VISIBLE);
            } else {
                sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
                proximitySensor = sensorManager
                        .getDefaultSensor(Sensor.TYPE_PROXIMITY);
                sensorManager.registerListener(CallScreenActivity.this, proximitySensor,
                        SensorManager.SENSOR_DELAY_NORMAL);

                muteButton.setVisibility(View.VISIBLE);
                changerSpeaker.setVisibility(View.VISIBLE);
                // speaker(false);
            }
            MySingleton.getInstance().setInCall(true);
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
            Log.d(TAG, "Video track added");
            addVideoViews();
        }

        @Override
        public void onVideoTrackPaused(Call call) {

        }

        @Override
        public void onVideoTrackResumed(Call call) {
        }
    }

    void callPopup() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                loop_();
            }
        }, duration_);
    }

    public void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

    public void turnOnBluetoothListener() {
        AudioManager localAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        localAudioManager.setMode(0);
        localAudioManager.setBluetoothScoOn(true);
        localAudioManager.startBluetoothSco();
        localAudioManager.setMode(AudioManager.MODE_IN_CALL);
    }

    public void turnOffBluetoothListener() {
        AudioManager localAudioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        localAudioManager.setBluetoothScoOn(false);
        localAudioManager.stopBluetoothSco();
        localAudioManager.setMode(AudioManager.MODE_NORMAL);
    }

    public static boolean isBluetoothHeadsetConnected() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()
                && mBluetoothAdapter.getProfileConnectionState(BluetoothHeadset.HEADSET) == BluetoothHeadset.STATE_CONNECTED;
    }
}
