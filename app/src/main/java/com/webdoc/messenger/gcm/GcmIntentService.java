package com.webdoc.messenger.gcm;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.SinchHelpers;
import com.sinch.android.rtc.messaging.Message;
import com.sinch.android.rtc.messaging.MessageClient;
import com.sinch.android.rtc.messaging.MessageClientListener;
import com.sinch.android.rtc.messaging.MessageDeliveryInfo;
import com.sinch.android.rtc.messaging.MessageFailureInfo;
import com.webdoc.DBHelper;
import com.webdoc.messenger.Messenger;
import com.webdoc.messenger.SinchService;

import java.util.List;

public class GcmIntentService extends IntentService implements ServiceConnection, MessageClientListener {

    private Intent mIntent;

    private SinchService.SinchServiceInterface mSinchServiceInterface;

    private static SinchService.SinchServiceInterface mSinchServiceInterfacee;

    DBHelper DB;

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (SinchHelpers.isSinchPushIntent(intent)) {
            mIntent = intent;
            connectToService();
        } else {
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private void connectToService() {
        getApplicationContext().bindService(new Intent(this, SinchService.class), this, Context.BIND_AUTO_CREATE);
        //SinchService.SinchServiceInterface mSinchServiceInterface
    }

    protected void onServiceConnected() {
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
        // for subclasses
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (mIntent == null) {
            return;


        }

        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }

        /*if (MySingleton.getInstance().getBoolean()) {
            //mSinchServiceInterface.removeMessageClientListener(this);
            mSinchServiceInterface.addMessageClientListener(this);
            MySingleton.getInstance().setBoolean(false);
        }*/

        DB = new DBHelper(this);
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo;
        String classname;
        Boolean _b_;

        Notification n;
        Intent intent = new Intent(getApplicationContext(), Messenger.class);

        if (SinchHelpers.isSinchPushIntent(mIntent)) {
            SinchService.SinchServiceInterface sinchService = (SinchService.SinchServiceInterface) iBinder;
            if (sinchService != null) {
                NotificationResult result = sinchService.relayRemotePushNotificationPayload(mIntent);
                if (result == null)
                    return;
                // handle result, e.g. show a notification or similar
                System.out.println("******" + result);
                System.out.println("******" + result.getDisplayName());
                /*if (result.isCall()) {
                    System.out.println("******" + result.getCallResult());

                    if (result.getCallResult().isVideoOffered())
                        n = new Notification.Builder(getApplicationContext())
                                .setContentTitle("Incoming Video Call")
                                .setContentText("From " + result.getCallResult().getRemoteUserId())
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setPriority(Notification.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_VIBRATE)
                                .build();
                    else
                        n = new Notification.Builder(getApplicationContext())
                                .setContentTitle("Incoming Voice Call")
                                .setContentText("From " + result.getCallResult().getRemoteUserId())
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setPriority(Notification.PRIORITY_HIGH)
                                .setDefaults(Notification.DEFAULT_VIBRATE)
                                .build();

                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                    notificationManager.notify(0, n);
                }*/
                if (result.isMessage()) {
                    /*taskInfo = am.getRunningTasks(1);
                    classname = taskInfo.get(0).topActivity.getClassName();
                    _b_ = classname.equals("com.webdoc.messenger.Messenger");
                    if (!_b_ || !MySingleton.getInstance().checkRecipient(result.getMessageResult().getSenderId())) {
                        int count = 0;
                        String a = result.getMessageResult().getSenderId();

                        DB.insertNotification_("", a, "Message");
                        count = DB.getNotification_(a, "Message");

                        intent.putExtra("Name", MySingleton.getInstance().getNameFromUserName(result.getMessageResult().getSenderId()));
                        intent.putExtra("UserName", result.getMessageResult().getSenderId());
                        intent.putExtra("Status", "0");
                        intent.putExtra("MyName", MySingleton.getInstance().getUserId());
                        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        PendingIntent pi = PendingIntent.getActivity(this, 10, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                        Intent stopIntent = new Intent("my.awersome.string.name");
                        stopIntent.putExtra("id", count);
                        PendingIntent stopPi = PendingIntent.getBroadcast(getApplicationContext(), 4, stopIntent, PendingIntent.FLAG_CANCEL_CURRENT);

                        PugNotification.with(getApplicationContext())
                                .load()
                                .identifier(count)
                                .title("Message")
                                .message(result.getMessageResult().getSenderId())
                                .bigTextStyle("From : " + result.getMessageResult().getSenderId())
                                .smallIcon(R.mipmap.ic_launcher)
                                .largeIcon(R.mipmap.ic_launcher)
                                .priority(Notification.PRIORITY_HIGH)
                                .autoCancel(true)
                                .flags(Notification.DEFAULT_VIBRATE)
                                .vibrate(new long[]{100})
                                .dismiss(stopPi)
                                .click(pi)
                                .simple()
                                .build();

                        new AudioPlayer(getApplicationContext()).playMsg();*/

                    }

                }

                GcmBroadcastReceiver.completeWakefulIntent(mIntent);
                mIntent = null;
            }
        }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
    }

    @Override
    public void onIncomingMessage(MessageClient messageClient, Message message) {
        /*String id = message.getMessageId();
        String body = message.getTextBody();

        String recipient = message.getRecipientIds() + "";
        recipient = recipient.replace("[", "").replace("]", "");

        String sender = message.getSenderId();
        String timestamp = message.getTimestamp() + "";

        String direction = MessageAdapter.DIRECTION_INCOMING + "";
        if(recipient.equals(MySingleton.getInstance().getUserId()) && body.equals("<Initialize>"))
            return;

        dbMethod(sender);
        DB.insertMessage(id, body, recipient, sender, timestamp, direction);*/
    }

    @Override
    public void onMessageSent(MessageClient messageClient, Message message, String s) {
        /*String id = message.getMessageId();
        String body = message.getTextBody();

        String recipient = message.getRecipientIds() + "";
        recipient = recipient.replace("[", "").replace("]", "");

        String sender = message.getSenderId();
        String timestamp = message.getTimestamp() + "";

        String direction = MessageAdapter.DIRECTION_OUTGOING + "";

        if(recipient.equals(MySingleton.getInstance().getUserId()) && body.equals("<Initialize>"))
            return;

        DB.insertMessage(id, body, recipient, sender, timestamp, direction);*/
    }

    @Override
    public void onMessageFailed(MessageClient messageClient, Message message, MessageFailureInfo messageFailureInfo) {

    }

    @Override
    public void onMessageDelivered(MessageClient messageClient, MessageDeliveryInfo messageDeliveryInfo) {

    }

    @Override
    public void onShouldSendPushData(MessageClient messageClient, Message message, List<PushPair> list) {

    }
}