package com.webdoc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupEditAdd extends AppCompatActivity {

    DatePicker Date;
    Spinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_edit_add);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        Date = (DatePicker) findViewById(R.id.datePicker);
        spinner = (Spinner) findViewById(R.id.spinner_hv);

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        if (getIntent().getStringExtra("Title").equals("Medication Description"))
        {
            getWindow().setLayout((int) (width * .9), (int) (height * .8));
            ((RelativeLayout) findViewById(R.id.hiddenView)).setVisibility(View.VISIBLE);


                if (getIntent().getStringExtra("Date") != null && getIntent().getStringExtra("Status") != null) {
                String[] ddaattee = getIntent().getStringExtra("Date").split(" ");


                String D = ddaattee[0];

                D = D.replace("Jan", "1");
                D = D.replace("Feb", "2");
                D = D.replace("Mar", "3");
                D = D.replace("Apr", "4");
                D = D.replace("May", "5");
                D = D.replace("Jun", "6");
                D = D.replace("Jul", "7");
                D = D.replace("Aug", "8");
                D = D.replace("Sep", "9");
                D = D.replace("Oct", "10");
                D = D.replace("Nov", "11");
                D = D.replace("Dec", "12");


                if (ddaattee.length == 4)
                    Date.updateDate(Integer.valueOf(ddaattee[3]), Integer.valueOf(D) - 1, Integer.valueOf(ddaattee[2]));
                else if (ddaattee.length == 3)
                    Date.updateDate(Integer.valueOf(ddaattee[2]), Integer.valueOf(D) - 1, Integer.valueOf(ddaattee[1]));
            }
        } else {
            getWindow().setLayout((int) (width * .9), (int) (height * .5));
        }



        if (MySingleton.getInstance().getLang()) {
/*            ((TextView) findViewById(R.id.popup_title)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_title)).setText("طبی حالت");*/
            ((TextView) findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) findViewById(R.id.reltext)).setText("موجودہ سٹیٹس");
            ((TextView) findViewById(R.id.popup_title)).setText(getIntent().getStringExtra("Title"));
            ((Button) findViewById(R.id.clinicType)).setTypeface(tf);
            ((Button) findViewById(R.id.clinicType)).setText("سیو");

        } else {
            ((TextView) findViewById(R.id.popup_title)).setText(getIntent().getStringExtra("Title"));
        }
        ((EditText) findViewById(R.id.editText_Condition_Edit)).setText(getIntent().getStringExtra("Value"));

        String statuss = getIntent().getStringExtra("Status");
        if (statuss != null) {
            statuss = statuss.replace("Active", "1");
            statuss = statuss.replace("On Hold", "2");
            statuss = statuss.replace("Prior History", "3");
            statuss = statuss.replace("No Longer 1", "4");
        }

        try {
            spinner.setSelection(Integer.parseInt(statuss));
        } catch (NumberFormatException ex) {
            spinner.setSelection(0);
        }

    }

    public void editCondition(View view) {

        String ans = ((EditText) findViewById(R.id.editText_Condition_Edit)).getText() + "";
        ans = Uri.encode(ans.trim().replaceAll("\n", " "));

        if (spinner.getSelectedItemPosition() == 0) {
            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Please select a status")
                    .show();
            return;
        }
        String status = Uri.encode(spinner.getSelectedItem() + "");

        String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        int D = Date.getDayOfMonth();
        int Mon = Date.getMonth();
        int Y = Date.getYear();

        String date = Y + "-" + Months[Mon] + "-" + D;

        WCFHandler wcf = new WCFHandler(this);

        String id = getIntent().getStringExtra("Id");
        JSONArray jarr;

        final Intent returnIntent = new Intent();

        if (getIntent().getStringExtra("Title").equals("Medical Conditions Description")) {
            if (ans.equals("")) {
                new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("Empty Description")
                        .show();
                return;
            }
            if (id != null) {
                String function__ = "EditConditionPatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + ans + "/" + status;

                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(1, jarr);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                String function__ = "AddConditionPatient/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(1, jarr);
                            setResult(50, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            }
        } else if (getIntent().getStringExtra("Title").equals("Allergies Description")) {
            if (ans.equals("")) {
                new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("Empty Description")
                        .show();
                return;
            }
            if (id != null) {
                String function__ = "EditAllergyPatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(2, jarr);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                String function__ = "AddAllergyPatient/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(2, jarr);
                            setResult(50, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            }
        } else if (getIntent().getStringExtra("Title").equals("Medication Description")) {
            String result = "";
            if (ans.equals("")) {
                result = "Empty Description";
            }
            if (status.equals("- Select Status -")) {
                if (ans.equals(""))
                    result += " and ";
                result += "No Status Selected";
            }
            if (!result.equals("")) {
                new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(result)
                        .show();
                return;
            }
            if (id != null) {
                String function__ = "EditMedicationPatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + ans + "/" + date + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(3, jarr);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                String function__ = "AddMedicationPatient/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + date + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(3, jarr);
                            setResult(50, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            }
        } else if (getIntent().getStringExtra("Title").equals("Immunizations Description")) {
            if (ans.equals("")) {
                new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("Empty Description")
                        .show();
                return;
            }
            if (id != null) {
                String function__ = "EditImmunizationPatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(4, jarr);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                String function__ = "AddImmunizationPatient/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(4, jarr);
                            setResult(50, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();

            }
        } else if (getIntent().getStringExtra("Title").equals("Medical Procedure Description")) {
            if (ans.equals("")) {
                new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("Empty Description")
                        .show();
                return;
            }
            if (id != null) {
                String function__ = "EditProceduresPatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(5, jarr);
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                String function__ = "AddProceduresPatient/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + status;
                final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(__PopupEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateArray(5, jarr);
                            setResult(50, returnIntent);
                            finish();
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            }
        }
    }

}
