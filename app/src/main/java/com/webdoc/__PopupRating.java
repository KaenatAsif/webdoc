package com.webdoc;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupRating extends AppCompatActivity {

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    TextView name;
    RatingBar ratbar;
    EditText comment;

    String DoctorEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_rating);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .75), (int) (height * .55));



        name = (TextView) findViewById(R.id._rating_name);
        ratbar = (RatingBar) findViewById(R.id._rating_rating);
        comment = (EditText) findViewById(R.id.editText_Comment);

        ratbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if(v<=3)
                    comment.setVisibility(View.VISIBLE);
                else
                    comment.setVisibility(View.GONE);
            }
        });

        name.setText(getIntent().getStringExtra("name"));
        double Rating = MySingleton.getInstance().getDoctorRating();
        //ratbar.setProgress(Rating);
        ratbar.setRating((float)Rating);

        DoctorEmail = getIntent().getStringExtra("DName");
    }

    public void submit(View view) {

        float current_rating=ratbar.getRating();
        String Comment = Uri.encode(comment.getText()+"");
        if(Comment.equals(""))
            Comment = "-";
        WCFHandler wcf = new WCFHandler(this);
        String function__ = "SaveFeedBack/"+DoctorEmail+"/"+MySingleton.getInstance().getUserId()+"/"+Comment+"/"+current_rating;
        final GetSingle asyncTask = new GetSingle(__PopupRating.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                if(result.equals("\"Success\""))
                {
                    new SweetAlertDialog(__PopupRating.this,SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Sucess")
                            .setContentText("Thankyou for your precious Feedback")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    MySingleton.getInstance().setDoctorStatus("2");
                                    sweetAlertDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .show();

                }
                else{
                    new SweetAlertDialog(__PopupRating.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("Please try again")
                            .show();
                }
            }
        }, function__ + "__" + "1");
        asyncTask.execute();
    }
}
