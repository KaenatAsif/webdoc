package com.webdoc;

import android.content.Context;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.SinchClient;

import org.json.JSONArray;

/**
 * Created by faheem on 03/08/2017.
 */

public class GetProfile extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a = "";
    SinchClient sc;
    Context Contextt_;
    Boolean profile;


    public GetProfile(Context c, AsyncResponse asyncResponse, String a) {
        Contextt_ = c;
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    public GetProfile(Context c, AsyncResponse asyncResponse, SinchClient sc) {
        Contextt_ = c;
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.sc = sc;
    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////



        //loading = ProgressDialog.show(getApplicationContext(),"Fetching...","Profile...",false,false);

    }


    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        //loading.dismiss();
        delegate.processFinish(s);
    }

    @Override
    protected Object doInBackground(Void... params) {
        WCFHandler wcf = new WCFHandler(Contextt_);
        if (a.equals("")) {
            //sc.terminateGracefully();
            return "";
        } else {
            String[] parts = a.split("__");

            if (parts[1].equals("1")) {
                String s = wcf.GetJsonResult1(parts[0]);
                return s;
            } else {
                JSONArray s = wcf.GetJsonResult(parts[0]);
                return s;
            }
        }
    }
}