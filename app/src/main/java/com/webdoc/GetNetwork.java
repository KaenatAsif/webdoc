package com.webdoc;

import android.content.Context;

import com.batch.android.Batch;
import com.batch.android.Config;

import java.io.IOException;

/**
 * Created by faheem on 24/08/2017.
 */

public class GetNetwork extends android.os.AsyncTask<Void, Void, Object> {
    //ProgressDialog loading;
    public AsyncResponse delegate = null;//Call back interface
    String a;
    Context Contextt_;

    public GetNetwork(Context c, AsyncResponse asyncResponse, String a) {
        this.Contextt_ = c;
        delegate = asyncResponse;//Assigning call back interfacethrough constructor
        this.a = a;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        MySingleton.getInstance().showLoadingPopup(Contextt_,"Please Wait");

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

    }

    @Override
    protected void onPostExecute(Object s) {
        super.onPostExecute(s);
        MySingleton.getInstance().dismissLoadingPopup();
        delegate.processFinish(s);
    }

    @Override
    protected Object doInBackground(Void... params) {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 172.217.9.142");
            int exitValue = ipProcess.waitFor();
            if (exitValue == 0){
                MySingleton.getInstance().dismissLoadingPopup();
                return true;}
            else {
                MySingleton.getInstance().dismissLoadingPopup();
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}