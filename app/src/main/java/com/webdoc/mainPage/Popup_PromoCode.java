package com.webdoc.mainPage;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Popup_PromoCode extends AppCompatActivity {
    EditText promo_num;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_promocode);

        /////////////////////////////////////////////////////////////////////////////////
/// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) findViewById(R.id.heading)).setText("پرومو کوڈ");

            ((TextView) findViewById(R.id.button2)).setTypeface(tf);
            ((TextView) findViewById(R.id.button2)).setText("کینسل");

            ((Button) findViewById(R.id.save)).setTypeface(tf);
            ((Button) findViewById(R.id.save)).setText("سیو");
        }



            DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .95), (int) (height * .3));



        promo_num = (EditText) findViewById(R.id.promo_num);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String _pin_ = promo_num.getText().toString();

                String _final_ = "FreeService/" + MySingleton.getInstance().getUserId() + "/" + _pin_;
                final GetSingle asyncTask = new GetSingle(Popup_PromoCode.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String resp = (String) result;
                        if (resp == null || resp.equals("")) {
                            new SweetAlertDialog(Popup_PromoCode.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            if (resp.equals("\"True\"")) {
                                new SweetAlertDialog(Popup_PromoCode.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Promocode added successfully")
                                        .setContentText("Welcome to WebDoc")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                                promo_num.setText("");
                                                /*Intent sub=new Intent(Popup_PromoCode.this,Popup_Subscription.class).putExtra("value",true);
                                                startActivity(sub);*/
                                                finish();
                                            }
                                        })
                                        .show();


                                return;

                            } else if (resp.equals("\"False\"")) {
                                new SweetAlertDialog(Popup_PromoCode.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("This Promocode does not exist!")
                                        .setContentText("Please try again")
                                        .show();
                                return;
                            }
                        }
                    }
                }, Uri.encode(_final_) + "__" + "1");
                asyncTask.execute();
            }
        });
    }

    public void cancel(View view) {

        finish();
    }
}
