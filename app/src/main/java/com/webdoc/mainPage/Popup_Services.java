package com.webdoc.mainPage;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;

public class Popup_Services extends AppCompatActivity {

    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X1";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y1";

    View rootLayout;

    private int revealX;
    private int revealY;

    @Override
    protected void onStop() {
        //unRevealActivity();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        //unRevealActivity();
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage_popup_services);


        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .95), (int) (height * .6));

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id._main_);

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) findViewById(R.id.heading)).setText("سروسز");

            ((TextView) findViewById(R.id.corporatetext)).setTypeface(tf);
            ((TextView) findViewById(R.id.corporatetext)).setText("کارپوریٹ");

            ((TextView) findViewById(R.id.iinsurancetext)).setTypeface(tf);
            ((TextView) findViewById(R.id.iinsurancetext)).setText("انشورنس");

            ((TextView) findViewById(R.id.claimstext)).setTypeface(tf);
            ((TextView) findViewById(R.id.claimstext)).setText("کلیم");

            ((TextView) findViewById(R.id.familytext)).setTypeface(tf);
            ((TextView) findViewById(R.id.familytext)).setText("خاندان کے رکن شامل کریں");


        }

       /* if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity((int) (revealX * .95), (int) (revealY * .6));
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(1000);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }

    protected void unRevealActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish();
        } else {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                    rootLayout, revealX, revealY, finalRadius, 0);

            circularReveal.setDuration(400);
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rootLayout.setVisibility(View.INVISIBLE);
                    finish();
                }
            });


            circularReveal.start();
        }*/

    }

    public void openCorporate(View view) {

        startActivity(new Intent(Popup_Services.this,_MainActivity.class).putExtra("Fragment","Corporate"));
    }

    public void openInsurance(View view) {
        startActivity(new Intent(Popup_Services.this,_MainActivity.class).putExtra("Fragment","Insurance"));
    }

    public void openClaim(View view) {
        startActivity(new Intent(Popup_Services.this, _MainActivity.class).putExtra("Fragment","Claims"));
    }

    public void openFamily(View view) {
        startActivity(new Intent(Popup_Services.this, _MainActivity.class).putExtra("Fragment","Family Registration"));
    }
}