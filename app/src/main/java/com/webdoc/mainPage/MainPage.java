package com.webdoc.mainPage;

import android.animation.Animator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.AsyncResponse;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;
import com.webdoc._Splash;
import com.webdoc.messenger.BaseActivity;
import com.webdoc.spotLight.SpotlightSequence;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class MainPage extends BaseActivity {
    ImageButton doctors;
    ImageButton subs;
    ImageButton buy;
    ImageButton doc;
    ImageButton buyonline;
    TextView myhealth_text;
    ToggleSwitch toggleSwitch;
    final int REQ_CODE_SPEECH_INPUT = 1001;

    public void logout() {
        final SweetAlertDialog pDialog = new SweetAlertDialog(MainPage.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText("Alert!");
        pDialog.setContentText("Are you sure you want to logout ?");
        pDialog.setCustomImage(R.drawable.logowebdoc1);
        pDialog.setConfirmText("Yes");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
                sharedpref.edit().clear().commit();
                MySingleton.getInstance().setBoolean(true);
                MySingleton.getInstance().MainList.clear();
                getSinchServiceInterface().stopClient();
                startActivity(new Intent(MainPage.this, _Splash.class));
                finish();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        pDialog.show();
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        System.exit(0);

    }

    boolean ANIMATION_RUN = false;
    int Count = 0;

    @Override
    protected void onPause() {
        super.onPause();
        if (ANIMATION_RUN) {
            stopDoctorAnimation();
            ANIMATION_RUN = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Count == 0) {
            Count++;
            return;
        }
        if (!ANIMATION_RUN) {
            initDoctorAnimation();
            ANIMATION_RUN = true;
        }
        if (MySingleton.getInstance().MainList == null || MySingleton.getInstance().MainList.size() == 0)
            functionCall(null);
    }

    private static final int ANIM_DURATION_TOOLBAR = 300;
    Toolbar topToolBar;

    View rootLayout;

    int width_s, height_s;

    private int revealX;
    private int revealY;

    private TextDrawable.IBuilder mDrawableBuilder;
    ImageView im;
    ImageButton myhealth, subscription, service, reports;
    RelativeLayout about;

    int h1, h2;

    private float[] lastTouchDownXY = new float[2];


    private void stopDoctorAnimation() {
        final ImageButton button = (ImageButton) findViewById(R.id.doctor_im);
        button.clearAnimation();
    }

    private void initDoctorAnimation() {
        final ImageButton button = (ImageButton) findViewById(R.id.doctor_im);

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        final Animation myAnimOut = AnimationUtils.loadAnimation(this, R.anim.bounce_out);
        final Animation myAnimIn = AnimationUtils.loadAnimation(this, R.anim.bounce_in);

        myAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!ANIMATION_RUN) {
                    stopDoctorAnimation();
                    ANIMATION_RUN = false;
                } else
                    button.startAnimation(myAnimIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        myAnimOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!ANIMATION_RUN) {
                    stopDoctorAnimation();
                    ANIMATION_RUN = false;
                } else
                    button.startAnimation(myAnimIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        myAnimIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!ANIMATION_RUN) {
                    stopDoctorAnimation();
                    ANIMATION_RUN = false;
                } else
                    button.startAnimation(myAnimOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        button.startAnimation(myAnim);
    }

    // ImageButton RoundImageView;
    TextView name;
    JSONArray Arr;
    WebView w;
    private final String url = "http://webdoc.com.pk/about-us/";
    RelativeLayout main;
    TextView Myhealth_text;
    TextView buyonlinetext;
    TextView servicestext;
    TextView doctorstext;
    TextView subscriptiontext;
    TextView reportstext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);
        /*FirebaseInstanceId.getInstance().getToken();
        FirebaseMessaging.getInstance().subscribeToTopic("news");
*/
        /////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////
        Batch.User.getInstallationID();

        Myhealth_text = (TextView) findViewById(R.id.myhealth_text);
        buyonlinetext = (TextView) findViewById(R.id.buyonlinetext);
        doctorstext = (TextView) findViewById(R.id.reltext);


        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        toggleSwitch = (ToggleSwitch) findViewById(R.id.log);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (position == 0) {
                    ((TextView) findViewById(R.id.myhealth_text)).setText("My Health");
                    ((TextView) findViewById(R.id.buyonlinetext)).setText("Buy Online");
                    ((TextView) findViewById(R.id.subscriptiontext)).setText("Subscription");
                    ((TextView) findViewById(R.id.reportstext)).setText("Reports");
                    ((TextView) findViewById(R.id.servicestext)).setText("Services");
                    ((TextView) findViewById(R.id.reltext)).setText("Doctors");
                    MySingleton.getInstance().setLang(false);

                } else {
                    ((TextView) findViewById(R.id.myhealth_text)).setTypeface(tf);
                    ((TextView) findViewById(R.id.myhealth_text)).setText("صحت");
                    ((TextView) findViewById(R.id.buyonlinetext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.buyonlinetext)).setText("آن لائن خریدیں");
                    ((TextView) findViewById(R.id.subscriptiontext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.subscriptiontext)).setText("سبسکرائب کریں");
                    ((TextView) findViewById(R.id.reportstext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.reportstext)).setText("رپورٹس ");
                    ((TextView) findViewById(R.id.servicestext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.servicestext)).setText("سروسز ");
                    ((TextView) findViewById(R.id.reltext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.reltext)).setText(" ڈاکٹر");

                    MySingleton.getInstance().setLang(true);

                }
            }
        });


// Apply the font to your TextView object
       /* Myhealth_text.setTypeface(tf);
        Myhealth_text.setText("صحت");*/
/*
        PushNotifications.start(getApplicationContext(), "8045541e-afde-408c-aa1f-8a323d898d36");
        PushNotifications.subscribe("hello");
        PushNotifications.setOnMessageReceivedListener(new PushNotificationReceivedListener() {
            @Override
            public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {

            }
        });*/


        buyonline = (ImageButton) findViewById(R.id.buyonline);
        buyonline.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // save the X,Y coordinates
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (!MySingleton.getInstance().getGprofile()) {
                        new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile Incomplete")
                                .setContentText("Please complete your general profile")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent g = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                                        startActivity(g);
                                        sweetAlertDialog.dismiss();
                                    }
                                })

                                .show();
                        return false;
                    } else {
                        Intent b = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "BuyOnline");
                        startActivity(b);
                    }
                }

                // let the touch event pass on to whoever needs it
                return false;
            }
        });


        doc = (ImageButton) findViewById(R.id.doctor_im);

        doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!MySingleton.getInstance().getGprofile()) {
                    new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Profile Incomplete")
                            .setContentText("Please complete your general profile")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile"));
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();


                    return;

                }

                startActivity(new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "DoctorList"));

            }

        });


        // doctors = (ImageButton) findViewById(R.id.doctors);
//        doctors.setEnabled(true);
//        subs = (ImageButton) findViewById(R.id.subs);
//        buy = (ImageButton) findViewById(R.id.buy);


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        width_s = dm.widthPixels;
        height_s = dm.heightPixels;

        topToolBar = (Toolbar) findViewById(R.id.toolbar);
        topToolBar.setTitle("");
        setSupportActionBar(topToolBar);

        h1 = ((View) findViewById(R.id.Top_Toolbar)).getHeight();
        h2 = ((View) findViewById(R.id.icon)).getHeight();

        View myView = findViewById(R.id._MAIN_);

        im = (ImageView) findViewById(R.id.RoundImageView);
        name = (TextView) findViewById(R.id.name);
        about = (RelativeLayout) findViewById(R.id.about);


        myhealth = (ImageButton) findViewById(R.id.myhealth_im);
        myhealth.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // save the X,Y coordinates
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (!MySingleton.getInstance().getGprofile()) {
                        new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile Incomplete")
                                .setContentText("Please complete your general profile")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent g = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                                        startActivity(g);
                                        sweetAlertDialog.dismiss();
                                    }
                                })

                                .show();
                        return false;
                    } else {
                        Intent m = new Intent(MainPage.this, Popup_MyHealth.class);
                        startActivity(m);
                    }

                    View Main = (View) view.getParent().getParent().getParent().getParent();
                    h1 = Main.findViewById(R.id.Top_Toolbar).getHeight();
                    h2 = Main.findViewById(R.id.icon).getHeight();

                    int height = ((View) view.getParent().getParent().getParent().getParent()).getHeight();
                    int width = ((View) view.getParent().getParent().getParent().getParent()).getWidth();

                    int mHeight = ((View) view.getParent()).getMeasuredHeight();
                    int mWidth = ((View) view.getParent()).getMeasuredWidth();

                    lastTouchDownXY[0] = motionEvent.getX() + (view.getWidth() / 2);
                    lastTouchDownXY[1] = motionEvent.getY() + (h1 + h2) + (view.getHeight() / 2);

                    int x = (int) lastTouchDownXY[0];
                    int y = (int) lastTouchDownXY[1];
                    int revealX = (int) (x);
                    int revealY = (int) (y);

                    Intent intent = new Intent(MainPage.this, Popup_MyHealth.class);
                    intent.putExtra(Popup_MyHealth.EXTRA_CIRCULAR_REVEAL_X, revealX);
                    intent.putExtra(Popup_MyHealth.EXTRA_CIRCULAR_REVEAL_Y, revealY);

                    presentActivity(view, intent);
                }

                // let the touch event pass on to whoever needs it
                return false;
            }
        });


        service = (ImageButton) findViewById(R.id.services_im);
        service.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // save the X,Y coordinates
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (!MySingleton.getInstance().getGprofile()) {
                        new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile Incomplete")
                                .setContentText("Please complete your general profile")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent g = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                                        startActivity(g);
                                        sweetAlertDialog.dismiss();
                                    }
                                })

                                .show();
                        return false;
                    } else {
                        Intent s = new Intent(MainPage.this, Popup_Services.class);
                        startActivity(s);
                    }


                    View Main = (View) view.getParent().getParent().getParent().getParent();
                    h1 = Main.findViewById(R.id.Top_Toolbar).getHeight();
                    h2 = Main.findViewById(R.id.icon).getHeight();

                    int height = ((View) view.getParent().getParent().getParent().getParent()).getHeight();
                    int width = ((View) view.getParent().getParent().getParent().getParent()).getWidth();

                    int mHeight = ((View) view.getParent()).getMeasuredHeight();
                    int mWidth = ((View) view.getParent()).getMeasuredWidth();

                    lastTouchDownXY[0] = width_s - (motionEvent.getX() + (view.getWidth() / 2));
                    lastTouchDownXY[1] = motionEvent.getY() + (h1 + h2) + (view.getHeight() / 2);

                    int x = (int) lastTouchDownXY[0];
                    int y = (int) lastTouchDownXY[1];
                    int revealX = (int) (x);
                    int revealY = (int) (y);

                    Intent intent = new Intent(MainPage.this, Popup_Services.class);
                    intent.putExtra(Popup_Services.EXTRA_CIRCULAR_REVEAL_X, revealX);
                    intent.putExtra(Popup_Services.EXTRA_CIRCULAR_REVEAL_Y, revealY);

                    presentActivity(view, intent);
                }

                // let the touch event pass on to whoever needs it
                return false;
            }
        });

        subscription = (ImageButton) findViewById(R.id.subscription_im);
        subscription.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // save the X,Y coordinates
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (!MySingleton.getInstance().getGprofile()) {
                        new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile Incomplete")
                                .setContentText("Please complete your general profile")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent g = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                                        startActivity(g);
                                        sweetAlertDialog.dismiss();
                                    }
                                })

                                .show();
                        return false;
                    } else {
                        Intent subsc = new Intent(MainPage.this, Popup_Subscription.class);
                        startActivity(subsc);
                    }

                    View Main = (View) view.getParent().getParent().getParent().getParent();
                    h1 = Main.findViewById(R.id.Top_Toolbar).getHeight();
                    h2 = Main.findViewById(R.id.icon).getHeight();

                    int height = ((View) view.getParent().getParent().getParent().getParent()).getHeight();
                    int width = ((View) view.getParent().getParent().getParent().getParent()).getWidth();

                    int mHeight = ((View) view.getParent()).getMeasuredHeight();
                    int mWidth = ((View) view.getParent()).getMeasuredWidth();

                    lastTouchDownXY[0] = ((mWidth * 1) + 10) + (motionEvent.getX() + (view.getWidth() / 2));
                    lastTouchDownXY[1] = mHeight + (motionEvent.getY() + (h1 + h2) + (view.getHeight() / 2));

                    int x = (int) lastTouchDownXY[0];
                    int y = (int) lastTouchDownXY[1];
                    int revealX = (int) (x);
                    int revealY = (int) (y);

                    Intent intent = new Intent(MainPage.this, Popup_Subscription.class);
                    intent.putExtra(Popup_Services.EXTRA_CIRCULAR_REVEAL_X, revealX);
                    intent.putExtra(Popup_Services.EXTRA_CIRCULAR_REVEAL_Y, revealY);

                    presentActivity(view, intent);
                }

                // let the touch event pass on to whoever needs it
                return false;
            }
        });

        reports = (ImageButton) findViewById(R.id.reports_im);
        reports.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                // save the X,Y coordinates
                if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    if (!MySingleton.getInstance().getGprofile()) {
                        new SweetAlertDialog(MainPage.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Profile Incomplete")
                                .setContentText("Please complete your general profile")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        Intent g = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                                        startActivity(g);
                                        sweetAlertDialog.dismiss();
                                    }
                                })

                                .show();

                        return false;

                    } else {
                        Intent r = new Intent(MainPage.this, Popup_Reports.class);
                        startActivity(r);
                    }

                    View Main = (View) view.getParent().getParent().getParent().getParent();
                    h1 = Main.findViewById(R.id.Top_Toolbar).getHeight();
                    h2 = Main.findViewById(R.id.icon).getHeight();

                    int height = ((View) view.getParent().getParent().getParent().getParent()).getHeight();
                    int width = ((View) view.getParent().getParent().getParent().getParent()).getWidth();

                    int mHeight = ((View) view.getParent()).getMeasuredHeight();
                    int mWidth = ((View) view.getParent()).getMeasuredWidth();

                    lastTouchDownXY[0] = ((mWidth * 2) + 20) + (motionEvent.getX() + (view.getWidth() / 2));
                    lastTouchDownXY[1] = mHeight + (motionEvent.getY() + (h1 + h2) + (view.getHeight() / 2));

                    int x = (int) lastTouchDownXY[0];
                    int y = (int) lastTouchDownXY[1];
                    int revealX = (int) (x);
                    int revealY = (int) (y);

                    Intent intent = new Intent(MainPage.this, Popup_Reports.class);
                    intent.putExtra(Popup_Reports.EXTRA_CIRCULAR_REVEAL_X, revealX);
                    intent.putExtra(Popup_Reports.EXTRA_CIRCULAR_REVEAL_Y, revealY);

                    presentActivity(view, intent);
                }

                // let the touch event pass on to whoever needs it
                return false;
            }
        });

        rootLayout = findViewById(R.id._MAIN_);

        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = width_s / 2;
            revealY = height_s / 2;


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity(revealX, revealY);
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }


        //functionCall(savedInstanceState);
        //if(Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]")))
        //Setting name in textview main page
        //functionSettingName();


    }


    public void presentActivity(View view, Intent intent) {
        /*ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, view, "transition");

        ActivityCompat.startActivity(this, intent, options.toBundle());*/
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_mainpage, menu);
        return true;


    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:

                //startActivity(new Intent(this, About.class));
                return true;
            case R.id.logout:

                //startActivity(new Intent(this, Help.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(4000);
            circularReveal.setInterpolator(new AccelerateInterpolator());
            circularReveal.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    functionCall(null);

                    //();
                    ANIMATION_RUN = true;
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }

    public void openGeneralProfile(View view) {
        startActivity(new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile"));
    }


    /* public boolean openOnline(View view) {
        *//* if (!MySingleton.getInstance().getGprofile()) {
            new SweetAlertDialog(MainPage.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Please complete your general profile first")
                    .show();
            return false;
        }*//*
        startActivity(new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "BuyOnline"));
        return true;
    }


    public void openDoctors(View view) {

        startActivity(new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "DoctorList"));
    }
*/
    public void logout(MenuItem item) {
        /*SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        MySingleton.getInstance().setBoolean(true);
        MySingleton.getInstance().MainList.clear();
        getSinchServiceInterface().stopClient();
        startActivity(new Intent(MainPage.this, _Splash.class));
        finish();

*/
        logout();
    }

    private Boolean getGoOnline() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        boolean b = sharedpref.getBoolean("GoOnline", true);
        return b;
    }

    private void setGoOnline(boolean b) {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putBoolean("GoOnline", b);
        editor.apply();
    }

    String function__;
    String[] SharedPrefNames = {"PatientProfile", "Conditions", "Allergies", "Medications", "Immunizations", "Procedures", "Insurance", "Corporate", "Consultations"};
    static final String[] Fragments = {"GeneralProfile", "BuyOnline", "DoctorList", "Conditions", "Allergies", "Immunizations", "Medications", "Procedures", "Corporate", "Insurance",
            "Claims", "Consultation Report", "Call Summary",};

    private void functionCall(final Bundle savedInstanceState) {
        if (getGoOnline()) {
            if (MySingleton.getInstance().isConnected(this)) {
                MySingleton.getInstance().showLoadingPopup(MainPage.this, "Fetching Profile - Please Wait");
                function__ = "GetPatientProfile/" + MySingleton.getInstance().getUserId();
                final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jsn = (JSONArray) result;
                        if (jsn == null) {
                            MySingleton.getInstance().dismissLoadingPopup();
                            new SweetAlertDialog(MainPage.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                            logout();
                                        }
                                    })
                                    .show();
                            functionSettingName();

                            return;
                        }
                        try {
                            for (int i = 0; i < jsn.length(); i++) {
                                if (i == 0) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    MySingleton.getInstance().setArray(Arr);
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setGprofile(false);
                                    } else
                                        MySingleton.getInstance().setGprofile(true);
                                    functionSettingName();
                                    MySingleton.getInstance().dismissLoadingPopup();

                                } else if (i == 6) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setIproduct(false);
                                    } else
                                        MySingleton.getInstance().setIproduct(true);
                                    MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());
                                } else if (i == 7) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setCpanel(false);
                                    } else
                                        MySingleton.getInstance().setCpanel(true);
                                    MySingleton.getInstance().setArray(Arr);
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                                } else {
                                    MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        functionSettingName();
                        function__ = "DoctorList";
                        final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {
                                JSONArray jarr = (JSONArray) result;
                                MySingleton.getInstance().addSharedPrefEntry("DoctorList", jarr.toString());
                                MySingleton.getInstance().setArray(jarr);

                                function__ = "CountryList";
                                final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                                    @Override
                                    public void processFinish(Object result) {
                                        JSONArray jarr = (JSONArray) result;
                                        MySingleton.getInstance().addSharedPrefEntry("CountryList", jarr.toString());
                                        MySingleton.getInstance().setArray(jarr);

                                        function__ = "CorporateCompaines";
                                        final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                                            @Override
                                            public void processFinish(Object result) {
                                                JSONArray jarr = (JSONArray) result;
                                                MySingleton.getInstance().addSharedPrefEntry("CorporateCompanies", jarr.toString());
                                                MySingleton.getInstance().setArray(jarr);

                                                function__ = "CorporateRelationShip";
                                                final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Object result) {
                                                        JSONArray jarr = (JSONArray) result;
                                                        MySingleton.getInstance().addSharedPrefEntry("CorporateRelationShip", jarr.toString());
                                                        MySingleton.getInstance().setArray(jarr);

                                                        function__ = "FreeReward/" + MySingleton.getInstance().getUserId();
                                                        final GetProfile asyncTask = new GetProfile(MainPage.this, new AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Object result) {
                                                                String res = ((String) result).replace("\"", "");
                                                                String link = res.split(",")[0];
                                                                final String text = res.split(",")[1];
                                                                //http:\/\/webdoc.com.pk\/imagesofsystem\/PrizeMoney.png
                                                                link = link.replaceAll("\\\\", "");

                                                                //UrlImageViewHelper.setUrlDrawable((ImageView) findViewById(R.id.awardIM), link);

                                                                //toolbarAnimation();
                                                                if (savedInstanceState == null) {
                                                                    // selectConditions();
                                                                }
                                                                JSONArray j1 = MySingleton.getInstance().getArray(0);

                                                                setGoOnline(false);
                                                                MySingleton.getInstance().dismissLoadingPopup();

                                                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        SpotlightSequence.getInstance(MainPage.this, null)
                                                                                .addSpotlight(findViewById(R.id.RoundImageView), "General Profile", "Click here to complete your general profile", "___General Profile___")
                                                                                .addSpotlight((findViewById(R.id.bottom_Doctor)), "Doctors", "Click here to connect with a doctor", "___Doctors___")
                                                                                //.addSpotlight((findViewById(R.id.buy)), "Buy Online", "Click here to buy Health Cards", "___Buy Online___")
                                                                                //.addSpotlight((findViewById(R.id.text1)), "Subscription", "Click here to subscribe Health Card", "___Subscription___")
                                                                                .startSequence(new AsyncResponse() {
                                                                                    @Override
                                                                                    public void processFinish(Object result) {
                                                                                        initDoctorAnimation();
                                                                                    }
                                                                                }, MainPage.this);
                                                                    }
                                                                }, 500);

                                                                /*new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        if (!text.equals("False")) {MainPage.this, TrophyScreen.class).putExtra("_TEXT_", text));
                                                                        }
                                                                    }
                                                                }, 3500);*/


                                                            }
                                                        }, function__ + "__" + "1");
                                                        asyncTask.execute();
                                                    }
                                                }, function__ + "__" + "0");
                                                asyncTask.execute();
                                            }
                                        }, function__ + "__" + "0");
                                        asyncTask.execute();
                                    }
                                }, function__ + "__" + "0");
                                asyncTask.execute();
                            }
                        }, function__ + "__" + "0");
                        asyncTask.execute();
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "No Network Available", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        } else {
            MySingleton.getInstance().Contextt_ = getApplicationContext();
            try {
                for (int i = 0; i < 9; i++) {
                    if (i == 0) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setGprofile(false);

                        } else
                            MySingleton.getInstance().setGprofile(true);
                        functionSettingName();
                        MySingleton.getInstance().dismissLoadingPopup();


                    } else if (i == 6) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setIproduct(false);
                        } else
                            MySingleton.getInstance().setIproduct(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else if (i == 7) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setCpanel(false);
                        } else
                            MySingleton.getInstance().setCpanel(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONArray jarr = MySingleton.getInstance().getSharedPrefEntry("DoctorList");
            MySingleton.getInstance().setArray(jarr);
            jarr = MySingleton.getInstance().getSharedPrefEntry("CountryList");
            MySingleton.getInstance().setArray(jarr);
            jarr = MySingleton.getInstance().getSharedPrefEntry("CorporateCompanies");
            MySingleton.getInstance().setArray(jarr);
            jarr = MySingleton.getInstance().getSharedPrefEntry("CorporateRelationShip");
            MySingleton.getInstance().setArray(jarr);
            //functionSettingName();
           /* function__ = "FreeReward/" + MySingleton.getInstance().getUserId();
            final GetSingle asyncTask = new GetSingle(MainPage.this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    String res = ((String) result).replace("\"", "");
                    MySingleton.getInstance().setFreeReward(res);
                    String link = res.split(",")[0];
                    final String text = res.split(",")[1];
                    //http:\/\/webdoc.com.pk\/imagesofsystem\/PrizeMoney.png
                    link = link.replaceAll("\\\\", "");
                    JSONArray j1 = MySingleton.getInstance().getArray(0);
                    if (savedInstanceState == null) {
                        //selectConditions();
                    }
                    //  toolbarAnimation();
                    setGoOnline(false);

                    MySingleton.getInstance().dismissLoadingPopup();
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!text.equals("False")) {
                                startActivity(new Intent(MainPage.this, TrophyScreen.class).putExtra("_TEXT_", text));
                            }


                        }
                    }, 3500);

                }
            }, function__ + "__" + "1");
            asyncTask.execute();
        }*/
            initDoctorAnimation();
        }
    }


    public void functionSettingName() {


        Arr = MySingleton.getInstance().getArray(0);
        final String _name = name.getText().toString();
        try {
            if (!MySingleton.getInstance().getGprofile()) {
                name.setText("Profile");

            } else {

                name.setText(Arr.get(0) + "");

                mDrawableBuilder = TextDrawable.builder()
                        .beginConfig()
                        .withBorder(4)
                        .bold()
                        .endConfig()
                        .round();
                String temp = (Arr.get(0).toString());
                char[] FirstChar = temp.toCharArray();
                TextDrawable drawable = mDrawableBuilder.build(String.valueOf(FirstChar[0]).toUpperCase(), ContextCompat.getColor(getApplicationContext(), R.color.line_divider_mini));
                im.setImageDrawable(drawable);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void about(MenuItem item) {
       /* w = (WebView) findViewById((R.id.w));
        main = (RelativeLayout) findViewById(R.id.main);
        about = (RelativeLayout) findViewById(R.id.AboutUs);
        about.setVisibility(View.VISIBLE);*/
        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(myIntent);

    }

    private void promptSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-in");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "How can I help you?");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(),
                    "Speech input not available",
                    Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    // here the string converted from your voice in result.get(0)

                    if (result.get(0).contains("subscription")||result.get(0).contains("subscribe")||result.get(0).contains("card")) {
                        Intent goSubs = new Intent(MainPage.this, Popup_Subscription.class);
                        startActivity(goSubs);
                    } else if (result.get(0).contains("doctor")||result.get(0).contains("doctors")||result.get(0).contains("doktor")||result.get(0).contains("Dr")) {
                        Intent goDoc = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "DoctorList");
                        startActivity(goDoc);
                    } else if (result.get(0).contains("my health")||result.get(0).contains("sehat")) {
                        Intent goHealth = new Intent(MainPage.this, Popup_MyHealth.class);
                        startActivity(goHealth);
                    } else if (result.get(0).contains("services")||(result.get(0).contains("service"))) {
                        Intent goServ = new Intent(MainPage.this, Popup_Services.class);
                        startActivity(goServ);
                    } else if (result.get(0).contains("buy online")||result.get(0).contains("online")||result.get(0).contains("buy")) {
                        Intent goBuy = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "BuyOnline");
                        startActivity(goBuy);
                    } else if (result.get(0).contains("reports")||result.get(0).contains("doctor")) {
                        Intent goReport = new Intent(MainPage.this, Popup_Reports.class);
                        startActivity(goReport);
                    } else if (result.get(0).contains("profile")) {
                        Intent goProfile = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "GeneralProfile");
                        startActivity(goProfile);
                    } else if (result.get(0).contains("condition")) {
                        Intent goCondition = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Conditions");
                        startActivity(goCondition);
                    } else if (result.get(0).contains("allergy") || result.get(0).contains("allergies")) {
                        Intent goAllergies = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Allergies");
                        startActivity(goAllergies);
                    } else if (result.get(0).contains("immunizations")||result.get(0).contains("immunization")) {
                        Intent goImm = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Immunizations");
                        startActivity(goImm);
                    } else if (result.get(0).contains("medications")||result.get(0).contains("medication")) {
                        Intent goMed = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Medications");
                        startActivity(goMed);
                    } else if (result.get(0).contains("procedures")||result.get(0).contains("procedure")) {
                        Intent goPro = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Procedures");
                        startActivity(goPro);
                    } else if (result.get(0).contains("promo code")||result.get(0).contains("promo")||result.get(0).contains("code")) {
                        Intent goPromo = new Intent(MainPage.this, Popup_PromoCode.class);
                        startActivity(goPromo);
                    } else if (result.get(0).contains("corporate")) {
                        Intent goCorp = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Corporate");
                        startActivity(goCorp);
                    } else if (result.get(0).contains("insurance")) {
                        Intent goInsure = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Insurance");
                        startActivity(goInsure);
                    } else if (result.get(0).contains("claims")||result.get(0).contains("claim")) {
                        Intent goClaim = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Claims");
                        startActivity(goClaim);
                    } else if (result.get(0).contains("consultation report")) {
                        Intent goCons = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Consultation Report");
                        startActivity(goCons);
                    } else if (result.get(0).contains("call summary")) {
                        Intent gocallSum = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Call Summary");
                        startActivity(gocallSum);
                    } else if (result.get(0).contains("family registration") || result.get(0).contains("family member")||result.get(0).contains("family")) {
                        Intent gofam = new Intent(MainPage.this, _MainActivity.class).putExtra("Fragment", "Family Registration");
                        startActivity(gofam);
                    }
                }
                break;
            }
        }
    }

    public void openSpeech(View view) {

        promptSpeechInput();
    }
}


