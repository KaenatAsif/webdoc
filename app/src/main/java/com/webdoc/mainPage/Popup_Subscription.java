package com.webdoc.mainPage;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Popup_Subscription extends AppCompatActivity {

    Button subscribe;
    EditText pin_num;
    EditText Promotext;
    View promo_layout;


    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X1";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y1";

    View rootLayout;

    private int revealX;
    private int revealY;

    @Override
    protected void onStop() {
        //unRevealActivity();
        super.onStop();
        finish();
    }

    @Override
    public void onBackPressed() {
        //unRevealActivity();
        finish();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage_popup_subscription);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////



        View v = findViewById(R.id._main_);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;




        pin_num = (EditText) findViewById(R.id.pin_num);
        subscribe = (Button) findViewById(R.id.subscribe);
        subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String _pin_ = pin_num.getText().toString();

                String _final_ = "ActivateCard/" + MySingleton.getInstance().getUserId() + "/" + _pin_;
                final GetSingle asyncTask = new GetSingle(Popup_Subscription.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String resp = (String) result;
                        if (resp == null || resp.equals("")) {
                            new SweetAlertDialog(Popup_Subscription.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            if (resp.equals("\"True\"")) {
                                new SweetAlertDialog(Popup_Subscription.this, SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("Subscription successful!")
                                        .setContentText("Welcome to WebDoc")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                sweetAlertDialog.dismissWithAnimation();
                                                pin_num.setText("");
                                            }
                                        })
                                        .show();
                                return;
                            } else if (resp.equals("\"False\"")) {
                                new SweetAlertDialog(Popup_Subscription.this, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("This pin code does not exist!")
                                        .setContentText("Please try again")
                                        .show();
                                return;
                            }
                        }
                    }
                }, Uri.encode(_final_) + "__" + "1");
                asyncTask.execute();
            }
        });


        getWindow().setLayout((int) (width * .95), (int) (height * .6));

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id._main_);

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);




        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) findViewById(R.id.heading)).setText("سبسکرائب کریں");

            ((TextView) findViewById(R.id.step1)).setTypeface(tf);
            ((TextView) findViewById(R.id.step1)).setText("1)کادڈ سکریچ کریں");

            ((TextView) findViewById(R.id.step2)).setTypeface(tf);
            ((TextView) findViewById(R.id.step2)).setText("2)پن درج کریں");

            ((TextView) findViewById(R.id.step3)).setTypeface(tf);
            ((TextView) findViewById(R.id.step3)).setText("3)سبمٹ کریں");

            ((Button) findViewById(R.id.subscribe)).setTypeface(tf);
            ((Button) findViewById(R.id.subscribe)).setText("سبمٹ");

            ((TextView) findViewById(R.id.promotext)).setTypeface(tf);
            ((TextView) findViewById(R.id.promotext)).setText("پرومو کوڈ");




        }

/*        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity((int) (revealX * .95), (int) (revealY * .6));
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }


    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(1000);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }






    protected void unRevealActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish();
        } else {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                    rootLayout, revealX, revealY, finalRadius, 0);

            circularReveal.setDuration(400);
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rootLayout.setVisibility(View.INVISIBLE);
                    finish();
                }
            });


            circularReveal.start();
        }*/

      /*  if (getIntent().getStringExtra("value").equals(true));
        {
            subscribe.setEnabled(false);
        }
*/
    }



    public void addPromo(View view) {

Intent openPromo= new Intent(Popup_Subscription.this,Popup_PromoCode.class);
        startActivity(openPromo);
    }
}