package com.webdoc.mainPage;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;

/**
 * Created by fahee on 1/30/2018.
 */

public class Popup_MyHealth extends AppCompatActivity {

    public static final String EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X1";
    public static final String EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y1";

    View rootLayout;

    private int revealX;
    private int revealY;

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainpage_popup_myhealth);

/////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        View v = findViewById(R.id._main_);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .95), (int) (height * .6));

        final Intent intent = getIntent();

        rootLayout = findViewById(R.id._main_);

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        if (MySingleton.getInstance().getLang())
        {
            ((TextView) findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) findViewById(R.id.heading)).setText("صحت");
            ((TextView) findViewById(R.id.conditionstext)).setTypeface(tf);
            ((TextView) findViewById(R.id.conditionstext)).setText("طبی حالت");
            ((TextView) findViewById(R.id.allergiestext)).setTypeface(tf);
            ((TextView) findViewById(R.id.allergiestext)).setText("الرجی");
            ((TextView) findViewById(R.id.immunizationstext)).setTypeface(tf);
            ((TextView) findViewById(R.id.immunizationstext)).setText("حفاظتی ٹیکے");
            ((TextView) findViewById(R.id.medicationstext)).setTypeface(tf);
            ((TextView) findViewById(R.id.medicationstext)).setText("ادویات");
            ((TextView) findViewById(R.id.procedurestext)).setTypeface(tf);
            ((TextView) findViewById(R.id.procedurestext)).setText("آپریشن");

        }

       // else ((TextView) findViewById(R.id.heading)).setText("My Health");

       /* if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) &&
                intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)) {
            rootLayout.setVisibility(View.INVISIBLE);

            revealX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0);
            revealY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0);


            ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        revealActivity((int) (revealX * .95), (int) (revealY * .6));
                        rootLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
            }
        } else {
            rootLayout.setVisibility(View.VISIBLE);
        }
    }

    protected void revealActivity(int x, int y) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);

            // create the animator for this view (the start radius is zero)
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(rootLayout, x, y, 0, finalRadius);
            circularReveal.setDuration(1000);
            circularReveal.setInterpolator(new AccelerateInterpolator());

            // make the view visible and start the animation
            rootLayout.setVisibility(View.VISIBLE);
            circularReveal.start();
        } else {
            finish();
        }
    }

    protected void unRevealActivity() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            finish();
        } else {
            float finalRadius = (float) (Math.max(rootLayout.getWidth(), rootLayout.getHeight()) * 1.1);
            Animator circularReveal = ViewAnimationUtils.createCircularReveal(
                    rootLayout, revealX, revealY, finalRadius, 0);

            circularReveal.setDuration(400);
            circularReveal.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    rootLayout.setVisibility(View.INVISIBLE);
                    finish();
                }
            });


            circularReveal.start();
        }*/
    }

    public void openConditions(View view) {
        startActivity(new Intent(Popup_MyHealth.this,_MainActivity.class).putExtra("Fragment","Conditions"));
    }

    public void showAllergies(View view) {
        startActivity(new Intent(Popup_MyHealth.this,_MainActivity.class).putExtra("Fragment","Allergies"));
    }

    public void showImmunizations(View view) {
        startActivity(new Intent(Popup_MyHealth.this,_MainActivity.class).putExtra("Fragment","Immunizations"));
    }

    public void showMedications(View view) {
        startActivity(new Intent(Popup_MyHealth.this,_MainActivity.class).putExtra("Fragment","Medications"));
    }

    public void showProcedures(View view) {
        startActivity(new Intent(Popup_MyHealth.this,_MainActivity.class).putExtra("Fragment","Procedures"));
    }
}

