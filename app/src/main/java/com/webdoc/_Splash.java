package com.webdoc;


import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.SinchError;
import com.webdoc.mainPage.MainPage;
import com.webdoc.messenger.BaseActivity;
import com.webdoc.messenger.SinchService;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;



public class _Splash extends BaseActivity implements SinchService.StartFailedListener {


    ToggleSwitch lang;

    public static Activity fa;

    RelativeLayout rl;

    private LinearLayout iv;
    private TextView et;

    ToggleSwitch toggleSwitch;

    Animation startAnimation;

    private RequestQueue requestQueue;
    private static final String URL = "http://portal.webdoc.com.pk/transection/apicall.php?email=03145362496@webdoc.com.pk&RRN=171222048525";
    private StringRequest request;

    Float transitionUp = -350f;

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (getSinchServiceInterface() != null) {
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().setStartListener(_Splash.this);
            }

        }
        //  AppEventsLogger.activateApp(this);

    }

    @Override
    protected void onServiceConnected() {
        if (getIntent().getBooleanExtra("check", true)) {
            int a = BuildConfig.VERSION_CODE;
            GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    String res = (String) result;
                    if (res.equals("\"True\"")) {
                        new SweetAlertDialog(_Splash.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        finish();

                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }

                                    }
                                })
                                .setContentText("Your application is outdated, kindly download the latest version")
                                .show();
                    } else {
                        if (!getSinchServiceInterface().isStarted()) {
                            getSinchServiceInterface().setStartListener(_Splash.this);
                        }
                        if (ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(_Splash.this,
                                    new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    1);
                            return;
                        }
                        if (MySingleton.getInstance().isConnectedButton(_Splash.this)) {
                            checkInfo();
                        } else {
                            if (ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(_Splash.this,
                                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        1);
                            } else
                                startEverything();
                        }
                    }

                }
            }, "VersionCheck/" + a + "__" + "1");
            asyncTask.execute();
        } else {
            if (ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(_Splash.this,
                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                return;
            }
            if (MySingleton.getInstance().isConnectedButton(_Splash.this)) {
                checkInfo();
            } else {
                startEverything();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        com.batch.android.Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        com.batch.android.Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        /*SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);*/

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        transitionUp = (float)(-(dm.heightPixels/4));

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live

        // CallbackManager callbackManager = CallbackManager.Factory.create();
        lang = (ToggleSwitch) findViewById(R.id.lang);


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        final InputFilter[] filter11 = new InputFilter[]{new InputFilter.LengthFilter(11)};
        final InputFilter[] filter99 = new InputFilter[]{new InputFilter.LengthFilter(99)};


        lang.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                if (position==0)
                {
                    ((TextView) findViewById(R.id.regtext)).setText("Login to WebDoc");
                    ((EditText) findViewById(R.id.password)).setGravity(Gravity.LEFT);

                    ((EditText) findViewById(R.id.password)).setHint("Password");
                    ((Button) findViewById(R.id.login)).setText("Login");
                    ((Button) findViewById(R.id.sign_in_to_register)).setText("Register as a new user");
                    ((Button) findViewById(R.id.sign_in_to_forgot_password)).setText("Forgot Password");

                    if (toggleSwitch.getCheckedTogglePosition()==0)
                    {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("Phone Number");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    }

                    else {

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("Email Address");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }
                }

                else
                {

                    ((TextView) findViewById(R.id.regtext)).setTypeface(tf);;
                    ((TextView) findViewById(R.id.regtext)).setText("لاگ ان کریں");
                    ((EditText) findViewById(R.id.password)).setTypeface(tf);
                    ((EditText) findViewById(R.id.password)).setGravity(Gravity.RIGHT);

                    ((EditText) findViewById(R.id.password)).setHint("پاس ورڈ");
                    ((Button) findViewById(R.id.login)).setTypeface(tf);
                    ((Button) findViewById(R.id.login)).setText("لاگ ان");
                    ((Button) findViewById(R.id.sign_in_to_register)).setTypeface(tf);
                    ((Button) findViewById(R.id.sign_in_to_register)).setText("نئے صارف کے طور پر رجسٹر کریں");
                    ((Button) findViewById(R.id.sign_in_to_forgot_password)).setTypeface(tf);
                    ((Button) findViewById(R.id.sign_in_to_forgot_password)).setText("پاسورڈ بھولنے کی صورت میں");


                    if (toggleSwitch.getCheckedTogglePosition()==0)
                    {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("موبائل فون نمبر");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    }
                    else {


                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("ای میل اڈریس");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }

                }
            }
        });

       /* try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.webdoc",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
*/
        //      private static final String EMAIL = "email";
       /* LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserDetails(loginResult);
                String a= loginResult.getAccessToken().getUserId();
                Toast.makeText(_Splash.this,a,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(_Splash.this,"no",Toast.LENGTH_LONG).show();
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(_Splash.this,"no0",Toast.LENGTH_LONG).show();
                // App code
            }
        });
*/




       /* loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
*/



        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
*/


        fa = this;

        requestQueue = Volley.newRequestQueue(this);

        startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_animation);


        rl = (RelativeLayout) findViewById(R.id.rl1);
        iv = (LinearLayout) findViewById(R.id.ivLogo);

        iv.startAnimation(startAnimation);

        /*new DBHelper(this).deletePatients();
        new DBHelper(this).deleteMessagess();

        for(int i=0;i<50;i++)
        new DBHelper(this).deleteNotification_(i+"");*/
    }





    public void startEverything() {


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);


        final InputFilter[] filter11 = new InputFilter[]{new InputFilter.LengthFilter(11)};
        final InputFilter[] filter99 = new InputFilter[]{new InputFilter.LengthFilter(99)};

        toggleSwitch = (ToggleSwitch) findViewById(R.id.log);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (position == 0) {
                    if (lang.getCheckedTogglePosition() == 0){
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("Phone Number");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);}
                    else{
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("موبائل فون نمبر");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    }



                } else {
                    if (lang.getCheckedTogglePosition() == 0) {

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("Email Address");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }
                    else{

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("ای میل اڈریس");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }

                }
            }
        });

        /*if (MySingleton.getInstance().isConnected(this)) {
            checkInfo();
        }*/

        if (MySingleton.getInstance().getBoolean()) {

            iv.postDelayed(new Runnable() {
                public void run() {

                    final TranslateAnimation anim = new TranslateAnimation(0, 0, 0, transitionUp);
                    anim.setDuration(2000); //2000
                    anim.setFillAfter(true);
                    //ViewGroup.LayoutParams params = iv.getLayoutParams();
                    //params.width = 100;
                    //iv.setLayoutParams(params);
                    iv.startAnimation(anim);
                    anim.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            rl.setTranslationY(transitionUp);
                            startAnimation.setInterpolator(new AccelerateInterpolator());
                            View root = findViewById(R.id.rl1);
                            ObjectAnimator fadeIn = ObjectAnimator.ofFloat(root, "alpha", 0f, 1f);
                            fadeIn.setDuration(1500);
                            fadeIn.start();

                            findViewById(R.id.lang).animate().alpha(1.0f).setDuration(1000);
                            // rl.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }

                    });


                }
            }, 2000);
        } else {
            iv.setTranslationY(transitionUp);
            rl.setTranslationY(transitionUp);

            rl.setAlpha(1f);
            lang.setVisibility(View.VISIBLE);
        }
    }

    SweetAlertDialog pDialog;
    String userName, password;

    public void openDrawer(View view) {
        if (MySingleton.getInstance().isConnected(this)) {
            pDialog = new SweetAlertDialog(_Splash.this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
            pDialog.setTitleText("Logging In - Please Wait");
            pDialog.setCancelable(false);
            pDialog.show();

            TextView uname = (TextView) findViewById(R.id.email);
            TextView pass = (TextView) findViewById(R.id.password);

            if (uname.getText().toString().equals("")) {
                userName = "patient@webdoc.com.pk";
            } else {
                if (toggleSwitch.getCheckedTogglePosition() == 0)
                    userName = uname.getText().toString() + "@webdoc.com.pk";
                else
                    userName = uname.getText().toString();
            }

            password = pass.getText().toString();

            GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    if (result == null) {
                        pDialog.dismiss();
                        new SweetAlertDialog(_Splash.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    }

                    if (result.equals("\"1\"")) {
                        if (!getSinchServiceInterface().isStarted()) {
                            getSinchServiceInterface().startClient(userName);
                        }
                    } else if (result.equals("")) {
                    } else {
                        pDialog.dismiss();
                        Snackbar.make(findViewById(android.R.id.content), "Invalid Email or Password", Snackbar.LENGTH_LONG)
                                .setActionTextColor(Color.RED)
                                .show();
                    }

                }
            }, "Login/" + userName + "/" + password + "__" + "1");
            asyncTask.execute();


        }
    }

    public void saveInfo(String name, String pass) {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.putString("Password", pass);
        editor.apply();
//        Intent n=new Intent(_Splash.this,MainPage.class).putExtra("",userName);
    }

    private void checkInfo() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        userName = sharedpref.getString("UserName", "0");
        password = sharedpref.getString("Password", "0");

        if (!userName.equals("0") && !password.equals("0")) {
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().startClient(userName);
            } else {
                //Toast.makeText(getApplicationContext(), "Line # 304", Toast.LENGTH_SHORT).show();
                //if (!getIntent().getBooleanExtra("check", true)) {
                MySingleton.getInstance().setUserId(userName);
                saveInfo(userName, password);
                startActivity(new Intent(getApplicationContext(), _MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                //}
            }

        } else {
            if (ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)
                ActivityCompat.requestPermissions(_Splash.this,
                        new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE},
                        1);
            else
                startEverything();
        }
    }

    public void openRegister(View view) {
        startActivity(new Intent(this, _RegisterActivity.class));
    }

    public void openForgot(View view) {
        startActivity(new Intent(this, _ForgotActivity.class));
    }

    @Override
    public void onStartFailed(SinchError error) {
        if (pDialog != null)
            pDialog.dismiss();
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        MySingleton.getInstance().setUserId(userName);
        saveInfo(userName, password);
        startActivity(new Intent(getApplicationContext(), MainPage.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra(userName, ""));
        if (pDialog != null)
            pDialog.dismiss();
       /* GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);*/
        //finish();
        //ActivityCompat.finishAffinity(_Splash.this);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED && grantResults[4] == PackageManager.PERMISSION_GRANTED) {

                    startEverything();
                } else {
                    new SweetAlertDialog(_Splash.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("Please Allow All Permissions")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    if (ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(_Splash.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(_Splash.this,
                                                new String[]{android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.CAMERA, android.Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                1);
                                    } else
                                        startEverything();
                                }
                            })
                            .show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //Toast.makeText(_MainActivity.this, "Permission denied to read Record Audio", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

*/

   /* private void getUserDetails(LoginResult loginResult) {

        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        Intent intent = new Intent(_Splash.this,
                                UserProfile.class);
                        intent.putExtra("userProfile", json_object.toString());
                        startActivity(intent);
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }*/


    /*@Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }*/

}



