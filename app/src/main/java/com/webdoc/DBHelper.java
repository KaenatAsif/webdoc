package com.webdoc;

/**
 * Created by faheem on 25/07/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.batch.android.Batch;
import com.batch.android.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "WebDoc.db";
    public static final String CONTACTS_TABLE_NAME = "contacts";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";

    private HashMap hp;



    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table PatientList " +
                        "(id integer primary key, name varchar, read varchar)"
        );
        db.execSQL(
                "create table MessageList " +
                        "(id varchar primary key, body varchar, recipient varchar, sender varchar, timestamp varchar, direction varchar ,read varchar)"
        );
        db.execSQL(
                "create table AllNotifications " +
                        "(id integer primary key, name varchar, type varchar)"
        );

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
/////////////////////////////batch/////////////////////////////////////////////////

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS PatientList");
        db.execSQL("DROP TABLE IF EXISTS MessageList");
        onCreate(db);
    }

    public boolean insertPatient(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("read", "0");

        db.insert("PatientList", null, contentValues);
        db.close();
        return true;
    }

    public boolean insertMessage(String id, String body, String recipient, String sender, String timestamp, String direction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("body", body);
        contentValues.put("recipient", recipient);
        contentValues.put("sender", sender);
        contentValues.put("timestamp", timestamp);
        contentValues.put("direction", direction);
        contentValues.put("read", "0");


        //db.insert("MessageList", null, contentValues);
        db.insertWithOnConflict("MessageList", null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);

        db.close();
        return true;
    }

    public boolean messagesRead(String name, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", value);

        db.update("PatientList", contentValues, "name = ?", new String[]{name});
        db.close();
        return true;
    }

    public boolean messages__Read(String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("read", "1");

        db.update("MessageList", contentValues, "sender = ?", new String[]{name});
        db.close();
        return true;
    }

    public boolean getData(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        int count = -1;
        Cursor c = null;
        try {
            String query = "SELECT COUNT(*) FROM PatientList WHERE name " + "= ?";
            c = db.rawQuery(query, new String[]{name});
            if (c.moveToFirst()) {
                count = c.getInt(0);
            }
            return count > 0;
        } finally {
            if (c != null) {
                c.close();
            }
            db.close();
        }
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        db.close();
        return numRows;
    }

    public boolean updatePatient(Integer id, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);

        db.update("PatientList", contentValues, "id = ? ", new String[]{Integer.toString(id)});
        db.close();
        return true;
    }

    public Integer deletePatient(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.close();
        return db.delete("PatientList",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public void deletePatients() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("PatientList", "", new String[]{});
        db.close();
    }

    public void deleteMessagess() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("MessageList", "", new String[]{});
        db.close();
    }

    public ArrayList<String> getAllPatients() {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from PatientList WHERE read = ?", new String[]{"0"});
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)));
            res.moveToNext();
        }
        db.close();
        return array_list;
    }

    public List<List<String>> getAllMessages() {
        List<List<String>> listOfLists = new ArrayList<List<String>>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from MessageList",null);
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            String id = res.getString(res.getColumnIndex("id"));
            String body = res.getString(res.getColumnIndex("body"));
            String recipient = res.getString(res.getColumnIndex("recipient"));
            String sender = res.getString(res.getColumnIndex("sender"));
            String timestamp = res.getString(res.getColumnIndex("timestamp"));
            String direction = res.getString(res.getColumnIndex("direction"));

            listOfLists.add(new ArrayList<String>(Arrays.asList(id, body, recipient, sender, timestamp, direction)));
            res.moveToNext();
        }
        db.close();
        return listOfLists;
    }

    public boolean insertNotification_(String id, String name, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("type", type);

        db.insert("AllNotifications", null, contentValues);
        db.close();
        return true;
    }

    public void deleteNotification_(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("AllNotifications", "id = ?", new String[]{id});
    }

    public int getNotification_(String name, String type) {
        int ans = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select MAX(id) from AllNotifications WHERE name = ? and type = ?", new String[]{name, type});
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            try {
                ans = Integer.parseInt(res.getString(0));
            } catch (NumberFormatException e) {
                ans = 0;
            }

            res.moveToNext();
        }
        db.close();
        return ans;
    }

    public List<Integer> getAllNotifications_(String name) {
        List<Integer> listOfLists = new ArrayList<Integer>();
        int id = 0;

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from AllNotifications WHERE name = ?", new String[]{name});
        res.moveToFirst();


        while (res.isAfterLast() == false) {
            id = Integer.parseInt(res.getString(res.getColumnIndex("id")));

            listOfLists.add(id);
            res.moveToNext();
        }
        db.close();
        return listOfLists;
    }
}