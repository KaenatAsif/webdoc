package com.webdoc;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONArray;
import org.json.JSONException;


/**
 * Created by faheem on 2/2/17.
 */
public class __DoctorProfile extends Activity {


    EditText fullNameET, mailET, qualificationET, sloganET;
    RatingBar dobET;

    Button button_avalibility_slots;

    ImageView profile_img;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_doctor_profile);


        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        JSONArray jarr = MySingleton.getInstance().getArray(9);
        JSONArray jinner = null;


        fullNameET = (EditText) findViewById(R.id.NameET);
        mailET = (EditText) findViewById(R.id.EmailET);
        qualificationET = (EditText) findViewById(R.id.QualificationET);
        sloganET = (EditText) findViewById(R.id.SloganET);

        dobET = (RatingBar) findViewById(R.id.RatD);
        dobET.setBackgroundColor(getResources().getColor(R.color.backgroundcolor));
        profile_img = (ImageView) findViewById(R.id.toolbarImage);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .75));
        String uname = getIntent().getStringExtra("UserName");
        boolean appointment_ = getIntent().getBooleanExtra("appointment?",false);

        if(appointment_)
            button_avalibility_slots.setVisibility(View.VISIBLE);


        try {
            for (int i = 0; i < jarr.length(); i++) {
                jinner = (JSONArray) jarr.get(i);
                if (jinner.get(3).equals(uname))
                    break;
            }
            fullNameET.setText(jinner.get(0) + " " + jinner.get(1));
            mailET.setText(jinner.get(5) + "");
            dobET.setRating((float) getIntent().getDoubleExtra("Rating", 4));
            qualificationET.setText(jinner.get(6) + "");
            sloganET.setText(jinner.get(7) + "");

            Glide.with(getApplicationContext())
                    .load(jinner.get(9) + "")
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(profile_img);

            //UrlImageViewHelper.setUrlDrawable(, );

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
