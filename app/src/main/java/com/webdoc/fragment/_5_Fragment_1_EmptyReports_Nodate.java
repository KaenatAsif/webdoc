package com.webdoc.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.webdoc.AsyncResponse;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.WCFHandler;
import com.webdoc.__PopupConsltDetail;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _5_Fragment_1_EmptyReports_Nodate extends Fragment {

    Spinner sDoctor, sServiceFee;
    LinearLayout lDateTo, lDateFrom;

    private int mYear, mMonth, mDay;

    String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    ArrayList<ListData> mDataList;
    ArrayList Spinner;
    ListView lv;
    LinearLayout tb;
    WCFHandler wcf;
    JSONArray jdr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_5_fragment_1_emptyreports_nodate, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        sDoctor = view.findViewById(R.id.spinner_Doctors);
        sServiceFee = view.findViewById(R.id.spinner_ServiceFee);
        lDateTo = view.findViewById(R.id.openDateTo);
        lDateFrom = view.findViewById(R.id.openDateFrom);
        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);


        lv = view.findViewById(R.id.listView);
        tb = view.findViewById(R.id.TableYesDate);

        mDataList = new ArrayList<ListData>();
        Spinner = new ArrayList();
        wcf = new WCFHandler(getActivity());

        jdr = MySingleton.getInstance().getArray(7);//wcf.GetJsonResult("DoctorList");

        Spinner.add("- All -");

        for(int i=0;i<jdr.length();i++)
        {
            try {
                Spinner.add(jdr.getJSONArray(i).get(0) + " "+jdr.getJSONArray(i).get(1));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayAdapter<String> adapterD = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_item,Spinner);
        adapterD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sDoctor.setAdapter(adapterD);

        view.findViewById(R.id.fetchReportDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
                pDialog.setTitleText("Fetching - Please Wait");
                pDialog.setCancelable(false);
                pDialog.show();

                mDataList.clear();
                lv.setAdapter(new SampleAdapter());
                tb.setVisibility(View.INVISIBLE);

                int dIndex = sDoctor.getSelectedItemPosition();
                dIndex--;
                String dID = "";
                if (dIndex == -1)
                    dID = "-1";
                else {
                    try {
                        dID = jdr.getJSONArray(dIndex).get(3) + "";
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                String serFee;
                if (sServiceFee.getSelectedItemPosition() != 0)
                    serFee = (sServiceFee.getSelectedItem() + "").split(" ")[0];
                else
                    serFee = "-1";




                GetProfile asyncTask = new GetProfile(getContext(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;
                        if (jarr == null) {
                            pDialog.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();

                        } else if (jarr.length() == 0) {
                            pDialog.dismiss();
                            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("No Consultations Found")
                                    .show();
                            return;
                        } else {
                            tb.setVisibility(View.VISIBLE);

                            for (int i = 0; i < jarr.length(); i++) {
                                try {
                                    JSONArray jobj = jarr.getJSONArray(i);
                                    mDataList.add(new ListData(jobj.get(5) + "", jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            lv.setAdapter(new SampleAdapter());
                            pDialog.dismiss();
                        }

                    }
                },"ConsultationReportWithoutdate/" + MySingleton.getInstance().getUserId() + "/" + dID + "/" + serFee+"__"+"0");
                asyncTask.execute();
                /*JSONArray jarr = wcf.GetJsonResult("ConsultationReportWithoutdate/" + MySingleton.getInstance().getUserId() + "/" + dID + "/" + serFee);
                if (jarr == null) {
                    pDialog.dismiss();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                            .show();

                } else if (jarr.length() == 0) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Consultations Found")
                            .show();
                    pDialog.dismiss();
                    return;
                } else {
                    tb.setVisibility(View.VISIBLE);

                    for (int i = 0; i < jarr.length(); i++) {
                        try {
                            JSONArray jobj = jarr.getJSONArray(i);
                            mDataList.add(new ListData(jobj.get(5) + "", jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    lv.setAdapter(new SampleAdapter());
                    pDialog.dismiss();
                }*/
            }
        });

        if (MySingleton.getInstance().isConnectedButton()) {

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent i_ = new Intent(getContext(), __PopupConsltDetail.class);
                    i_.putExtra("ID", mDataList.get(i).ID);
                    startActivity(i_);
                }
            });
        } else
            setHasOptionsMenu(true);
    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_pentadruple, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            String[] parts = item.Date.split("-");

            holder.tvDate.setText(parts[0]+"\n- "+parts[1]+" -\n"+parts[2]);
            holder.tvDoctor.setText(item.Doctor);
            holder.tvConsultationType.setText(item.Consultation_Type);
            holder.tvPaymentMethod.setText(item.Payment_Method);
            holder.tvCharges.setText(item.Charges);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvConsultationType;
        private TextView tvPaymentMethod;
        private TextView tvCharges;

        private ViewHolder(View view) {
            this.view = view;
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvDoctor = (TextView) view.findViewById(R.id.tvDoctor);
            tvConsultationType = (TextView) view.findViewById(R.id.tvConsultationType);
            tvPaymentMethod = (TextView) view.findViewById(R.id.tvPaymentMethod);
            tvCharges = (TextView) view.findViewById(R.id.tvCharges);
        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Consultation_Type;
        private String Payment_Method;
        private String Charges;

        public ListData(String ID, String D, String Doc, String CT, String PM, String C) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Consultation_Type = CT;
            this.Payment_Method = PM;
            this.Charges = C;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }
}