package com.webdoc.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.webdoc.MySingleton;
import com.webdoc.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class _4_Fragment_2_CallSummary_HealthCards extends Fragment {

    ArrayList<ListData> main;
    JSONArray jarr;

    SimpleDateFormat simpleDateFormat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_4_fragment_2_callsummary_health_cards, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        simpleDateFormat = new SimpleDateFormat("M/dd/yyyy");





        main = new ArrayList<>();
        jarr = MySingleton.getInstance().getCallSummaryArray(2);

        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        for (int i = 0; i < jarr.length(); i++) {
            try {
                String name = jarr.getJSONArray(i).getString(0);
                 String vmade = jarr.getJSONArray(i).getString(1);
                String amade = jarr.getJSONArray(i).getString(2);
                String vtotal = jarr.getJSONArray(i).getString(3);
                String atotal = jarr.getJSONArray(i).getString(4);
                String expiryFrom = jarr.getJSONArray(i).getString(5).replace(" 12:00:00 AM", "");
                String expiryTo = jarr.getJSONArray(i).getString(6).replace(" 12:00:00 AM", "");





                main.add(new ListData(i + "", name, vmade, amade, vtotal, atotal, expiryFrom, expiryTo));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }






        ListView lv = view.findViewById(R.id.listView);
        lv.setAdapter(new SampleAdapter());

        if (jarr.length() == 0)
            lv.setVisibility(View.GONE);
        else
            view.findViewById(R.id.nrf).setVisibility(View.GONE);

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_callsummary, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);

            holder.Description.setText(item.Description);


            ///////////////////////////////////////URDU////////////////////////////////////////////////
            // Give path to the Font location
            String Path2font = "DroidNaskh-Regular.ttf";
            final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

            if (MySingleton.getInstance().getLang()) {
                ((TextView) holder.view.findViewById(R.id.TVvideo)).setTypeface(tf);
                ((TextView) holder.view.findViewById(R.id.TVvideo)).setText("ویڈیو کالز");

                ((TextView) holder.view.findViewById(R.id.TVAudio)).setTypeface(tf);
                ((TextView) holder.view.findViewById(R.id.TVAudio)).setText("آڈیو کالز");

                ((TextView) holder.view.findViewById(R.id.TVExpiry)).setTypeface(tf);
                ((TextView) holder.view.findViewById(R.id.TVExpiry)).setText("ایکسپائری");




            }


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                String textV = "<b><font color=#72BB53>" + item.VIDEOMADE + "</font></b><font color=#000>/" + item.VIDEOTOTAl + "</font>";
                String textA = "<b><font color=#FFD81A>" + item.AUDIOMADE + "</font></b><font color=#000>/" + item.AUDIOTOTAL + "</font>";

                holder.VIDEOSTATUS.setText(Html.fromHtml(textV, Html.FROM_HTML_MODE_LEGACY));
                holder.AUDIOSTATUS.setText(Html.fromHtml(textA, Html.FROM_HTML_MODE_LEGACY));
            } else {
                String textV = "<b><font color=#72BB53>" + item.VIDEOMADE + "</font></b><font color=#000>/" + item.VIDEOTOTAl + "</font>";
                String textA = "<b><font color=#FFD81A>" + item.AUDIOMADE + "</font></b><font color=#000>/" + item.AUDIOTOTAL + "</font>";

                holder.VIDEOSTATUS.setText(Html.fromHtml(textV));
                holder.AUDIOSTATUS.setText(Html.fromHtml(textA));
            }

            holder.VIDEOBAR.setProgress(Integer.parseInt(item.VIDEOMADE));
            holder.VIDEOBAR.setMax(Integer.parseInt(item.VIDEOTOTAl));

            holder.AUDIOBAR.setProgress(Integer.parseInt(item.AUDIOMADE));
            holder.AUDIOBAR.setMax(Integer.parseInt(item.AUDIOTOTAL));

            if (item.Description.equals("Silver Card"))
                holder.MIDDLELAYOUT.setVisibility(View.GONE);
            else
                holder.MIDDLELAYOUT.setVisibility(View.VISIBLE);

            Date date1, date2;
            try {
                date1 = Calendar.getInstance().getTime();
                date2 = simpleDateFormat.parse(item.EXPIRY);

                String days = printDifference(date1, date2);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    String textE = "<b><font color=#CB3439>" + days + "</font></b><font color=#000>/365</font>";
                    holder.EXPIRYSTATUS.setText(Html.fromHtml(textE, Html.FROM_HTML_MODE_LEGACY));
                } else {
                    String textE = "<b><font color=#CB3439>" + days + "</font></b><font color=#000>/365</font>";
                    holder.EXPIRYSTATUS.setText(Html.fromHtml(textE));
                }

                int progress = Integer.parseInt(days) / 10;
                int max = 365 / 10;

                holder.EXPIRYBAR.setProgress(progress);
                holder.EXPIRYBAR.setMax(max);
                holder.ACTIVATION.setText(item.ACTIVATION);
                holder.EXPIRY.setText(item.EXPIRY);
/*

                Date date3 = Calendar.getInstance().getTime();
                Date date4 = simpleDateFormat.parse(item.EXPIRY);

                notification(date3, date4);

*/


            } catch (ParseException e) {
                e.printStackTrace();
            }


            return convertView;

        }
    }

    private static class ViewHolder {

        private View view;

        private TextView Description;

        private LinearLayout MIDDLELAYOUT;
        private TextView VIDEOSTATUS;
        private NumberProgressBar VIDEOBAR;

        private TextView AUDIOSTATUS;
        private NumberProgressBar AUDIOBAR;

        private TextView EXPIRYSTATUS;
        private NumberProgressBar EXPIRYBAR;

        private TextView ACTIVATION;
        private TextView EXPIRY;

        private ViewHolder(View view) {
            this.view = view;
            Description = view.findViewById(R.id.Name);
            MIDDLELAYOUT = view.findViewById(R.id.callsummary_middlepart);
            VIDEOSTATUS = view.findViewById(R.id.Made_Total_V);
            AUDIOSTATUS = view.findViewById(R.id.Made_Total_A);
            EXPIRYSTATUS = view.findViewById(R.id.Made_Total_E);
            ACTIVATION = view.findViewById(R.id.TVExpiryFrom);
            EXPIRY = view.findViewById(R.id.TVExpiryTo);

            VIDEOBAR = (NumberProgressBar) view.findViewById(R.id.number_progress_bar_video);
            AUDIOBAR = (NumberProgressBar) view.findViewById(R.id.number_progress_bar_audio);
            EXPIRYBAR = (NumberProgressBar) view.findViewById(R.id.number_progress_bar_expiry);

        }
    }

    private class ListData {
        private String ID;
        private String Description;
        private String VIDEOMADE;
        private String AUDIOMADE;
        private String VIDEOTOTAl;
        private String AUDIOTOTAL;
        private String ACTIVATION;
        private String EXPIRY;
        private String Status;


        //["Sehat Sahara Plus", "2", "2", "10", "16", "7\/19\/2017 12:00:00 AM", "7\/19\/2018 12:00:00 AM"]
        public ListData(String id, String Description, String videoMade, String audioMade, String videoTotal, String audioTotal, String From, String To) {
            this.ID = id;
            this.Description = Description;
            this.VIDEOMADE = videoMade;
            this.AUDIOMADE = audioMade;
            this.VIDEOTOTAl = videoTotal;
            this.AUDIOTOTAL = audioTotal;
            this.ACTIVATION = From;
            this.EXPIRY = To;
        }
    }

    public String printDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if (elapsedDays > 0)
            return elapsedDays + "";
        else {
            elapsedDays = 365;
            return elapsedDays + "";
        }



/*        if (elapsedDays<=13)
        {
            PugNotification.with(getActivity())
                    .load()
                    .identifier(13)
                    .title("Message")
                    .message("Expired")
                    .bigTextStyle("From : " + sender)
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.mipmap.ic_launcher)
                    .priority(Notification.PRIORITY_HIGH)
                    .autoCancel(true)
                    .flags(Notification.DEFAULT_VIBRATE)
                    .vibrate(new long[]{100})
                    .simple()
                    .build();
        }*/
    }


/*public String notification(Date startDate,Date endDate)
{
    long different = endDate.getTime() - startDate.getTime();

    long secondsInMilli = 1000;
    long minutesInMilli = secondsInMilli * 60;
    long hoursInMilli = minutesInMilli * 60;
    long daysInMilli = hoursInMilli * 24;

    long elapsedDays = different / daysInMilli;
    different = different % daysInMilli;

    long elapsedHours = different / hoursInMilli;
    different = different % hoursInMilli;

    long elapsedMinutes = different / minutesInMilli;
    different = different % minutesInMilli;

    long elapsedSeconds = different / secondsInMilli;

    if (elapsedDays<=25)
    {
        Intent intent = new Intent(getActivity(), MainPage.class);
// use System.currentTimeMillis() to have a unique ID for the pending intent
        PendingIntent pIntent = PendingIntent.getActivity(getActivity(), (int) System.currentTimeMillis(), intent, 0);

// build notification
// the addAction re-use the same intent to keep the example short
        Notification n = new Notification.Builder(getActivity())
                .setContentTitle("New mail from " + "test@gmail.com")
                .setContentText("Subject")
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .addAction(R.drawable.common_google_signin_btn_icon_dark, "Call", pIntent)
                .addAction(R.drawable.common_google_signin_btn_icon_dark, "More", pIntent)
                .addAction(R.drawable.common_google_signin_btn_icon_dark, "And more", pIntent).build();


        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(0, n);


    }


    return null;
}*/

}