package com.webdoc.fragment.navigation;

/**
 * @author msahakyan
 */

public interface NavigationManager {

    void showFragmentGeneralProfile(String title);

    void showFragmentConditions(String title);

    void showFragmentAllergies(String title);

    void showFragmentMedications(String title);

    void showFragmentImmunizations(String title);

    void showFragmentProcedures(String title);

    void showFragmentInsuranceProducts(String title);

    void showFragmentCorporatePanel(String title);

    void showFragmentHealthCards(String title);

    void showFragmentClaims(String title);

    void showFragmentConsultationReports(String title);

    void showFragmentCallSummary(String title);

    void showFragmentDoctorList(String title);

    void showFragmentFamilyRegistration(String title);
}
