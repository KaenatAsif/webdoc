package com.webdoc.fragment.navigation;

import android.annotation.SuppressLint;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.webdoc.R;
import com.webdoc._MainActivity;
import com.webdoc.fragment.FragmentDoctorList;
import com.webdoc.fragment._1_FragmentGeneralProfile;
import com.webdoc.fragment._2_Fragment_1_Conditions;
import com.webdoc.fragment._2_Fragment_2_Allergies;
import com.webdoc.fragment._2_Fragment_3_Medications;
import com.webdoc.fragment._2_Fragment_4_Immunizations;
import com.webdoc.fragment._2_Fragment_5_Procedures;
import com.webdoc.fragment._3_Fragment_1_InsuranceProducts;
import com.webdoc.fragment._3_Fragment_2_CorporatePanel;
import com.webdoc.fragment._2o5_Fragment_HealthCards;
import com.webdoc.fragment._3_Fragment_3_Claims;
import com.webdoc.fragment._3_Fragment_4_FamilyRegistration;
import com.webdoc.fragment._4_Fragment_1_ConsultationReports;
import com.webdoc.fragment._4_Fragment_2_CallSummary;


/**
 * @author msahakyan
 */

public class FragmentNavigationManager implements NavigationManager {



    private static FragmentNavigationManager sInstance;

    private FragmentManager mFragmentManager;
    private _MainActivity mActivity;

    public static FragmentNavigationManager obtain(_MainActivity activity) {
        if (sInstance == null) {
            sInstance = new FragmentNavigationManager();
        }
        sInstance.configure(activity);
        return sInstance;
    }

    private void configure(_MainActivity activity) {
        mActivity = activity;
        mFragmentManager = mActivity.getSupportFragmentManager();
    }

    @Override
    public void showFragmentGeneralProfile(String title) {
        showFragment(_1_FragmentGeneralProfile.newInstance(title) , false);
    }

    @Override
    public void showFragmentConditions(String title) {
        showFragment(_2_Fragment_1_Conditions.newInstance(title), true);
    }

    @Override
    public void showFragmentAllergies(String title) {
        showFragment(_2_Fragment_2_Allergies.newInstance(title), true);
    }

    @Override
    public void showFragmentFamilyRegistration(String title) {
        showFragment(_3_Fragment_4_FamilyRegistration.newInstance(title), true);
    }


    @Override
    public void showFragmentMedications(String title){
        showFragment(_2_Fragment_3_Medications.newInstance(title), true);
    }

    @Override
    public void showFragmentImmunizations(String title) {
        showFragment(_2_Fragment_4_Immunizations.newInstance(title), true);
    }

    @Override
    public void showFragmentProcedures(String title) {
        showFragment(_2_Fragment_5_Procedures.newInstance(title), true);
    }

    @Override
    public void showFragmentInsuranceProducts(String title) {
        showFragment(_3_Fragment_1_InsuranceProducts.newInstance(title), true);
    }

    @Override
    public void showFragmentCorporatePanel(String title) {
        showFragment(_3_Fragment_2_CorporatePanel.newInstance(title), true);
    }

    @Override
    public void showFragmentHealthCards(String title) {
        showFragment(_2o5_Fragment_HealthCards.newInstance(title), true);
    }

    @Override
    public void showFragmentClaims(String title) {
        showFragment(_3_Fragment_3_Claims.newInstance(title), true);
    }

    @Override
    public void showFragmentDoctorList(String title) {
        showFragment(FragmentDoctorList.newInstance(title),true);
    }

    @Override
    public void showFragmentConsultationReports(String title) {
        showFragment(_4_Fragment_1_ConsultationReports.newInstance(title), true);
    }

    @Override
    public void showFragmentCallSummary(String title) {
        showFragment(_4_Fragment_2_CallSummary.newInstance(title), true);
    }

    private void showFragment(Fragment fragment, boolean allowStateLoss) {
        FragmentManager fm = mFragmentManager;

        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction()
            .replace(R.id.container, fragment);

        ft.addToBackStack(null);

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }
}
