package com.webdoc.fragment;

/**
 * Created by faheem on 13/12/2017.
 */

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;

import org.json.JSONArray;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _4_Fragment_2_CallSummary extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    public _4_Fragment_2_CallSummary() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _5_Fragment_1_EmptyReports.
     */
    static _4_Fragment_2_CallSummary a6Fragment2CallSummary = new _4_Fragment_2_CallSummary();

    public static _4_Fragment_2_CallSummary newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            a6Fragment2CallSummary.setArguments(args);
        } catch (IllegalStateException e) {
            a6Fragment2CallSummary = new _4_Fragment_2_CallSummary();
            a6Fragment2CallSummary.setArguments(args);
        }
        return a6Fragment2CallSummary;
    }

    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        /////////////////////////////////////////////////////////////////////////////////
     /*   /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////
*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_4_fragment_2_callsummary, container,
                false);
        return v;
    }

    JSONArray jarr;

    TabLayout tabLayout;
    ViewPager viewPager;
    _4_Fragment_2_CallSummary_Insurance _I_;
    _4_Fragment_2_CallSummary_HealthCards _HC_;
    _4_Fragment_2_CallSummary_Corporate _C_;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _MainActivity.hideDoctorLayout();

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.main_tab_content);

        GetSingle async = new GetSingle(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                    jarr = (JSONArray) result;

                    if (jarr == null) {
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    } else {
                        MySingleton.getInstance().setCallSummaryArray(jarr);

                        _I_ = new _4_Fragment_2_CallSummary_Insurance();
                        _HC_ = new _4_Fragment_2_CallSummary_HealthCards();
                        _C_ = new _4_Fragment_2_CallSummary_Corporate();

                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);

                    }

            }
        }, "getCallBalance/" + MySingleton.getInstance().getUserId() + "__" + "0");
        async.execute();


    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter Adapter = new ViewPageAdapter(getChildFragmentManager());
        if (MySingleton.getInstance().getLang()) {

            Adapter.addFragments(_HC_, "ہیلتھ کارڈ");
            Adapter.addFragments(_I_, "انشورنس");
            Adapter.addFragments(_C_, "کارپوریٹ");
        } else {
            Adapter.addFragments(_HC_, "Health Cards");
            Adapter.addFragments(_I_, "Insurance");
            Adapter.addFragments(_C_, "Corporate");
        }
            //   Adapter.addFragments(new _4_Fragment_1_ConsultationReports_Nodate(), "Promo Code/ Free Reward");
            viewPager.setAdapter(Adapter);

    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> tabT = new ArrayList<>();

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragments(Fragment fragment, String titles) {
            this.fragments.add(fragment);
            this.tabT.add(titles);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public CharSequence getPageTitle(int position) {
            return tabT.get(position);
        }


    }
}