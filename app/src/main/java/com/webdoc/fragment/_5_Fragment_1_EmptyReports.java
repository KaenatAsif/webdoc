package com.webdoc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webdoc.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _5_Fragment_1_EmptyReports#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _5_Fragment_1_EmptyReports extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    public _5_Fragment_1_EmptyReports() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _5_Fragment_1_EmptyReports.
     */
    static _5_Fragment_1_EmptyReports a5Fragment1EmptyReports = new _5_Fragment_1_EmptyReports();

    public static _5_Fragment_1_EmptyReports newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
           a5Fragment1EmptyReports.setArguments(args);
        } catch (IllegalStateException e) {
            a5Fragment1EmptyReports = new _5_Fragment_1_EmptyReports();
            a5Fragment1EmptyReports.setArguments(args);
        }
        return a5Fragment1EmptyReports;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_5_fragment_1_emptyreports, container, false);
    }

    TabLayout tabLayout;
    ViewPager viewPager;
    _5_Fragment_1_EmptyReports_Nodate nd;
    _5_Fragment_1_EmptyReports_Yesdate yd;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.main_tab_content);

        nd = new _5_Fragment_1_EmptyReports_Nodate();
        yd =new _5_Fragment_1_EmptyReports_Yesdate();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);


    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPageAdapter Adapter = new ViewPageAdapter(getChildFragmentManager());

            Adapter.addFragments(nd, "Without Date");
            Adapter.addFragments(yd, "With Date");

        viewPager.setAdapter(Adapter);
    }

    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments = new ArrayList<>();
        ArrayList<String> tabT = new ArrayList<>();

        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragments(Fragment fragment, String titles) {
            this.fragments.add(fragment);
            this.tabT.add(titles);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        public CharSequence getPageTitle(int position) {
            return tabT.get(position);
        }


    }
}
