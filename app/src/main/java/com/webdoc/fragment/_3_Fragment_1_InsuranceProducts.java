package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupInsuranceEditAdd;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by faheem on 04/07/2017.
 */

public class _3_Fragment_1_InsuranceProducts extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    ArrayList<ListData> mDataList;
    View _v_;

    ListView lv;
    JSONArray jarr;
    boolean Network_b = false;

    public _3_Fragment_1_InsuranceProducts() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment FragmentDrama.
     */
    static _3_Fragment_1_InsuranceProducts fragment = new _3_Fragment_1_InsuranceProducts();

    public static _3_Fragment_1_InsuranceProducts newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _3_Fragment_1_InsuranceProducts();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////
*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_3_fragment_1_insuranceproducts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _v_ = view;

        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("انشورنس پراڈکٹ");

        }

            if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            view.findViewById(R.id.floatingActionButton).setVisibility(View.GONE);
            setHasOptionsMenu(true);
            Network_b = false;
        } else
            Network_b = true;

        lv = view.findViewById(R.id.listView);
        jarr = MySingleton.getInstance().getArray(6);

        mDataList = new ArrayList<ListData>();

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                mDataList.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (mDataList.get(0).Product.equals("")) {
            mDataList.remove(0);
            view.findViewById(R.id.topRow).setVisibility(View.GONE);
        }
        lv.setAdapter(new SampleAdapter());

        if (MySingleton.getInstance().isConnectedButton(getActivity())) {

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent i_ = new Intent(getContext(), __PopupInsuranceEditAdd.class);
                    i_.putExtra("Value", mDataList.get(i).Product);
                    i_.putExtra("Id", mDataList.get(i).ID);
                    i_.putExtra("MSISDN", mDataList.get(i).MSISDN);
                    i_.putExtra("Index", i + "");
                    startActivityForResult(i_, 101);
                }
            });
        } else
            setHasOptionsMenu(true);

        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.hide();
        fab.animate().translationY(fab.getHeight() + 500).setInterpolator(new AccelerateInterpolator(2)).start();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(), __PopupInsuranceEditAdd.class);
                startActivityForResult(i_, 101);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //fab.showMenuButton(true);
                if (Network_b) {
                    fab.show();
                    fab.bringToFront();
                    fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
                }
            }
        }, 1500);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                MySingleton.getInstance().editPopup(getContext());
            }
            if (resultCode == 50) {
                MySingleton.getInstance().addPopup(getContext());
            }
            jarr = MySingleton.getInstance().getArray(6);

            try {
                if (jarr.equals(new JSONArray("[[\"\",\"\",\"\",\"\",\"\"]]"))) {
                    MySingleton.getInstance().setIproduct(false);
                } else
                    MySingleton.getInstance().setIproduct(true);
            } catch (JSONException e) {
            }
            mDataList.clear();

            for (int i = 0; i < jarr.length(); i++) {
                try {
                    JSONArray jobj = jarr.getJSONArray(i);
                    mDataList.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (mDataList.get(0).Product.equals("")) {
                mDataList.remove(0);
                _v_.findViewById(R.id.topRow).setVisibility(View.GONE);
            } else
                _v_.findViewById(R.id.topRow).setVisibility(View.VISIBLE);

            lv.setAdapter(new SampleAdapter());
        }

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_quadruple, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            holder.tvInsuranceProduct.setText(item.Product);
            holder.tvInsuranceMSISDN.setText(item.MSISDN);
            holder.tvInsuranceActivation.setText(item.Activation);
            holder.tvInsuranceExpiry.setText(item.Expiry);
            if (Network_b)
                holder.edit.setTextColor(Color.RED);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView tvInsuranceProduct;
        private TextView tvInsuranceMSISDN;
        private TextView tvInsuranceActivation;
        private TextView tvInsuranceExpiry;
        private TextView edit;

        private ViewHolder(View view) {
            this.view = view;
            tvInsuranceProduct = (TextView) view.findViewById(R.id.tvInsuranceProduct);
            tvInsuranceMSISDN = (TextView) view.findViewById(R.id.tvInsuranceMSISDN);
            tvInsuranceActivation = (TextView) view.findViewById(R.id.tvInsuranceActivation);
            tvInsuranceExpiry = (TextView) view.findViewById(R.id.tvInsuranceExpiry);
            edit = view.findViewById(R.id.tvInsuranceEdit);
        }
    }

    private static class ListData {

        private String Product;
        private String MSISDN;
        private String Activation;
        private String Expiry;
        private String ID;

        public ListData(String ID, String Activation, String Expiry, String Product, String MSISDN) {
            this.ID = ID;
            this.Product = Product;
            this.MSISDN = MSISDN;
            this.Activation = Activation.replace(" 12:00AM", "");
            this.Expiry = Expiry.replace(" 12:00AM", "");
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }

}