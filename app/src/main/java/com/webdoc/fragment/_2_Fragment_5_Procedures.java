package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupEditAdd;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by faheem on 04/07/2017.
 */

public class _2_Fragment_5_Procedures extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    ArrayList<ListData> main;
    View _v_;

    ListView lv;
    JSONArray jarr;
    boolean Network_b = false;

    public _2_Fragment_5_Procedures() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment FragmentDrama.
     */
    static _2_Fragment_5_Procedures fragment = new _2_Fragment_5_Procedures();

    public static _2_Fragment_5_Procedures newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _2_Fragment_5_Procedures();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_2_fragment_5_procedures, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _v_ = view;


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("آپریشن");

            ((TextView) view.findViewById(R.id.destext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.destext)).setText("تفصیل");

            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("موجودہ سٹیٹس");
        }


        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            view.findViewById(R.id.floatingActionButton).setVisibility(View.GONE);
            setHasOptionsMenu(true);
            Network_b = false;
        } else
            Network_b = true;

        lv = view.findViewById(R.id.listView);
        jarr = MySingleton.getInstance().getArray(5);

        main = new ArrayList();

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                String a = Uri.encode(jobj.get(2).toString()).replace("%C2%A0", " ");
                a = a.replace("%20", " ");
                if (a.equals(""))
                    a = "No Longer Active";
                main.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", a));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (main.get(0).Description.equals("")) {
            main.remove(0);
            view.findViewById(R.id.topRow).setVisibility(View.GONE);
        }

        lv.setAdapter(new SampleAdapter());
        if (MySingleton.getInstance().isConnectedButton(getActivity())) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent i_ = new Intent(getContext(), __PopupEditAdd.class);
                    if (MySingleton.getInstance().getLang()){
                        i_.putExtra("Title", "آپریشن کی تفصیل");

                    }
                    else {
                        i_.putExtra("Title", "Medical Procedure Description");

                    }                    i_.putExtra("Hint", "E.g. Kidney Transplant");
                    i_.putExtra("Value", main.get(i).Description);
                    i_.putExtra("Status", main.get(i).Status);
                    i_.putExtra("Id", main.get(i).ID);
                    i_.putExtra("Index", i + "");
                    startActivityForResult(i_, 101);
                }
            });
        } else
            setHasOptionsMenu(true);

        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.hide();
        fab.animate().translationY(fab.getHeight() + 500).setInterpolator(new AccelerateInterpolator(2)).start();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(), __PopupEditAdd.class);
                if (MySingleton.getInstance().getLang()){
                    i_.putExtra("Title", "آپریشن کی تفصیل");

                }
                else {
                    i_.putExtra("Title", "Medical Procedure Description");
                }
                i_.putExtra("Hint", "E.g. Kidney Transplant");
                startActivityForResult(i_, 101);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //fab.showMenuButton(true);
                if (Network_b) {
                    fab.show();
                    fab.bringToFront();
                    fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
                }
            }
        }, 1500);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                MySingleton.getInstance().editPopup(getContext());
            } else if (resultCode == 50) {
                MySingleton.getInstance().addPopup(getContext());
            }
            jarr = MySingleton.getInstance().getArray(5);
            main.clear();

            for (int i = 0; i < jarr.length(); i++) {
                try {
                    JSONArray jobj = jarr.getJSONArray(i);
                    String a = Uri.encode(jobj.get(2).toString()).replace("%C2%A0", " ");
                    a = a.replace("%20", " ");
                    if (a.equals(""))
                        a = "No Longer Active";
                    main.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", a));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (main.get(0).Description.equals("")) {
                main.remove(0);
                _v_.findViewById(R.id.topRow).setVisibility(View.GONE);
            } else
                _v_.findViewById(R.id.topRow).setVisibility(View.VISIBLE);

            lv.setAdapter(new SampleAdapter());
        }

    }


    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_single, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            holder.Description.setText(item.Description);
            holder.Status.setText(item.Status);
            if (Network_b)
                if (MySingleton.getInstance().getLang())
                {
                    holder.edit.setText("ترمیم");
                    holder.edit.setTextColor(Color.RED);

                }
                holder.edit.setTextColor(Color.RED);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView Description;
        private TextView Status;
        private TextView edit;

        private ViewHolder(View view) {
            this.view = view;
            Description = view.findViewById(R.id.tvCondition);
            Status = view.findViewById(R.id.tvConditionStatus);
            edit = view.findViewById(R.id.tvConditionEdit);
        }

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }

    private class ListData {
        private String Description;
        private String Status;
        private String ID;

        public ListData(String id, String Description, String Status) {
            this.ID = id;
            this.Description = Description;
            this.Status = Status;
        }
    }
}