package com.webdoc.fragment;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.webdoc.AsyncResponse;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.WCFHandler;
import com.webdoc.__PopupConsltDetail;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A fragment that launches other parts of the demo application.
 */
public class _4_Fragment_1_ConsultationReports_Yesdate extends Fragment {

    Spinner sDoctor, sServiceFee;
    LinearLayout lDateTo, lDateFrom;
    TextView dateto, datefrom;

    private int mYear, mMonth, mDay;

    String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    ArrayList<ListData> mDataList;
    ArrayList Spinner;
    ListView lv;
    LinearLayout tb;
    WCFHandler wcf;
    JSONArray jdr;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_4_fragment_1_consultationreports_yesdate, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        sDoctor = view.findViewById(R.id.spinner_Doctors);
        sServiceFee = view.findViewById(R.id.spinner_ServiceFee);
        lDateTo = view.findViewById(R.id.openDateTo);
        lDateFrom = view.findViewById(R.id.openDateFrom);
        dateto = view.findViewById(R.id.tv_DateTo);
        datefrom = view.findViewById(R.id.tv_DateFrom);

        lv = view.findViewById(R.id.listView);
        tb = view.findViewById(R.id.TableYesDate);

        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.doctext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.doctext)).setText("ڈاکٹر");

            ((TextView) view.findViewById(R.id.sertext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.sertext)).setText("سروس");

            ((Button) view.findViewById(R.id.fetchReportDate)).setTypeface(tf);
            ((Button) view.findViewById(R.id.fetchReportDate)).setText("سبمٹ");

            ((TextView) view.findViewById(R.id.datefrom)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.datefrom)).setText("تاریخ سے");

            ((TextView) view.findViewById(R.id.dateto)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.dateto)).setText("تاریخ تک");


        }

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        dateto.setText(mYear + "-" + Months[mMonth] + "-" + mDay);
        datefrom.setText(2017 + "-" + Months[4] + "-" + 1);

        mDataList = new ArrayList<ListData>();
        Spinner = new ArrayList();
        wcf = new WCFHandler(getActivity());

        lDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                dateto.setText(year + "-" + Months[monthOfYear] + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        lDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                datefrom.setText(year + "-" + Months[monthOfYear] + "-" + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        jdr = MySingleton.getInstance().getArray(9);//wcf.GetJsonResult("DoctorList");
        Spinner.add("- All -");

        for (int i = 0; i < jdr.length(); i++) {
            try {
                Spinner.add(jdr.getJSONArray(i).get(0) + " " + jdr.getJSONArray(i).get(1));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayAdapter<String> adapterD = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, Spinner);
        adapterD.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sDoctor.setAdapter(adapterD);

        view.findViewById(R.id.fetchReportDate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final SweetAlertDialog pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
                pDialog.setTitleText("Fetching - Please Wait");
                pDialog.setCancelable(false);
                pDialog.show();

                mDataList.clear();
                lv.setAdapter(new SampleAdapter());
                tb.setVisibility(View.INVISIBLE);

                String dFrom = datefrom.getText() + "";
                String dTo = dateto.getText() + "";

                int dIndex = sDoctor.getSelectedItemPosition();
                dIndex--;

                //JSONArray jdr = wcf.GetJsonResult("DoctorList");
                String dID = "";
                if (dIndex == -1)
                    dID = "-1";
                else {
                    try {
                        dID = jdr.getJSONArray(dIndex).get(3) + "";
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                String serFee;
                if (sServiceFee.getSelectedItemPosition() != 0)
                    serFee = (sServiceFee.getSelectedItem() + "").split(" ")[0];
                else
                    serFee = "-1";


                GetProfile asyncTask = new GetProfile(getContext(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;
                        if (jarr == null) {
                            pDialog.dismiss();
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();

                        } else if (jarr.length() == 0) {
                            pDialog.dismiss();
                            new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("No Consultations Found")
                                    .show();
                            return;
                        } else {
                            tb.setVisibility(View.VISIBLE);

                            for (int i = 0; i < jarr.length(); i++) {
                                try {
                                    JSONArray jobj = jarr.getJSONArray(i);
                                    mDataList.add(new ListData(jobj.get(5) + "", jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            lv.setAdapter(new SampleAdapter());
                            pDialog.dismiss();
                        }

                    }
                }, "ConsultationReport/" + MySingleton.getInstance().getUserId() + "/" + dFrom + "/" + dTo + "/" + dID + "/" + serFee + "__" + "0");
                asyncTask.execute();
                /*JSONArray jarr = wcf.GetJsonResult("ConsultationReport/" + MySingleton.getInstance().getUserId() + "/" + dFrom + "/" + dTo + "/" + dID + "/" + serFee);
                if (jarr == null) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                            .show();

                } else if (jarr.length() == 0) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Consultations Found")
                            .show();
                    pDialog.dismiss();
                    return;
                } else {
                    tb.setVisibility(View.VISIBLE);

                    for (int i = 0; i < jarr.length(); i++) {
                        try {
                            JSONArray jobj = jarr.getJSONArray(i);
                            mDataList.add(new ListData(jobj.get(5) + "", jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", jobj.get(3) + "", jobj.get(4) + ""));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    lv.setAdapter(new SampleAdapter());
                    pDialog.dismiss();
                }*/
            }
        });

        if (MySingleton.getInstance().isConnectedButton(getActivity())) {

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent i_ = new Intent(getContext(), __PopupConsltDetail.class);
                    i_.putExtra("ID", mDataList.get(i).ID);
                    startActivity(i_);
                }
            });
        } else
            setHasOptionsMenu(true);

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_pentadruple, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            String[] parts = item.Date.split("-");

            holder.tvDate.setText(parts[0] + "\n- " + parts[1] + " -\n" + parts[2]);
            holder.tvDoctor.setText(item.Doctor);
            holder.tvConsultationType.setText(item.Consultation_Type);
            holder.tvPaymentMethod.setText(item.Payment_Method);
            holder.tvCharges.setText(item.Charges);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView tvDate;
        private TextView tvDoctor;
        private TextView tvConsultationType;
        private TextView tvPaymentMethod;
        private TextView tvCharges;

        private ViewHolder(View view) {
            this.view = view;
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvDoctor = (TextView) view.findViewById(R.id.tvDoctor);
            tvConsultationType = (TextView) view.findViewById(R.id.tvConsultationType);
            tvPaymentMethod = (TextView) view.findViewById(R.id.tvPaymentMethod);
            tvCharges = (TextView) view.findViewById(R.id.tvCharges);
        }
    }

    private static class ListData {

        private String ID;
        private String Date;
        private String Doctor;
        private String Consultation_Type;
        private String Payment_Method;
        private String Charges;

        public ListData(String ID, String D, String Doc, String CT, String PM, String C) {
            this.ID = ID;
            this.Date = D;
            this.Doctor = Doc;
            this.Consultation_Type = CT;
            this.Payment_Method = PM;
            this.Charges = C;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }
}