package com.webdoc.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.webdoc.MySingleton;
import com.webdoc.R;

import org.json.JSONArray;
import org.json.JSONException;

public class _4_Fragment_2_CallSummary_Corporate extends Fragment {

    JSONArray jarr;
    boolean check = false;

    private TextView Description;

    private TextView VIDEOSTATUS;
    private NumberProgressBar VIDEOBAR;

    private TextView AUDIOSTATUS;
    private NumberProgressBar AUDIOBAR;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.__fragment_4_fragment_2_callsummary_corporate, container,
                false);

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        jarr = MySingleton.getInstance().getCallSummaryArray(1);

        Description = view.findViewById(R.id.Name);

        VIDEOSTATUS = view.findViewById(R.id.Made_Total_V);
        VIDEOBAR = (NumberProgressBar) view.findViewById(R.id.number_progress_bar_video_c);

        AUDIOSTATUS = view.findViewById(R.id.Made_Total_A);
        AUDIOBAR = (NumberProgressBar) view.findViewById(R.id.number_progress_bar_audio_c);

        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.TVvideo)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.TVvideo)).setText("ویڈیو کالز");

            ((TextView) view.findViewById(R.id.TVAudio)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.TVAudio)).setText("آڈیو کالز");

            ((TextView) view.findViewById(R.id.paneltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.paneltext)).setText("پینل کا نام");

            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setText("کارپوریٹ");
        }



            //["Falcon-i", "0", "0", "10", "10"]
        for (int i = 0; i < jarr.length(); i++) {
            try {
                String name = jarr.getJSONArray(i).getString(0);
                String vmade = jarr.getJSONArray(i).getString(1);
                String amade = jarr.getJSONArray(i).getString(2);
                String vtotal = jarr.getJSONArray(i).getString(3);
                String atotal = jarr.getJSONArray(i).getString(4);

                Description.setText(name);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    String textV = "<b><font color=#72BB53>"+vmade+"</font></b><font color=#000>/"+vtotal+"</font>";
                    String textA = "<b><font color=#FFD81A>"+amade+"</font></b><font color=#000>/"+atotal+"</font>";

                    VIDEOSTATUS.setText(Html.fromHtml(textV,Html.FROM_HTML_MODE_LEGACY));
                    AUDIOSTATUS.setText(Html.fromHtml(textA,Html.FROM_HTML_MODE_LEGACY));
                } else {
                    String textV = "<b><font color=#72BB53>"+vmade+"</font></b><font color=#000>/"+vtotal+"</font>";
                    String textA = "<b><font color=#FFD81A>"+amade+"</font</b><font color=#000>/"+atotal+"</font>";

                    VIDEOSTATUS.setText(Html.fromHtml(textV));
                    AUDIOSTATUS.setText(Html.fromHtml(textA));
                }

                VIDEOBAR.setProgress(Integer.parseInt(vmade));
                VIDEOBAR.setMax(Integer.parseInt(vtotal));

                AUDIOBAR.setProgress(Integer.parseInt(amade));
                AUDIOBAR.setMax(Integer.parseInt(atotal));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (jarr.length() == 0)
            view.findViewById(R.id.corp_tohide).setVisibility(View.GONE);
        else
            view.findViewById(R.id.nrf).setVisibility(View.GONE);

    }
}
