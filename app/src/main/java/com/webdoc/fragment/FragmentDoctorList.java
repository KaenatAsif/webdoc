package com.webdoc.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.baoyz.widget.PullRefreshLayout;
import com.webdoc.AsyncResponse;
import com.webdoc.GetProfile;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;
import com.webdoc.__DoctorProfile;
import com.webdoc.messenger.Messenger;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _4_Fragment_1_ConsultationReports#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDoctorList extends Fragment {

    static ImageView im;
    boolean activityVisible = true;

    private static final String KEY_MOVIE_TITLE = "key_title";
    String status;

    Intent message_int;
    // list of data items
    ListView listView;

    private ArrayList<ListData> mDataList;

    JSONArray jarr;

    ToggleSwitch toggleSwitch;

    // declare the color generator and drawable builder
    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    private static FragmentDoctorList FragmentDoctorList = new FragmentDoctorList();
    static boolean first_time = true;

    boolean show_popup = true;

    public FragmentDoctorList() {
        // Required empty public constructor
    }

    public static FragmentDoctorList newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            FragmentDoctorList.setArguments(args);
        } catch (IllegalStateException e) {
            FragmentDoctorList = new FragmentDoctorList();
            FragmentDoctorList.setArguments(args);
        }

        return FragmentDoctorList;
    }

    public static FragmentDoctorList getInstance() {
        return FragmentDoctorList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem itemdl = menu.findItem(R.id.doctor_list);
        MenuItem itemds = menu.findItem(R.id.doctor_search);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        itemdl.setVisible(false);
        itemds.setVisible(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_fragment_doctor_list, container, false);
    }

    View _v_;
    Handler _handler_;
    Runnable _run_;
    SampleAdapter listAdapter;
    static SampleAdapter listAdapter_;
    PullRefreshLayout layout;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _MainActivity.hideDoctorLayout();

        _MainActivity.hideLogoRound();

        listAdapter = new SampleAdapter();


        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("ڈاکٹر");

            toggleSwitch = (ToggleSwitch) view.findViewById(R.id.online);

        }

        ///////////////////////////////////////////////////////////////////////////////////////////

        _handler_ = new Handler();
        _run_ = new Runnable() {
            @Override
            //automatically disappear after given time and start game
            public void run() {
                onViewCreated(true);
            }
        };

        layout = (PullRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onViewCreated(true);
                layout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        layout.setRefreshStyle(PullRefreshLayout.STYLE_CIRCLES);

        message_int = new Intent(getActivity(), Messenger.class);
        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();

        listView = (ListView) view.findViewById(R.id.listViewdList);

        toggleSwitch = (ToggleSwitch) view.findViewById(R.id.online);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                onViewCreated(false);
            }
        });
        if (first_time) {

            onViewCreated(true);
dataAvailable();

//           first_time = false;
        } else {
            jarr = MySingleton.getInstance().getArray(8);
            onViewCreated(true);
//            onViewCreated(true);
            dataAvailable();
        }

        _v_ = view;
    }

    public void onViewCreated(Boolean a) throws NullPointerException {
        status = (1 - toggleSwitch.getCheckedTogglePosition()) + "";

        listView.setAdapter(listAdapter);
        listAdapter_ = listAdapter;

        if (a) {
            if (show_popup) {
                MySingleton.getInstance().showLoadingPopup(getActivity(), "Fetching Doctor's List - Please Wait");
                show_popup = false;
            }

            GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    MySingleton.getInstance().dismissLoadingPopup();
                    jarr = (JSONArray) result;
                    /*try {
                        jarr = new JSONArray("[[\"Tahawur Abbas\",\"Khaleeq\",\"03005103515\",\"drtahawur@webdoc.com.pk\",\"\",\"Pakistan\",\"Family Medicine\",\"Prevention is better than cure\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2017\\/02\\/drr.jpg\"],[\"Sana\",\"Akhlaque\",\"03347325013\",\"drsana@webdoc.com.pk\",\"2.9375\",\"PAKISTAN\",\"General Medicine\",\"\\\"Life is so prescious and Prevention is better than cure\\\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/drsana.jpg\"],[\"Test\",\"Doctor\",\"1234567890\",\"testdoctor@webdoc.com.pk\",\"\",\"Pakistan\",\"Dentist\",\"This is a message.\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/choos_a_doctor.png\"],[\"Muhammad \",\"Sohail\",\"03314677479\",\"drsohail@webdoc.com.pk\",\"4\",\"Pakistan\",\"General Medicine\",\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Sohail.jpg\"],[\"Raalah\",\"Ghaffar\",\"03365187573\",\"DrRaalah@webdoc.com.pk\",\"\",\"Pakistan\",\"Internal Medicine\",\"\",\"1\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/drraalah.jpg\"],[\"Saira\\u000d\\u000a\",\"Tahawur\\u000d\\u000a\",\"03329143354\",\"\",\"\",\"Pakistan\\u000d\\u000a\",\"Internal Medicine\",\"Hello\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2017\\/02\\/dr.jpg\"],[\"Seemab \",\"Khalid\",\"03348458829\",\"drseemab@webdoc.com.pk\",\"\",\"Pakistan\",\"Internal Medicine\",\"everyday holds the possibility of a miracle\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Seemab.jpg\"],[\"Neelam\",\"Shahzadi\",\"03355867137\",\"drneelam@webdoc.com.pk\",\"\",\"Pakistan\",\"General Medicine\",\"\",\"0\",\"http:\\/\\/webdoc.com.pk\\/wp-content\\/uploads\\/2014\\/05\\/Neelem.jpg\"]]");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                    if (jarr == null) {
                        MySingleton.getInstance().dismissLoadingPopup();

                        try {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                        } catch (NullPointerException e) {
                        }
                        return;

                    }
                    MySingleton.getInstance().updateArray(9, jarr);
                    MySingleton.getInstance().dismissLoadingPopup();
                    dataAvailable();

                    _handler_.removeCallbacks(_run_);
                    if (!MySingleton.getInstance().getIncall())
                        _handler_.postDelayed(_run_, 30000);


                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (MySingleton.getInstance().isConnectedButton(getActivity())) {

                                if ((!MySingleton.getInstance().getIproduct() && !MySingleton.getInstance().getCpanel()) && false) {
                                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Something went wrong!")
                                            .setContentText("Please Add An Insurance Product or A Corporate Panel")
                                            .show();
                                    return;
                                }

                                message_int.putExtra("Name", mDataList.get(position).Name);
                                message_int.putExtra("UserName", mDataList.get(position).userName);
                                //message_int.putExtra("Status", mDataList.get(position).Status);
                                MySingleton.getInstance().setDoctorStatus(mDataList.get(position).Status);
                                MySingleton.getInstance().setDoctorRating(mDataList.get(position).Rating);

                                startActivity(message_int);
                            } else {
                                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText("Something went wrong!")
                                        .setContentText("No Network")
                                        .show();
                            }
                        }
                    });

                }
            }, "DoctorList" + "__" + "0");
            if (!MySingleton.getInstance().getIncall())
                asyncTask.execute();
        } else {
            dataAvailable();

            _handler_.removeCallbacks(_run_);
            _handler_.postDelayed(_run_, 30000);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (MySingleton.getInstance().isConnectedButton(getActivity())) {

                        if ((!MySingleton.getInstance().getIproduct() && !MySingleton.getInstance().getCpanel()) && false) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("Please Add An Insurance Product or Corporate Panel")
                                    .show();
                            return;
                        }

                        _handler_.removeCallbacks(_run_);

                        message_int.putExtra("Name", mDataList.get(position).Name);
                        message_int.putExtra("UserName", mDataList.get(position).userName);
                        MySingleton.getInstance().setDoctorStatus(mDataList.get(position).Status);
                        MySingleton.getInstance().setDoctorRating(mDataList.get(position).Rating);

                        startActivity(message_int);
                    } else {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("No Network")
                                .show();
                    }
                }
            });
        }
    }

    void dataAvailable() {

        listAdapter.removeEntrys();

        if (jarr == null || jarr.length() == 0)
            return;
        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                String fn = jobj.get(0) + "";
                String ln = jobj.get(1) + "";
                String id = jobj.get(3) + "";
                String regStatus = jobj.get(10) + "";

                Double Rating;
                if (!(jobj.get(4) + "").equals(""))
                    Rating = Double.parseDouble(jobj.get(4) + "");
                else
                    Rating = 4.0;

                String status_ = jobj.get(8) + "";
                //ratbar.setProgress(Rating);
                if (fn.contains("\\r\\n") && ln.contains("\\r\\n")) {
                    fn = fn.replace("\\r\\n", "");
                    ln = ln.replace("\\r\\n", "");
                }
                if (status.equals("1")) {
                    if (status_.equals("1") || status_.equals("2")) {
                        //if (!mDataList.contains(new ListData(fn.trim() + " " + ln.trim(), Rating, id, status_)))
                        if (regStatus.equals("Approved") || regStatus.equals("testing1"))
                            listAdapter.addEntry(new ListData(fn.trim() + " " + ln.trim(), Rating, id, status_, regStatus));
                    }
                } else if (status.equals("0"))
                    if (status_.equals("0"))
                        //if (!mDataList.contains(new ListData(fn.trim() + " " + ln.trim(), Rating, id, status_)))
                        if (regStatus.equals("Approved"))
                            listAdapter.addEntry(new ListData(fn.trim() + " " + ln.trim(), Rating, id, status_, regStatus));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        layout.setRefreshing(false);
    }

    public void onDoctorSearch(String dname) {

        listAdapter.searchEntry(dname);

        /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (MySingleton.getInstance().isConnectedButton()) {
                    message_int.putExtra("Name", mDataList.get(position).Name);
                    message_int.putExtra("UserName", mDataList.get(position).userName);
                    MySingleton.getInstance().setDoctorRating(mDataList.get(position).Rating);
                    startActivity(message_int);
                } else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                }
            }
        });*/

        _handler_.removeCallbacks(_run_);
        _handler_.postDelayed(_run_, 30000);
    }


    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            mDataList = new ArrayList<ListData>();
        }

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void addEntry(ListData d) {
            if (!mDataList.contains(d)) {
                mDataList.add(d);
                notifyDataSetChanged();
            }

        }

        public void removeEntrys() {
            mDataList.clear();
        }

        public void searchEntry(String dname) {
            String status = (1 - toggleSwitch.getCheckedTogglePosition()) + "";
            removeEntrys();

            for (int i = 0; i < jarr.length(); i++) {
                try {
                    JSONArray jobj = jarr.getJSONArray(i);
                    String fn = jobj.get(0) + "";
                    String ln = jobj.get(1) + "";
                    String id = jobj.get(3) + "";
                    String regStatus = jobj.get(10) + "";

                    Double Rating;
                    if (!(jobj.get(4) + "").equals(""))
                        Rating = Double.parseDouble(jobj.get(4) + "");
                    else
                        Rating = 4.0;

                    String status_ = jobj.get(8) + "";
                    //ratbar.setProgress(Rating);
                    if (fn.contains("\\r\\n") && ln.contains("\\r\\n")) {
                        fn = fn.replace("\\r\\n", "");
                        ln = ln.replace("\\r\\n", "");
                    }
                    if ((fn.trim() + " " + ln.trim()).toLowerCase().contains(dname) && status.equals(status_))
                        if (regStatus.equals("Approved"))
                            listAdapter.addEntry(new ListData(fn.trim() + " " + ln.trim(), Rating, id, status_, regStatus));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_dlist, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final ListData item = getItem(position);

            TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            // UrlImageViewHelper.setUrlDrawable(holder.imageView, item.Path);

            holder.textView.setText(item.Name);
            holder.textView.setSelected(true);

            holder.ratingBar.setRating((float) item.Rating);

            //holder.regStatus.setText("");

            holder.openProfile.setTag(position);
            holder.openProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i_ = new Intent(getActivity(), __DoctorProfile.class);
                    i_.putExtra("UserName", mDataList.get(Integer.parseInt(view.getTag() + "")).userName);
                    i_.putExtra("Rating", mDataList.get(Integer.parseInt(view.getTag() + "")).Rating);
                    startActivity(i_);
                }
            });

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                VectorDrawable drawable_ = (VectorDrawable) holder.status.getDrawable();
                if (item.Status.equals("0"))
                    drawable_.mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.darker_gray), PorterDuff.Mode.SRC_IN);
                else if (item.Status.equals("1"))
                    drawable_.mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.green), PorterDuff.Mode.SRC_IN);
                else if (item.Status.equals("2"))
                    drawable_.mutate().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);
            } else {
                BitmapDrawable drawable_ = (BitmapDrawable) holder.status.getDrawable();
                if (item.Status.equals("0"))
                    drawable_.mutate().setColorFilter(ContextCompat.getColor(getContext(), android.R.color.darker_gray), PorterDuff.Mode.SRC_IN);
                else if (item.Status.equals("1"))
                    drawable_.mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.green), PorterDuff.Mode.SRC_IN);
                else if (item.Status.equals("2"))
                    drawable_.mutate().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);
            }


            return convertView;
        }

        public int getIdByName(String name) {
            int id = 0;
            for (int i = 0; i < mDataList.size(); i++) {
                if (mDataList.get(i).Status.equals("1"))
                    if (mDataList.get(i).userName.equals(name)) {
                        id = i;
                        break;
                    }
            }
            return id;
        }

        public void changeStatus(int position, String name) {
            mDataList.get(position).Status = "2";
            MySingleton.getInstance().changeDoctorStatusInJsonArray(name);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder {

        private View view;

        private ImageView imageView;
        private TextView textView;
        private RatingBar ratingBar;
        private ImageButton openProfile;
        // private TextView regStatus;
        public ImageView status;

        private ViewHolder(View view) {
            this.view = view;
            imageView = (ImageView) view.findViewById(R.id.imageViewdList);
            textView = (TextView) view.findViewById(R.id.textViewdList);
            ratingBar = (RatingBar) view.findViewById(R.id.rating_bar_rating);
            openProfile = (ImageButton) view.findViewById(R.id.dlist_openprofile);
            status = (ImageView) view.findViewById(R.id.dlist_status);
            //regStatus = (TextView) view.findViewById(R.id.dlist_reg_status);
        }
    }

    private static class ListData {

        //["Sana", "Akhlaque", "03347325013", "drsana@webdoc.com.pk", "3.05", "PAKISTAN", "General Medicine", "\"Life is so prescious and Prevention is better than cure\"", "0"],

        private String Name;
        private String userName;
        private double Rating;
        private String Status;
        private String RegStatus;

        public ListData(String data, double rating, String uName, String status, String regStatus) {
            this.Name = data;
            this.Rating = rating;
            this.userName = uName;
            this.Status = status;
            this.RegStatus = regStatus;
        }
    }

    public static void changeStatus(String name) {
        int id = listAdapter_.getIdByName(name);
        listAdapter_.changeStatus(id, name);
    }
}
