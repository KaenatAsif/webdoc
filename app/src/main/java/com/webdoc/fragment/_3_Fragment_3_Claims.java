package com.webdoc.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;
import com.webdoc.pictureGallery.PictureGallery;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class _3_Fragment_3_Claims extends Fragment {

    private static final String KEY_MOVIE_TITLE = "Title";

    public _3_Fragment_3_Claims() {
    }

    static _3_Fragment_3_Claims fragment = new _3_Fragment_3_Claims();

    public static _3_Fragment_3_Claims newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _3_Fragment_3_Claims();
            fragment.setArguments(args);
        }

        return fragment;
    }

    ArrayList<ListData> main;
    SampleAdapter sa;
    ListView lv;

    private RequestQueue requestQueue;
    private static final String URL = "http://webdoc.com.pk/ImagesUploadByCam/upload.php";
    private StringRequest request;

    String DIR = "";
    int COUNT = 0;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        /// Batch service : push notification
      /*  Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_3_fragment_4_claims, container, false);



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _MainActivity.hideDoctorLayout();

        requestQueue = Volley.newRequestQueue(getContext());

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                boolean a = false;

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", MySingleton.getInstance().getUserId());
                hashMap.put("dir", DIR);
                if (COUNT == -1)
                    hashMap.put("flag", "New");
                else {
                }
                return hashMap;
            }
        };

        sa = new SampleAdapter();
        lv = (ListView) view.findViewById(R.id.list_view);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(dm.widthPixels / dm.xdpi, 2);
        double y = Math.pow(dm.heightPixels / dm.ydpi, 2);
        double screenInches = Math.sqrt(x + y);
        MySingleton.getInstance().setScreenSize(screenInches);

        GetSingle async = new GetSingle(getContext(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                if (jarr == null) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;

                }
                else if(jarr.length()==0){
                    new SweetAlertDialog(getContext(), SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("For Befikar Subscribers")
                            .setContentText("Please subscribe to Befikar card to claim")
                            .show();
                    return;

                }

                for (int i = 0; i < jarr.length(); i++) {
                    try {
                        JSONArray inner = jarr.getJSONArray(i);
                        String i_ = inner.getString(0);
                        String b_ = inner.getString(1);
                        String s_ = inner.getString(2);
                        if (!s_.equals("null"))
                            sa.addEntry(new ListData(i_, b_, s_));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, "PrescriptionOfClaim/" + MySingleton.getInstance().getUserId() + "__0");
        async.execute();

        lv.setAdapter(sa);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                String status = sa.getItem(i).Status;
                final String id = sa.getItem(i).ID;



                if (status.equals("Pending")) {
                    String function="AddNewClaim/" +MySingleton.getInstance().getUserId() +"/"+ id;
                    final GetSingle async = new GetSingle(getContext(), new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            String res = ((String) result).replaceAll("\"", "");
                            if (!res.equals("Fail")) {
                                COUNT = -1;
                                DIR = res;
                                requestQueue.add(request);
                                sa.getItem(i).Status = "Active";
                                MySingleton.getInstance().setClaimNo(res);
                                startActivity(new Intent(getContext(), PictureGallery.class).putExtra("_ID_", id));
                                sa.notifyDataSetChanged();
                            }
                        }
                    }, function + "__1");
                    async.execute();
                }
                ///DocumentSubmitted/{email}/{claimno}
                else if (status.equals("Processing")) {
                    final GetSingle async = new GetSingle(getContext(), new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            String res = ((String) result).replaceAll("\"", "");
                            if (!res.equals("Fail")) {
                                COUNT = -1;
                                DIR = res;
                                requestQueue.add(request);

                                sa.getItem(i).Status = "Active";
                                MySingleton.getInstance().setClaimNo(res);
                                startActivity(new Intent(getContext(), PictureGallery.class).putExtra("_ID_", id));
                                sa.notifyDataSetChanged();
                            }
                        }
                    }, "AddNewClaim/" +MySingleton.getInstance().getUserId() +"/"+ id + "__1");
                    async.execute();
                }

                else {
                    startActivity(new Intent(getContext(), PictureGallery.class).putExtra("_ID_", id));
                }
            }
        });

    }

    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            main = new ArrayList<>();
        }

        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
               /* if (position == main.size() - 1)
                    convertView = View.inflate(getContext(), R.layout.___list_item_kitchenlast, null);
                else*/
                convertView = View.inflate(getContext(), R.layout.___list_item_claims, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);


            holder.Prescription.setText(item.Prescription);
            if (item.Status.equals("Pending")) {
                holder.Status.setText("Raise Claim");

            }
            else if (item.Status.equals("Processing")) {
                holder.Status.setText("Processing Claim");
            }
            else
                holder.Status.setText("View Claim");

            return convertView;



        }

    }

    private static class ViewHolder {
        private View view;

        private TextView Prescription;
        private TextView Status;

        private ViewHolder(View view) {
            this.view = view;
            Prescription = (TextView) view.findViewById(R.id.body);
            Status = (TextView) view.findViewById(R.id.stv);
        }
    }

    private class ListData {
        private String Prescription;
        private String ID;
        private String Status;

        public ListData(String id, String p, String s) {
            ID = id;
            Prescription = p;
            Status = s;
        }
    }



}
