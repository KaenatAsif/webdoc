package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupEditProfile;

import org.json.JSONArray;
import org.json.JSONException;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _1_FragmentGeneralProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _1_FragmentGeneralProfile extends Fragment {

    String[] SharedPrefNames = {"PatientProfile", "Conditions", "Allergies", "Medications", "Immunizations", "Procedures", "Insurance", "Corporate", "Consultations"};

    private static final String KEY_MOVIE_TITLE = "key_title";

    JSONArray Arr;
    TextView fn, ln, cnic, dob, gender, address, country, city, mobile;
    Button b1;

    private ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
    private TextDrawable.IBuilder mDrawableBuilder;

    LinearLayout navigationView;
    TextView tName, tEmail;
    ImageView tImage;
    View _v_;



    public _1_FragmentGeneralProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _1_FragmentGeneralProfile.
     */
    static _1_FragmentGeneralProfile fragmentGeneralProfile = new _1_FragmentGeneralProfile();

    public static _1_FragmentGeneralProfile newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragmentGeneralProfile.setArguments(args);
        } catch (IllegalStateException e) {
            fragmentGeneralProfile = new _1_FragmentGeneralProfile();
            fragmentGeneralProfile.setArguments(args);
        }
        return fragmentGeneralProfile;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        /////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("DEV5A9F7B9DC1FADA6227D4E6C8C99")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_1_fragmentgeneralprofile, container, false);




    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);



        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("جنرل پروفائل");

            ((TextView) view.findViewById(R.id.identtext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.identtext)).setText("شناخت");
            ((TextView) view.findViewById(R.id.identtext)).setTextSize(14);
            ((TextView) view.findViewById(R.id.identtext)).setPadding(0,10,4,10);

            ((TextView) view.findViewById(R.id.persontext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.persontext)).setText("ذاتی معلومات");
            ((TextView) view.findViewById(R.id.persontext)).setTextSize(14);
            ((TextView) view.findViewById(R.id.persontext)).setPadding(0,10,4,10);

            ((TextView) view.findViewById(R.id.fntext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.fntext)).setText("پہلا نام");
            ((TextView) view.findViewById(R.id.fntext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.fntext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.ln)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.ln)).setText("آخری نام");
            ((TextView) view.findViewById(R.id.ln)).setTextSize(13);
            ((TextView) view.findViewById(R.id.ln)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.cnic)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.cnic)).setText("شناختی کارڈ نمبر");
            ((TextView) view.findViewById(R.id.cnic)).setTextSize(13);
            ((TextView) view.findViewById(R.id.cnic)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.dobtext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.dobtext)).setText("پیدائش کی تاریخ");
            ((TextView) view.findViewById(R.id.dobtext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.dobtext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.gentext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.gentext)).setText("جنس");
            ((TextView) view.findViewById(R.id.gentext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.gentext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.addtext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.addtext)).setText("پتہ");
            ((TextView) view.findViewById(R.id.addtext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.addtext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.counttext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.counttext)).setText("ملک");
            ((TextView) view.findViewById(R.id.counttext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.counttext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.citytext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.citytext)).setText("شہر");
            ((TextView) view.findViewById(R.id.citytext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.citytext)).setPadding(0,0,40,0);

            ((TextView) view.findViewById(R.id.mobiletext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.mobiletext)).setText("فون نمبر");
            ((TextView) view.findViewById(R.id.mobiletext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.mobiletext)).setPadding(0,0,40,0);


        }



        //LayoutInflater inflater = getLayoutInflater(savedInstanceState);
        _v_ = view;

        mDrawableBuilder = TextDrawable.builder()
                .beginConfig()
                .withBorder(4)
                .endConfig()
                .round();



        fn = view.findViewById(R.id.GeneralProfile_FN);
        dob = view.findViewById(R.id.GeneralProfile_DOB);
        ln = view.findViewById(R.id.GeneralProfile_LN);
        cnic = view.findViewById(R.id.GeneralProfile_CNIC);
        gender = view.findViewById(R.id.GeneralProfile_Gender);
        address = view.findViewById(R.id.GeneralProfile_Address);
        country = view.findViewById(R.id.GeneralProfile_Country);
        city = view.findViewById(R.id.GeneralProfile_City);
        mobile = view.findViewById(R.id.GeneralProfile_Mobile);
        b1 = view.findViewById(R.id.editProfile);

        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            view.findViewById(R.id.editProfile).setVisibility(View.GONE);
        }


        ((Button) view.findViewById(R.id.editProfile)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //MySingleton.getInstance().logout(getContext());
                Intent i_ = new Intent(getActivity(), __PopupEditProfile.class);
                i_.putExtra("Date", dob.getText().toString());
                i_.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(i_, 101);
            }
        });

        //functionCall(0);
       // funtionSettingData();

        Toolbar topToolBar = MySingleton.getInstance().getToolbar();



        Arr = MySingleton.getInstance().getArray(0);

        try {

            if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                MySingleton.getInstance().setGprofile(false);

                if (MySingleton.getInstance().getLang())
                {
                    b1.setText("پروفائل شامل کریں");
                    b1.setTextSize(12);
                }

                  else
                      b1.setText("Add");
                setHasOptionsMenu(true);
            } else {
                MySingleton.getInstance().setGprofile(true);

                if (MySingleton.getInstance().getLang())
                {
                    b1.setText("پروفائل میں ترمیم کریں");
                    b1.setTextSize(13);
                    b1.setPadding(3,0,3,0);
                }

                else
                    b1.setText("Edit");
            }

            fn.setText(Arr.get(0) + "");
            fn.setSelected(true);
            ln.setText(Arr.get(1) + "");
            cnic.setText(Arr.get(2) + "");
            dob.setText((Arr.get(3) + "").replace("12:00AM", ""));
            gender.setText(Arr.get(4) + "");
            address.setText((Arr.get(5) + "").replace("@slash@","/"));
            country.setText(Arr.get(6) + "");
            city.setText(Arr.get(7) + "");
            mobile.setText(Arr.get(8) + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
    SweetAlertDialog pDialog;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Result!")
                        .setContentText("Edited Successfully")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                functionSettingData();
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
            } else if (resultCode == 50) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Result!")
                        .setContentText("Added Successfully")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                functionSettingData();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();

            }


           /* pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
            pDialog.setTitleText("Logging In - Please Wait");
            pDialog.setCancelable(false);
            pDialog.show();
            String function__ = "ViewPatientProfile/" + MySingleton.getInstance().getUserId();
            GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    if (result == null) {
                        pDialog.dismiss();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    }

                }
            },  function__ + "__" + "1");
            asyncTask.execute();*/
           // Arr = MySingleton.getInstance().getArray(0);

            /*try {

                if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                    MySingleton.getInstance().setGprofile(false);
                    b1.setText("Add Profile");
                    setHasOptionsMenu(true);
                } else {
                    MySingleton.getInstance().setGprofile(true);
                    b1.setText("Edit Profile");
                }

                fn.setText(Arr.get(0) + "");
                fn.setSelected(true);
                ln.setText(Arr.get(1) + "");
                cnic.setText(Arr.get(2) + "");
                dob.setText((Arr.get(3) + "").replace("12:00AM", ""));
                gender.setText(Arr.get(4) + "");
                address.setText((Arr.get(5) + "").replace("@slash@","/"));
                country.setText(Arr.get(6) + "");
                city.setText(Arr.get(7) + "");
                mobile.setText(Arr.get(8) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }

    /*private void functionCall(int index) {
        if (MySingleton.getInstance().isConnected(getActivity())) {
            MySingleton.getInstance().showLoadingPopup(getActivity(), "Fetching Profile - Please Wait");
            String function__ = "GetPatientProfile/" + MySingleton.getInstance().getUserId();
            final GetProfile asyncTask = new GetProfile(getActivity(), new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONArray jsn = (JSONArray) result;
                    if (jsn == null) {
                        MySingleton.getInstance().dismissLoadingPopup();
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                        // logout();
                                    }
                                })
                                .show();
                        return;
                    }
                    try {
                        for (int i = 0; i < jsn.length(); i++) {
                            if (i == 0) {
                                JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                MySingleton.getInstance().setArray(Arr);
                                MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                                if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                    MySingleton.getInstance().setGprofile(false);
                                } else
                                    MySingleton.getInstance().setGprofile(true);
                            } else if (i == 6) {
                                JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                                    MySingleton.getInstance().setIproduct(false);
                                } else
                                    MySingleton.getInstance().setIproduct(true);
                                MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());
                            } else if (i == 7) {
                                JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                    MySingleton.getInstance().setCpanel(false);
                                } else
                                    MySingleton.getInstance().setCpanel(true);
                                MySingleton.getInstance().setArray(Arr);
                                MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                            } else {
                                MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MySingleton.getInstance().dismissLoadingPopup();

                    Arr = MySingleton.getInstance().getArray(0);

                    try {
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            b1.setText("Add Profile");
                            setHasOptionsMenu(true);
                        } else
                            b1.setText("Edit Profile");

                        fn.setText(Arr.get(0) + "");
                        fn.setSelected(true);
                        ln.setText(Arr.get(1) + "");
                        cnic.setText(Arr.get(2) + "");
                        dob.setText((Arr.get(3) + "").replace("12:00AM", ""));
                        gender.setText(Arr.get(4) + "");
                        address.setText(Arr.get(5) + "");
                        country.setText(Arr.get(6) + "");
                        city.setText(Arr.get(7) + "");
                        mobile.setText(Arr.get(8) + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                    }

                }
            }, function__ + "__" + "0");
            asyncTask.execute();
        } else {
            try {
                for (int i = 0; i < 9; i++) {
                    if (i == 0) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setGprofile(false);
                        } else
                            MySingleton.getInstance().setGprofile(true);
                    } else if (i == 6) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setIproduct(false);
                        } else
                            MySingleton.getInstance().setIproduct(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else if (i == 7) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setCpanel(false);
                        } else
                            MySingleton.getInstance().setCpanel(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }*/


    public void functionSettingData()
    {

        Arr = MySingleton.getInstance().getArray(0);

        try {
            if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                b1.setText("Add");
                setHasOptionsMenu(true);
            } else
                b1.setText("Edit");

            fn.setText(Arr.get(0) + "");
            fn.setSelected(true);
            ln.setText(Arr.get(1) + "");
            cnic.setText(Arr.get(2) + "");
            dob.setText((Arr.get(3) + "").replace("12:00AM", ""));
            gender.setText(Arr.get(4) + "");
            address.setText((Arr.get(5) + "").replace("@slash@","/"));
            country.setText(Arr.get(6) + "");
            city.setText(Arr.get(7) + "");
            mobile.setText(Arr.get(8) + "");
         //   MainPage mp=new MainPage();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}
