package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__popup_addEdit_Family;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _2_Fragment_1_Conditions#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _3_Fragment_4_FamilyRegistration extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    TextView remove;

    ArrayList<ListData> main;
    View _v_;

    ListView lv;
    JSONArray jarr;

    Boolean Network_b = false;

    public _3_Fragment_4_FamilyRegistration() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     */
    static _3_Fragment_4_FamilyRegistration fragmentfamily = new _3_Fragment_4_FamilyRegistration();

    public static _3_Fragment_4_FamilyRegistration newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragmentfamily.setArguments(args);
        } catch (IllegalStateException e) {
            fragmentfamily = new _3_Fragment_4_FamilyRegistration();
            fragmentfamily.setArguments(args);
        }

        return fragmentfamily;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment__3__fragment__family_registration, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("خاندان کی رجسٹریشن");

            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("موبائل نمبر");
            ((TextView) view.findViewById(R.id.reltext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0, 0, 40, 0);


            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("رشتہ");
            ((TextView) view.findViewById(R.id.reltext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0, 0, 40, 0);

        }

        _v_ = view;

        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            view.findViewById(R.id.floatingActionButton).setVisibility(View.GONE);
            Network_b = false;
            setHasOptionsMenu(true);
        } else
            Network_b = true;
///ViewFamilyMember/{email}
//        AddFamilyMember/{email}/{MobileNo}/{RealtionShipId}
        lv = view.findViewById(R.id.listView);


        String function__ = "ViewFamilyMember/" + MySingleton.getInstance().getUserId();

        final GetSingle asyncTask = new GetSingle(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;

                if (jarr == null) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                            .show();
                    return;
                } else {
                    MySingleton.getInstance().updateFamilyArray(0, jarr);
                    SetItemInListView();
                }
            }
        }, function__ + "__" + "0");
        asyncTask.execute();


        if (MySingleton.getInstance().isConnectedButton(getActivity())) {
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    /*Intent i_ = new Intent(getContext(), __popup_addEdit_Family.class);
                    i_.putExtra("Title", "Family Registration");

                    i_.putExtra("Status", main.get(i).Status);
                   *//* i_.putExtra("Id", main.get(i).ID);
                    i_.putExtra("Index", i + "");*//*
                    startActivityForResult(i_, 101);*/
                    confirmDelete(main.get(i).Description);
                }
            });
        }

        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.hide();
        fab.animate().translationY(fab.getHeight() + 500).setInterpolator(new AccelerateInterpolator(2)).start();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(), __popup_addEdit_Family.class);
                i_.putExtra("Title", "Family Registration");
                startActivityForResult(i_, 101);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //fab.showMenuButton(true);
                if (Network_b) {
                    fab.show();
                    fab.bringToFront();
                    fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
                }
            }
        }, 750);

    }

    public void SetItemInListView()

    {
//        final FloatingActionButton fab = getActivity().findViewById(R.id.floatingActionButton);
        jarr = MySingleton.getInstance().getFamilyArray(0);

        main = new ArrayList();

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                /*JSONArray second= jobj.getJSONArray(0);
                JSONArray third= second.getJSONArray(0);*/
//                if (jarr.length()<=7)
//                    fab.hide();
//


                main.add(new ListData(jobj.get(1) + "", jobj.get(0) + ""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        lv.setAdapter(new SampleAdapter());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Result!")
                        .setContentText("Added Successfully")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                SetItemInListView();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();            }
            else if (resultCode == 50) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Result!")
                        .setContentText("Added Successfully")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                SetItemInListView();
                                sweetAlertDialog.dismiss();

                            }
                        })
                        .show();              }
            jarr = MySingleton.getInstance().getFamilyArray(0);
            if (jarr == null)
                return;
            main.clear();
            for (int i = 0; i < jarr.length(); i++) {
                try {
                    JSONArray jobj = jarr.getJSONArray(i);

                    main.add(new ListData(jobj.get(1) + "", jobj.get(0) + ""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (main.get(0).Description.equals("")) {
                main.remove(0);
                _v_.findViewById(R.id.topRow).setVisibility(View.GONE);
            } else
                _v_.findViewById(R.id.topRow).setVisibility(View.VISIBLE);
            lv.setAdapter(new SampleAdapter());
        }

    }

    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_single, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            holder.Description.setText(item.Description);
            holder.Status.setText(item.Status);
            if (MySingleton.getInstance().getLang())
                holder.edit.setText("ڈیلیٹ");
else
            holder.edit.setText("Remove");


            if (Network_b)

                holder.edit.setTextColor(Color.RED);

            return convertView;

        }
    }

    private static class ViewHolder {

        private View view;

        private TextView Description;
        private TextView Status;
        private TextView edit;

        private ViewHolder(View view) {
            this.view = view;
            Description = view.findViewById(R.id.tvCondition);
            Status = view.findViewById(R.id.tvConditionStatus);
            edit = view.findViewById(R.id.tvConditionEdit);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }

    private class ListData {
        private String Description;
        private String Status;


        public ListData(String Description, String Status) {

            this.Description = Description;
            this.Status = Status;
        }
    }

    public void confirmDelete(final String ans) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText("Alert!");
        pDialog.setContentText("Are you sure you want to remove this family member ?");
        pDialog.setCustomImage(R.drawable.logowebdoc1);
        pDialog.setConfirmText("Yes");
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                String function__ = "RemoveFamilyMember/" + MySingleton.getInstance().getUserId() + "/" + ans;
                final GetSingle asyncTask = new GetSingle(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jarr = (JSONArray) result;

                        if (jarr == null) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .show();
                            return;
                        } else {
                            MySingleton.getInstance().updateFamilyArray(0, jarr);
                            MySingleton.getInstance().editPopup(getActivity());

                            jarr = MySingleton.getInstance().getFamilyArray(0);

                            main = new ArrayList();

                            for (int i = 0; i < jarr.length(); i++) {
                                try {
                                    JSONArray jobj = jarr.getJSONArray(i);
                                    main.add(new ListData(jobj.get(0) + "", jobj.get(1) + ""));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            lv.setAdapter(new SampleAdapter());
                        }
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            }
        });
        pDialog.setCancelText("No");
        pDialog.showCancelButton(true);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        });
        pDialog.show();


    }
}
