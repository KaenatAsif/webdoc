package com.webdoc.fragment;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupCorPanelAdd;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _4_FragmentCorporatePanel#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _4_FragmentCorporatePanel extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    public _4_FragmentCorporatePanel() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _4_FragmentCorporatePanel.
     */
    static _4_FragmentCorporatePanel fragment = new _4_FragmentCorporatePanel();
    public static _4_FragmentCorporatePanel newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _4_FragmentCorporatePanel();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_4_fragmentcorporatepanel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((Button)view.findViewById(R.id.editPanel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(),__PopupCorPanelAdd.class);
                /*i_.putExtra();
                i_.putExtra();
                i_.putExtra();
                i_.putExtra();*/
                startActivity(i_);
            }
        });



        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setPadding(0, 10, 270, 10);
            ((TextView) view.findViewById(R.id.heading)).setText("کارپوریٹ پینل");

            ((TextView) view.findViewById(R.id.comptext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.comptext)).setText("کمپنی");
            ((TextView) view.findViewById(R.id.comptext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.comptext)).setPadding(0, 0, 40, 0);

            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("امپلاۓ نمبر");
            ((TextView) view.findViewById(R.id.reltext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0, 0, 40, 0);

            ((TextView) view.findViewById(R.id.cnictext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.cnictext)).setText("امپلاۓ شناختی کارڈ نمبر");
            ((TextView) view.findViewById(R.id.cnictext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.cnictext)).setPadding(0, 0, 40, 0);

            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("امپلاۓ سے رشتہ");
            ((TextView) view.findViewById(R.id.reltext)).setTextSize(13);
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0, 0, 40, 0);


        }


    }
}
