package com.webdoc.fragment;

import android.animation.Animator;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc._MainActivity;
import com.webdoc.cards.CardItem;
import com.webdoc.cards.CardPagerAdapter;
import com.webdoc.cards.ShadowTransformer;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

/**
 * Created by faheem on 28/11/2017.
 */

public class _2o5_Fragment_HealthCards extends Fragment {

    JSONArray jarr;

    int height;


    private ViewPager mViewPager;

    private CardPagerAdapter mCardAdapter;
    private ShadowTransformer mCardShadowTransformer;

    View v;
    View HEALTH_CARDS, LIST_VIEW;

    private static final String KEY_MOVIE_TITLE = "key_title";
    private static final int ANIM_DURATION_TOOLBAR = 750;

    private String[] ts = {"Pending", "All", "Expired"};

    ToggleSwitch toggleSwitch;

    ArrayList<ListData> main;

    SampleAdapter sa;
    ListView lv;

    private static final String URL = "https://portal.webdoc.com.pk/transection/apicall.php?email=03145362496@webdoc.com.pk&RRN=";


    public _2o5_Fragment_HealthCards() {
    }

    static _2o5_Fragment_HealthCards fragment = new _2o5_Fragment_HealthCards();

    public static _2o5_Fragment_HealthCards newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _2o5_Fragment_HealthCards();
            fragment.setArguments(args);
        }

        return fragment;
    }

    public ViewPropertyAnimator upAnimation(View view) {
        view.setTranslationY(height);

        ViewPropertyAnimator vpa = view.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(0);

        return vpa;
    }

    public ViewPropertyAnimator downAnimation(View view) {

        ViewPropertyAnimator vpa = view.animate()
                .translationY(height)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(0);

        return vpa;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return v = inflater.inflate(R.layout.__fragment_3_fragment_3_healthcards, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _MainActivity.hideDoctorLayout();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;

        HEALTH_CARDS = (RelativeLayout) view.findViewById(R.id._Main_HealthCards);
        LIST_VIEW = (RelativeLayout) view.findViewById(R.id._Main_Vouchers);


        LIST_VIEW.animate()
                .translationY(height);

        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setPadding(0, 10,520, 10);
            ((TextView) view.findViewById(R.id.heading)).setText("ہیلتھ کارڈ");

            ((Button) view.findViewById(R.id.btnvv)).setText("واؤچر دیکھیں");
            ((Button) view.findViewById(R.id.btnvhc)).setText("ہیلتھ کارڈ دیکھیں");
        }
        ///////////////////////////////////////////////////////////////////////////////////////////


        //((CardView) view.findViewById(R.id.upercard)).setCardElevation(5.0f);

        /*((ImageView) view.findViewById(R.id.stamp)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/


        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            ((Button) view.findViewById(R.id.btnvv)).setVisibility(View.GONE);
        }
        mViewPager = (ViewPager) view.findViewById(R.id.viewPager);

        Resources a = getResources();

        mCardAdapter = new CardPagerAdapter(a, getActivity());

        mCardAdapter.addCardItem(new CardItem(R.string.movie_title, R.string.movie_genre));
        mCardAdapter.addCardItem(new CardItem(R.string.movie_title, R.string.movie_genre));
        mCardAdapter.addCardItem(new CardItem(R.string.movie_title, R.string.movie_genre));
        mCardAdapter.addCardItem(new CardItem(R.string.movie_title, R.string.movie_genre));

        mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter, getActivity());

        mViewPager.setAdapter(mCardAdapter);
        mViewPager.setOffscreenPageLimit(4);

        mViewPager.setPageTransformer(false, mCardShadowTransformer);
        mCardShadowTransformer.enableScaling(true);

        final View yourView = view.getRootView();

        if (MySingleton.getInstance().getLang()){

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    new SimpleTooltip.Builder(getActivity())
                            .anchorView(yourView)
                            .animationPadding(-9.0f)
                            .text("یہاں کلک کریں")
                            .backgroundColor(Color.WHITE)
                            .textColor(getResources().getColor(R.color.line_divider))
                            .gravity(Gravity.TOP)
                            .animated(true)
                            .transparentOverlay(false)
                            .build()
                            .show();
                }
            },1500);

        }
        else
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new SimpleTooltip.Builder(getActivity())
                        .anchorView(yourView)
                        .animationPadding(-9.0f)
                        .text("Tap Here To Proceed")
                        .backgroundColor(Color.WHITE)
                        .textColor(getResources().getColor(R.color.line_divider))
                        .gravity(Gravity.TOP)
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show();
            }
        },1500);


        toggleSwitch = (ToggleSwitch) view.findViewById(R.id.toggleSwitch);
        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                boolean isAll = position == 1;
                sa.clear();
                for (int i = 0; i < jarr.length(); i++) {
                    try {
                        JSONArray inner = jarr.getJSONArray(i);
                        String i_ = inner.getString(0);
                        String b_ = inner.getString(1);
                        String s_ = inner.getString(2);
                        if (s_.equals(ts[position]) || isAll)
                            sa.addEntry(new ListData(i_, b_, s_));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                sa.notifyDataSetChanged();
            }
        });


        sa = new SampleAdapter();
        lv = (ListView) view.findViewById(R.id.list_View);
        lv.setAdapter(sa);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String id = sa.getItem(i).Prescription;
                GetSingle async = new GetSingle(getActivity(), new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String jarr = (String) result;
                        Toast.makeText(getActivity(), jarr, Toast.LENGTH_SHORT).show();
                        if (jarr == null) {
                            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("No Network")
                                    .show();
                            return;
                        }
                    }
                }, URL + id + "__PHP");
                async.execute();
            }
        });

        view.findViewById(R.id.btnvv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                functionCall();
                downAnimation(HEALTH_CARDS).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        upAnimation(LIST_VIEW).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                LIST_VIEW.clearAnimation();
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });
                        HEALTH_CARDS.clearAnimation();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
            }
        });
        view.findViewById(R.id.btnvhc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downAnimation(LIST_VIEW).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        upAnimation(HEALTH_CARDS).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                HEALTH_CARDS.clearAnimation();
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        });
                        LIST_VIEW.clearAnimation();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                });
            }
        });
    }


    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            main = new ArrayList<>();
        }

        public void clear() {
            main.clear();
        }

        public void addEntry(ListData l) {
            main.add(l);
        }

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
               /* if (position == main.size() - 1)
                    convertView = View.inflate(getContext(), R.layout.___list_item_kitchenlast, null);
                else*/
                convertView = View.inflate(getActivity(), R.layout.___list_item_voucher, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            try {
                ListData item = getItem(position);

                holder.Prescription.setText(item.Prescription);
                holder.Detail.setText("Click Here");
                holder.Status.setText(item.Status);
                if (item.Status.equals(ts[0])) {
                    Drawable bgDrawable = holder.Status.getBackground();
                    bgDrawable.setColorFilter(getResources().getColor(android.R.color.darker_gray), PorterDuff.Mode.SRC_IN);
                }
                if (item.Status.equals(ts[2])) {
                    Drawable bgDrawable = holder.Status.getBackground();
                    bgDrawable.setColorFilter(getResources().getColor(R.color.line_divider), PorterDuff.Mode.SRC_IN);
                }
                if (item.Status.equals("Paid")) {
                    Drawable bgDrawable = holder.Status.getBackground();
                    bgDrawable.setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.SRC_IN);
                }

            } catch (IndexOutOfBoundsException e) {
            }

            return convertView;
        }
    }

    private static class ViewHolder {
        private View view;

        private TextView Prescription;
        private TextView Status;
        private TextView Detail;

        private ViewHolder(View view) {
            this.view = view;
            Prescription = (TextView) view.findViewById(R.id.prescriptiontv);
            Status = (TextView) view.findViewById(R.id.statustv);
            Detail = (TextView) view.findViewById(R.id.stv);
        }
    }

    private class ListData {
        private String Prescription;
        private String ID;
        private String Status;

        public ListData(String id, String p, String s) {
            ID = id;
            Prescription = p;
            Status = s;
        }
    }

    public void functionCall() {
        GetSingle async = new GetSingle(getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                jarr = (JSONArray) result;
                if (jarr == null) {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                }
                for (int i = 0; i < jarr.length(); i++) {
                    try {
                        JSONArray inner = jarr.getJSONArray(i);
                        String i_ = inner.getString(0);
                        String b_ = inner.getString(1);
                        String s_ = inner.getString(2);
                        if (s_.equals("Pending"))
                            sa.addEntry(new ListData(i_, b_, s_));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                sa.notifyDataSetChanged();
            }
        }, "ViewPendingTransection/03145362496@webdoc.com.pk" + "__0");
        async.execute();
    }


}
