package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupCorPanelAdd;

import org.json.JSONArray;
import org.json.JSONException;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link _3_Fragment_2_CorporatePanel#newInstance} factory method to
 * create an instance of this fragment.
 */
public class _3_Fragment_2_CorporatePanel extends Fragment {

    private static final String KEY_MOVIE_TITLE = "key_title";

    TextView Company, Empno, Empcnic, Emprelation;
    JSONArray Arr;
    Button b1;

    public _3_Fragment_2_CorporatePanel() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment _3_Fragment_2_CorporatePanel.
     */
    static _3_Fragment_2_CorporatePanel fragment = new _3_Fragment_2_CorporatePanel();

    public static _3_Fragment_2_CorporatePanel newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _3_Fragment_2_CorporatePanel();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_3_fragment_2_corporatepanel, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);




        Company = (TextView) view.findViewById(R.id.CorPanel_Company);
        Empno = (TextView) view.findViewById(R.id.CorPanel_EmpNo);
        Empcnic = (TextView) view.findViewById(R.id.CorPanel_EmpCnic);
        Emprelation = (TextView) view.findViewById(R.id.CorPanel_Relation);

        b1 = (Button) view.findViewById(R.id.editPanel);


        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("کارپوریٹ پینل");

            ((TextView) view.findViewById(R.id.comptext)).setText("کمپنی");
            ((TextView) view.findViewById(R.id.comptext)).setPadding(0,0,40,0);
            ((TextView) view.findViewById(R.id.reltext)).setText("امپلاۓ نمبر");
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0,0,40,0);
            ((TextView) view.findViewById(R.id.cnictext)).setText("امپلاۓ شناختی کارڈ نمبر");
            ((TextView) view.findViewById(R.id.cnictext)).setPadding(0,0,40,0);
            ((TextView) view.findViewById(R.id.reltext)).setText("امپلاۓ سے رشتہ");
            ((TextView) view.findViewById(R.id.reltext)).setPadding(0,0,40,0);


        }


        ///////////////////////////////////////////////////////////////////////////////////////////

        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            b1.setVisibility(View.GONE);
        }


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(), __PopupCorPanelAdd.class);
                /*i_.putExtra();
                i_.putExtra();
                i_.putExtra();
                i_.putExtra();*/
                startActivityForResult(i_, 101);
            }
        });

        Arr = MySingleton.getInstance().getArray(7);

        try {
            if (!MySingleton.getInstance().getCpanel()) {

                if (MySingleton.getInstance().getLang())

                {
                    b1.setText("پینل کی معلومات\nشامل کریں");
                }
                else
                    b1.setText("Add Panel\nInformation");
                setHasOptionsMenu(true);
            } else

            if (MySingleton.getInstance().getLang())

            {
                b1.setText("پینل کی معلومات\nمیں ترمیم کریں");
            }
            else
                b1.setText("Edit Panel\nInformation");

            Company.setText(Arr.get(2) + "");
            Empno.setText(Arr.get(3) + "");
            Empcnic.setText(Arr.get(4) + "");
            Emprelation.setText(Arr.get(5) + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            try {

                if (resultCode == Activity.RESULT_OK) {
                    MySingleton.getInstance().editPopup(getContext());
                } else if (resultCode == 50) {
                    MySingleton.getInstance().addPopup(getContext());
                }

                Arr = MySingleton.getInstance().getArray(7);
                if (Arr.equals(new JSONArray("[[\"\",\"\",\"\",\"\",\"\",\"\"]]"))) {
                    MySingleton.getInstance().setCpanel(false);
                } else
                    MySingleton.getInstance().setCpanel(true);

                if (!MySingleton.getInstance().getCpanel()) {
                    b1.setText("Add Panel\nInformation");
                    setHasOptionsMenu(true);
                } else
                    b1.setText("Edit Panel\nInformation");

                Company.setText(Arr.get(2) + "");
                Empno.setText(Arr.get(3) + "");
                Empcnic.setText(Arr.get(4) + "");
                Emprelation.setText(Arr.get(5) + "");

            } catch (JSONException e) {
            }
        }

    }

}
