package com.webdoc.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.webdoc.MySingleton;
import com.webdoc.R;
import com.webdoc.__PopupEditAdd;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by faheem on 04/07/2017.
 */

public class _2_Fragment_3_Medications extends Fragment {
    private static final String KEY_MOVIE_TITLE = "key_title";

    ArrayList<ListData> mDataList;
    View _v_;

    ListView lv;
    JSONArray jarr;
    boolean Network_b = false;

    public _2_Fragment_3_Medications() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment FragmentDrama.
     */
    static _2_Fragment_3_Medications fragment = new _2_Fragment_3_Medications();

    public static _2_Fragment_3_Medications newInstance(String movieTitle) {

        Bundle args = new Bundle();
        args.putString(KEY_MOVIE_TITLE, movieTitle);
        try {
            fragment.setArguments(args);
        } catch (IllegalStateException e) {
            fragment = new _2_Fragment_3_Medications();
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      /*  /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.__fragment_2_fragment_3_medications, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        _v_=view;

        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) view.findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.heading)).setTextSize(14);
            ((TextView) view.findViewById(R.id.heading)).setGravity(Gravity.CENTER);
            ((TextView) view.findViewById(R.id.heading)).setText("ادویات");

            ((TextView) view.findViewById(R.id.destext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.destext)).setText("تفصیل");

            ((TextView) view.findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.reltext)).setText("موجودہ سٹیٹس");

            ((TextView) view.findViewById(R.id.datetext)).setTypeface(tf);
            ((TextView) view.findViewById(R.id.datetext)).setText("تاریخ مقرر");

        }


        if (!MySingleton.getInstance().isConnectedButton(getActivity())) {
            view.findViewById(R.id.floatingActionButton).setVisibility(View.GONE);
            setHasOptionsMenu(true);
            Network_b = false;
        } else
            Network_b = true;

        lv = view.findViewById(R.id.listView);
        jarr = MySingleton.getInstance().getArray(3);

        mDataList = new ArrayList<ListData>();
        String a;

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                a = Uri.encode(jobj.getString(3));
                a = a.replace("%C2%A0", " ");
                a = a.replace("%20", " ");

                mDataList.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", a));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (mDataList.get(0).Description.equals("")) {
            mDataList.remove(0);
            view.findViewById(R.id.topRow).setVisibility(View.GONE);
        }
        lv.setAdapter(new SampleAdapter());

        if (MySingleton.getInstance().isConnectedButton(getActivity())) {

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent i_ = new Intent(getContext(), __PopupEditAdd.class);
                    if (MySingleton.getInstance().getLang())
                    {
                        i_.putExtra("Title", "طبی حالت کی تفصیل");

                    }
                    else {
                        i_.putExtra("Title", "Medical Conditions Description");

                    }                    i_.putExtra("Value", mDataList.get(i).Description);
                    i_.putExtra("Id", mDataList.get(i).ID);
                    i_.putExtra("Date", mDataList.get(i).DataPrescribed);
                    i_.putExtra("Status", mDataList.get(i).Status);
                    i_.putExtra("Index", i + "");
                    startActivityForResult(i_, 101);
                }
            });
        } else
            setHasOptionsMenu(true);

        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);
        fab.hide();
        fab.animate().translationY(fab.getHeight() + 500).setInterpolator(new AccelerateInterpolator(2)).start();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i_ = new Intent(getContext(), __PopupEditAdd.class);
                if (MySingleton.getInstance().getLang())
                {
                    i_.putExtra("Title", "طبی حالت کی تفصیل");

                }
                else {
                    i_.putExtra("Title", "Medical Conditions Description");

                }
                startActivityForResult(i_, 101);
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //fab.showMenuButton(true);
                if (Network_b){
                    fab.show();
                fab.bringToFront();
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();}
            }
        }, 1500);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {

            if (resultCode == Activity.RESULT_OK) {
                MySingleton.getInstance().editPopup(getContext());
            } else if (resultCode == 50) {
                MySingleton.getInstance().addPopup(getContext());
            }
            jarr = MySingleton.getInstance().getArray(3);
            mDataList.clear();
            String a;
            for (int i = 0; i < jarr.length(); i++) {
                try {
                    JSONArray jobj = jarr.getJSONArray(i);
                    a = Uri.encode(jobj.getString(3));
                    a = a.replace("%C2%A0", " ");
                    a = a.replace("%20", " ");
                    mDataList.add(new ListData(jobj.get(0) + "", jobj.get(1) + "", jobj.get(2) + "", a));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (mDataList.get(0).Description.equals("")) {
                mDataList.remove(0);
                _v_.findViewById(R.id.topRow).setVisibility(View.GONE);
            } else
                _v_.findViewById(R.id.topRow).setVisibility(View.VISIBLE);

            lv.setAdapter(new SampleAdapter());
        }

    }


    private class SampleAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mDataList.size();
        }

        @Override
        public ListData getItem(int position) {
            return mDataList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.___list_item_triple, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            ListData item = getItem(position);

           /* TextDrawable drawable = mDrawableBuilder.build(String.valueOf(item.Name.charAt(0)), mColorGenerator.getColor(item.Name));
            holder.imageView.setImageDrawable(drawable);
            holder.view.setBackgroundColor(Color.TRANSPARENT);
            holder.textView.setText(item.Name);*/

            holder.DataPrescribed.setText(item.DataPrescribed);
            holder.Description.setText(item.Description);
            holder.Status.setText(item.Status);
            if (Network_b)

                if (MySingleton.getInstance().getLang())
                {
                    holder.edit.setText("ترمیم");
                    holder.edit.setTextColor(Color.RED);

                }
                holder.edit.setTextColor(Color.RED);

            return convertView;
        }
    }

    private static class ViewHolder {

        private View view;

        private TextView Description;
        private TextView DataPrescribed;
        private TextView Status;
        private TextView edit;

        private ViewHolder(View view) {
            this.view = view;
            Description = (TextView) view.findViewById(R.id.tvMedicationDescription);
            DataPrescribed = (TextView) view.findViewById(R.id.tvMedicationDatePrescribed);
            Status = (TextView) view.findViewById(R.id.tvMedicationStatus);
            edit = view.findViewById(R.id.tvMedicationEdit);
        }
    }

    private static class ListData {

        private String Description;
        private String DataPrescribed;
        private String Status;
        private String ID;

        public ListData(String id, String DataPrescribed, String Description, String Status) {
            this.ID = id;
            this.Description = Description;
            this.DataPrescribed = DataPrescribed.replace(" 12:00AM", "");
            this.Status = Status;
        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.doctor_list);
        MySingleton.getInstance().setActivityName("FragmentDoctorList");
        //item.setIcon(null);
    }

}