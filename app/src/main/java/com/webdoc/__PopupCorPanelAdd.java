package com.webdoc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupCorPanelAdd extends AppCompatActivity {
    ArrayList<String> listC,listR;

    Spinner COMPANY, RELATION;
    EditText EMPNO, EMPCNIC;

    String function__;

    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_cor_panel_add);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////



        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.popup_company)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_company)).setTextSize(14);
            ((TextView) findViewById(R.id.popup_company)).setGravity(Gravity.CENTER);
            ((TextView) findViewById(R.id.popup_company)).setText("کمپنی");

            ((TextView) findViewById(R.id.popup_empno)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_empno)).setText("امپلاۓ نمبر");

            ((TextView) findViewById(R.id.popup_cnic)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_cnic)).setText("امپلاۓ شناختی کارڈ نمبر");

            ((TextView) findViewById(R.id.popup_relation)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_relation)).setText("امپلاۓ سے رشتہ");

            ((Button) findViewById(R.id.clinicType)).setTypeface(tf);
            ((Button) findViewById(R.id.clinicType)).setText("سیو");
        }



        listC = new ArrayList<String>();
        if (MySingleton.getInstance().getLang()){
            listC.add("- منتخب کریں -");

        }
        listC.add("- Select -");

        JSONArray jarr = MySingleton.getInstance().getArray(11);

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                listC.add(jobj.get(1) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        listR = new ArrayList<String>();
        if (MySingleton.getInstance().getLang()){
            listR.add("- منتخب کریں -");

        }
        listR.add("- Select -");

        jarr = MySingleton.getInstance().getArray(12);

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                listR.add(jobj.get(1) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .7));

        COMPANY = (Spinner) findViewById(R.id.spinner_company);
        ArrayAdapter<String> adapterC = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listC);
        adapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        COMPANY.setAdapter(adapterC);

        EMPNO = (EditText) findViewById(R.id.editText_Employee_no);
        EMPCNIC = (EditText) findViewById(R.id.editText_cnic);

        RELATION = (Spinner) findViewById(R.id.spinner_relation);
        ArrayAdapter<String> adapterR = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listR);
        adapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        RELATION.setAdapter(adapterR);

        JSONArray Arr = MySingleton.getInstance().getArray(7);

        try {
            if (!MySingleton.getInstance().getCpanel()) {
                function__ = "AddCorporatePatient/";
            } else
                function__ = "EditCorporatePatient/";


            int spinnerPosition = adapterC.getPosition(Arr.getString(2));
            try {
                COMPANY.setSelection(spinnerPosition);
            } catch (NumberFormatException e) {
                COMPANY.setSelection(0);
            }

            EMPNO.setText(Arr.getString(3));
            EMPCNIC.setText(Arr.getString(4));

            spinnerPosition = adapterR.getPosition(Arr.getString(5));
            try {
                RELATION.setSelection(spinnerPosition);
            } catch (NumberFormatException e) {
                RELATION.setSelection(0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void editCondition(View view) {
        //{email}/{employeeNo}/{companyId}/{EmployeeCNIC}/{EmployeeRelationId}

        String _final_ = function__ + MySingleton.getInstance().getUserId() + "/" + EMPNO.getText() + "/" + COMPANY.getSelectedItemPosition() + "/" + EMPCNIC.getText() + "/" + RELATION.getSelectedItemPosition();

        final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                Intent returnIntent = new Intent();

                if (jarr == null) {
                    new SweetAlertDialog(__PopupCorPanelAdd.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                            .show();
                    return;
                } else {
                    try {
                        MySingleton.getInstance().updateArray(7, jarr.getJSONArray(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (function__.split("/")[0].equals("EditCorporatePatient"))
                        setResult(Activity.RESULT_OK, returnIntent);
                    else
                        setResult(50, returnIntent);
                    finish();
                }
            }
        }, _final_ + "__" + "0");
        asyncTask.execute();
    }
}
