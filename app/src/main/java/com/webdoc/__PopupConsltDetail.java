package com.webdoc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;
import org.json.JSONException;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupConsltDetail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_conslt_detail);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .65));
        String function__ = "ConsultationDetails/" + MySingleton.getInstance().getUserId() + "/" + getIntent().getStringExtra("ID");

        final GetSingle asyncTask = new GetSingle(__PopupConsltDetail.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                if (jarr == null) {
                    new SweetAlertDialog(__PopupConsltDetail.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                } else if (jarr.equals("\"\"")) {
                } else {
                    try {
                        JSONArray jobj = jarr.getJSONArray(0);

                        ((TextView) findViewById(R.id.CD_Date)).setText(jobj.get(0) + "");
                        ((TextView) findViewById(R.id.CD_Doctor)).setText(jobj.get(1) + "");
                        ((TextView) findViewById(R.id.CD_CT)).setText(jobj.get(2) + "");
                        ((TextView) findViewById(R.id.CD_PM)).setText(jobj.get(3) + "");
                        //((TextView)findViewById(R.id.CD_IP)).setText("-");
                        ((TextView) findViewById(R.id.CD_CC)).setText(jobj.get(4) + "");
                        //((TextView)findViewById(R.id.CD_C)).setText(jobj.get(6)+"");
                        //((TextView)findViewById(R.id.CD_D)).setText(jobj.get(7)+"");
                        ((TextView) findViewById(R.id.CD_P)).setText(jobj.get(8) + "");
                        ((TextView) findViewById(R.id.CD_T)).setText(jobj.get(9) + "");
                        ((TextView) findViewById(R.id.CD_R)).setText(jobj.get(10) + "");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, function__ + "__" + "0");
        asyncTask.execute();

        /*WCFHandler wcf = new WCFHandler(this);

        JSONArray jarr = wcf.GetJsonResult("ConsultationDetails/" + MySingleton.getInstance().getUserId() + "/" + getIntent().getStringExtra("ID"));*/

    }
}
