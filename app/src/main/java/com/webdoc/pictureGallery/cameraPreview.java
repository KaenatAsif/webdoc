package com.webdoc.pictureGallery;

/**
 * Created by faheem on 16/05/2017.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class cameraPreview extends Activity {

    private RequestQueue requestQueue;
    private static final String URL = "http://webdoc.com.pk/ImagesUploadByCam/upload.php";
    private StringRequest request;
    final Calendar c = Calendar.getInstance();
    SimpleDateFormat df;

    String DIR = "";
    int COUNT = 0;
    int PICTURE_COUNT = 0;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._picture_gallery_fromgallery);

        requestQueue = Volley.newRequestQueue(this);

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                boolean a = false;
                MySingleton.getInstance().dismissLoadingPopup();
                GetSingle async = new GetSingle(cameraPreview.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        Boolean a = false;
                        final Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();

                    }
                }, "AddClaimImage/" + MySingleton.getInstance().getUserId() + "/" + DIR + "/" + PICTURE_COUNT + "__1");
                // PICTURE_COUNT++;
                // sa.notifyDataSetChanged();
                async.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Boolean a = false;
                MySingleton.getInstance().dismissLoadingPopup();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", MySingleton.getInstance().getUserId());
                hashMap.put("dir", DIR);
                if (COUNT == -1)
                    hashMap.put("flag", "New");
                else {
                    hashMap.put("flag", "OLD");
                    String uploadImage = getStringImage(bitmap);
                    hashMap.put("image", uploadImage);
                    hashMap.put("name", PICTURE_COUNT + "");

                    bitmap.recycle();
                    bitmap = null;
                    System.gc();
                }
                return hashMap;
            }
        };
        int socketTimeout = 60000; // 60 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        request.setRetryPolicy(policy);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .8));

        ImageView im = (ImageView) findViewById(R.id.stillshot_imageview);

        String Path = getIntent().getStringExtra("path");
        final String DID = getIntent().getStringExtra("DID");
        final String PID = getIntent().getStringExtra("PID");

        df = new SimpleDateFormat("yyyy-MM-dd");
        final String formattedDate = df.format(c.getTime());

        Boolean check = getIntent().getBooleanExtra("FromGallery", true);

        if (check) {
            InputStream imageStream = null;
            try {
                imageStream = getApplicationContext().getContentResolver().openInputStream(Uri.parse(Path));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bitmap = BitmapFactory.decodeStream(imageStream);
        } else {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(Path, options);
        }

        im.setImageBitmap(bitmap);

        findViewById(R.id.retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Boolean sb = getIntent().getBooleanExtra("SendBack", false);
                if (sb) {
                    final Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    MySingleton.getInstance().showLoadingPopup(cameraPreview.this, "Uploading Image - Please Wait");
                    requestQueue.add(request);
                }


            }
        });

        DIR = getIntent().getStringExtra("dir");
        PICTURE_COUNT = getIntent().getIntExtra("picture_count", 0);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
