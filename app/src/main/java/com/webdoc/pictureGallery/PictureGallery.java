package com.webdoc.pictureGallery;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.batch.android.Batch;
import com.batch.android.Config;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.webdoc.AsyncResponse;
import com.webdoc.GetSingle;
import com.webdoc.MySingleton;
import com.webdoc.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.webdoc._MainActivity.getToolbarNavigationIcon;

/**
 * Created by fahee on 1/10/2018.
 */

public class PictureGallery extends AppCompatActivity {
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
    View HOME;

    private RequestQueue requestQueue;
    private static final String URL = "http://webdoc.com.pk/ImagesUploadByCam/upload.php";
    private StringRequest request;


    String DIR = "";
    int COUNT = 0;
    int PICTURE_COUNT = 0;
    Bitmap bitmap;

    int width, height;

    ArrayList<ListData> main;
    SampleAdapter sa;
    ListView lv;

    String Lnumber = "", Llink = "", Mnumber = "", Mlink = "", Rnumber = "", Rlink = "";
    int i, j, LIST_INDEX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout._picture_gallery);


        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

       /* getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);
*/


        Toolbar topToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);


        HOME = getToolbarNavigationIcon(topToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        requestQueue = Volley.newRequestQueue(this);

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                boolean a = false;
                MySingleton.getInstance().dismissLoadingPopup();
                GetSingle async = new GetSingle(PictureGallery.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        Boolean a = false;
                        final Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }, "AddClaimImage/" + MySingleton.getInstance().getUserId() + "/" + DIR + "/" + PICTURE_COUNT + "__1");
                // PICTURE_COUNT++;
                // sa.notifyDataSetChanged();
                async.execute();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Boolean a = false;
                MySingleton.getInstance().dismissLoadingPopup();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("email", MySingleton.getInstance().getUserId());
                hashMap.put("dir", DIR);
                if (COUNT == -1)
                    hashMap.put("flag", "New");
                else {
                    hashMap.put("flag", "OLD");
                    String uploadImage = getStringImage(bitmap);
                    hashMap.put("image", uploadImage);
                    hashMap.put("name", PICTURE_COUNT + "");

                    bitmap.recycle();
                    bitmap = null;
                    System.gc();
                }
                return hashMap;
            }
        };
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        request.setRetryPolicy(policy);

        height = MySingleton.getInstance().getHeight();
        width = MySingleton.getInstance().getWidth();

        sa = new SampleAdapter();
        lv = findViewById(R.id.listView);
        lv.setAdapter(sa);

        functionCall();

        if (sa.getCount() == 0) {
            new SweetAlertDialog(PictureGallery.this, SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText("Upload Photos")
                    .setContentText("1)Medicine receipt,\n2)Id card front side,\n3)Id card back side")

                    .show();
            return;
        }

        if (sa.getCount() < 5) {
            for (int i = sa.getCount(); i < 5; i++) {
                int j = i * 3;
                //sa.addEntry(new ListData(j + "-" + ++j + "-" + ++j, "", "", "", "", "", ""));
            }
        }
    }

    public void functionCall() {
        final GetSingle async = new GetSingle(PictureGallery.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                if (jarr == null) {
                    new SweetAlertDialog(PictureGallery.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Network")
                            .show();
                    return;
                }


                try {
                    MySingleton.getInstance().setClaimNo(jarr.getJSONArray(0).getString(0));
                    for (i = 0; i < jarr.length(); i = i + 3) {
                        j = i;
                        Lnumber = Llink = Mnumber = Mlink = Rnumber = Rlink = "";

                        JSONArray left = jarr.getJSONArray(i);
                        Lnumber = left.getString(0);
                        Llink = left.getString(1);

                        JSONArray middle = jarr.getJSONArray(i + 1);
                        Mnumber = middle.getString(0);
                        Mlink = middle.getString(1);

                        JSONArray right = jarr.getJSONArray(i + 2);
                        Rnumber = right.getString(0);
                        Rlink = right.getString(1);


                        sa.addEntry(new ListData(j + "-" + ++j + "-" + ++j, Llink, Lnumber, Mlink, Mnumber, Rlink, Rnumber));
                        LIST_INDEX++;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (!Lnumber.equals("") && !Llink.equals("")) {
                        sa.addEntry(new ListData(j + "-" + ++j + "-" + ++j, Llink, Lnumber, Mlink, Mnumber, Rlink, Rnumber));
                    }


                }
            }
        }, "GetClaimsHistory/" + MySingleton.getInstance().getUserId() + "/" + getIntent().getStringExtra("_ID_") + "__0");
        async.execute();
    }

    public void submitClaim(View view) {
        String dir = MySingleton.getInstance().getClaimNo();


        GetSingle async = new GetSingle(PictureGallery.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {


                new SweetAlertDialog(PictureGallery.this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Result")
                        .setContentText("Documents submitted successfully")
                        .show();
                return;


            }
        }, "DocumentSubmitted/" + MySingleton.getInstance().getUserId() + "/" + dir + "__1");
        async.execute();


    }

    public void opengallery(View view, String dir, int count) {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");

        DIR = dir;
        COUNT = 100;
        PICTURE_COUNT = count;

        startActivityForResult(photoPickerIntent, 110);
    }


    private class SampleAdapter extends BaseAdapter {

        SampleAdapter() {
            main = new ArrayList<>();
        }

        public void addEntry(ListData l) {
            main.add(l);
            notifyDataSetChanged();
        }

        public void updateEntry(int p, ListData l) {
            main.remove(p);
            main.add(p, l);
            notifyDataSetChanged();
        }

        public void clear() {
            main.clear();
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return main.size();
        }

        @Override
        public ListData getItem(int position) {
            return main.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;

            if (convertView == null) {
               /* if (position == main.size() - 1)
                    convertView = View.inflate(getContext(), R.layout.___list_item_kitchenlast, null);
                else*/
                convertView = View.inflate(PictureGallery.this, R.layout.___list_item_gallery, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final ListData item = getItem(position);

            if (item.DrawableL.equals("") && item.DrawableR.equals("")) {
                holder.ProgressBarL.setVisibility(View.GONE);
                holder.ProgressBarR.setVisibility(View.GONE);
                return convertView;
            }

            if (!item.NameL.equals("")) {
                holder.NameL.setText(item.NameL);
                holder.NameL.setTextColor(getResources().getColor(R.color.black_p50));
                holder.imageViewL.setContentDescription(position + "");

                holder.ProgressBarL.setVisibility(View.VISIBLE);

                if (true) {

                    Glide.with(PictureGallery.this)
                            .load(item.DrawableL)
                            .override(width, height)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.ProgressBarL.setVisibility(View.GONE);
                                    holder.NameL.setVisibility(View.VISIBLE);
                                    holder.imageViewL.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imageViewL);

                    holder.imageViewL.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int position = Integer.parseInt(view.getContentDescription().toString());
                            startActivity(new Intent(PictureGallery.this, ClaimImagePopup.class).putExtra("_LINK_", sa.getItem(position).DrawableL));
                        }
                    });
                }
            }

            if (!item.NameM.equals("")) {
                holder.NameM.setText(item.NameM);
                holder.NameM.setTextColor(getResources().getColor(R.color.black_p50));
                holder.imageViewM.setContentDescription(position + "");

                holder.ProgressBarM.setVisibility(View.VISIBLE);

                if (true) {
                    Glide.with(PictureGallery.this)
                            .load(item.DrawableM)
                            .override(width, height)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    holder.ProgressBarM.setVisibility(View.GONE);
                                    holder.NameM.setVisibility(View.VISIBLE);
                                    holder.imageViewM.setVisibility(View.VISIBLE);
                                    return false;
                                }
                            })
                            .into(holder.imageViewM);

                    holder.imageViewM.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int position = Integer.parseInt(view.getContentDescription().toString());
                            startActivity(new Intent(getApplicationContext(), ClaimImagePopup.class).putExtra("_LINK_", sa.getItem(position).DrawableM));
                        }
                    });
                }

                if (!item.NameR.equals("")) {
                    holder.NameR.setText(item.NameR);
                    holder.NameR.setTextColor(getResources().getColor(R.color.black_p50));
                    holder.imageViewR.setContentDescription(position + "");

                    holder.ProgressBarR.setVisibility(View.VISIBLE);

                    if (true) {
                        Glide.with(getApplicationContext())
                                .load(item.DrawableR)
                                .override(width, height)
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        holder.ProgressBarR.setVisibility(View.GONE);
                                        holder.NameR.setVisibility(View.VISIBLE);
                                        holder.imageViewR.setVisibility(View.VISIBLE);
                                        return false;
                                    }
                                })
                                .into(holder.imageViewR);

                        holder.imageViewR.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int position = Integer.parseInt(view.getContentDescription().toString());
                                startActivity(new Intent(getApplicationContext(), ClaimImagePopup.class).putExtra("_LINK_", sa.getItem(position).DrawableR));
                            }
                        });
                    }
                }
            }

            return convertView;

        }
    }

    private static class ViewHolder {
        private View view;

        private TextView NameL;
        private ImageView imageViewL;
        private ProgressBar ProgressBarL;

        private TextView NameM;
        private ImageView imageViewM;
        private ProgressBar ProgressBarM;

        private TextView NameR;
        private ImageView imageViewR;
        private ProgressBar ProgressBarR;

        private ViewHolder(View view) {
            this.view = view;
            NameL = (TextView) view.findViewById(R.id.NameL);
            ProgressBarL = (ProgressBar) view.findViewById(R.id.progressBarL);
            imageViewL = (ImageView) view.findViewById(R.id.imageLeft);

            NameM = (TextView) view.findViewById(R.id.NameM);
            ProgressBarM = (ProgressBar) view.findViewById(R.id.progressBarM);
            imageViewM = (ImageView) view.findViewById(R.id.imageMiddle);

            NameR = (TextView) view.findViewById(R.id.NameR);
            ProgressBarR = (ProgressBar) view.findViewById(R.id.progressBarR);
            imageViewR = (ImageView) view.findViewById(R.id.imageRight);
        }
    }

    private class ListData {
        private String Name;
        private String DrawableL;
        private String NameL;
        private String DrawableM;
        private String NameM;
        private String DrawableR;
        private String NameR;

        public ListData(String n, String dl, String nl, String dm, String nm, String dr, String nr) {
            Name = n;

            if (!nl.equals("null")) {
                NameL = nl;
                DrawableL = dl;
            } else {
                NameL = "";
                DrawableL = "";
            }

            if (!nm.equals("null")) {
                NameM = nm;
                DrawableM = dm;
            } else {
                NameM = "";
                DrawableM = "";
            }

            if (!nr.equals("null")) {
                NameR = nr;
                DrawableR = dr;
            } else {
                NameR = "";
                DrawableR = "";
            }
        }
    }

    public void openCamera(View view) {
        String[] Parts;
        int count = 0;
        for (int i = 0; i < sa.getCount(); i++) {
            ListData ld = sa.getItem(i);
            Parts = ld.Name.split("-");
            if (ld.DrawableL.equals("")) {
                count = Integer.parseInt(Parts[0]);
                break;
            } else if (ld.DrawableM.equals("")) {
                count = Integer.parseInt(Parts[1]);
                break;
            } else if (ld.DrawableR.equals("")) {
                count = Integer.parseInt(Parts[2]);
                break;
            } else {
                count = count + 3;
            }
        }
        String dir = MySingleton.getInstance().getClaimNo();

        takePhoto(dir, count);
    }

    public void openGallery(View view) {
        String[] Parts;
        int count = 0;
        for (int i = 0; i < sa.getCount(); i++) {
            ListData ld = sa.getItem(i);
            Parts = ld.Name.split("-");
            if (ld.DrawableL.equals("")) {
                count = Integer.parseInt(Parts[0]);
                break;
            } else if (ld.DrawableM.equals("")) {
                count = Integer.parseInt(Parts[1]);
                break;
            } else if (ld.DrawableR.equals("")) {
                count = Integer.parseInt(Parts[2]);
                break;
            } else {
                count = count + 3;
            }
        }
        String dir = MySingleton.getInstance().getClaimNo();

        opengallery(view, dir, count);
    }

    public void takePhoto(String dir, int count) {

        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
//            fileTemp = ImageUtils.getOutputMediaFile();
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            DIR = dir;
            COUNT = 100;
            PICTURE_COUNT = count;

            startActivityForResult(intent, TAKE_PICTURE);
//            } else {
//                Toast.makeText(this, getString(R.string.error_create_image_file), Toast.LENGTH_LONG).show();
//            }
        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = imageUri;
                    getContentResolver().notifyChange(selectedImage, null);
                    ContentResolver cr = getContentResolver();

                    try {
                        bitmap = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, selectedImage);

                        //bitmap = Bitmap.createScaledBitmap(bitmap, 450, 1500, false);

                        Toast.makeText(this, selectedImage.toString(),
                                Toast.LENGTH_LONG).show();

                        MySingleton.getInstance().showLoadingPopup(PictureGallery.this, "Uploading Image - Please Wait");
                        requestQueue.add(request);


                        //C - 25 / 01
                    } catch (Exception e) {
                        Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
            case 110:
                if (data == null)
                    return;
                Uri selectedImage = data.getData();

                Intent preview = new Intent(PictureGallery.this, cameraPreview.class);
                preview.putExtra("path", selectedImage + "");
                preview.putExtra("FromGallery", true);


                preview.putExtra("dir", DIR);
                preview.putExtra("picture_count", PICTURE_COUNT);
                startActivityForResult(preview, 999);

                // To display selected image in image view
            case 999:
                if (resultCode == Activity.RESULT_OK) {
                    sa.clear();
                    functionCall();
                }
        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

}
