package com.webdoc.pictureGallery;

import android.animation.Animator;
import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.liuguangqiang.progressbar.CircleProgressBar;
import com.liuguangqiang.swipeback.SwipeBackLayout;
import com.webdoc.R;

/**
 * Created by Eric on 15/2/27.
 */
public class ClaimImagePopup extends Activity {

    private CircleProgressBar progressBar;
    private SwipeBackLayout swipeBackLayout;
    private ProgressBar imProgressBar;
    private ImageView imageView;

    private static final int ANIM_DURATION_TOOLBAR = 750;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_claim_image);


        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        initViews();
        Toast.makeText(this, "", Toast.LENGTH_LONG).show();
    }
    private void initViews() {
        animation();

        progressBar = (CircleProgressBar) findViewById(R.id.progressbar1);
        swipeBackLayout = (SwipeBackLayout) findViewById(R.id.swipe_layout);
        swipeBackLayout.setEnableFlingBack(false);
        imProgressBar = findViewById(R.id.progressBar);
        imageView = findViewById(R.id._im_);

        swipeBackLayout.setOnPullToBackListener(new SwipeBackLayout.SwipeBackListener() {
            @Override
            public void onViewPositionChanged(float fractionAnchor, float fractionScreen) {
                progressBar.setProgress((int) (progressBar.getMax() * fractionAnchor));
            }
        });

        imProgressBar.setVisibility(View.VISIBLE);
        Glide.with(getApplicationContext())
                .load(getIntent().getStringExtra("_LINK_"))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        imProgressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .into(imageView);

        findViewById(R.id.click2close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ClaimImagePopup.this, "asd", Toast.LENGTH_SHORT).show();
                closeScreen(view);
            }
        });
    }

    public void animation() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        findViewById(R.id._main_).setTranslationY(height);

        findViewById(R.id._main_).animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(0);
    }

    public void closeScreen(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;

        findViewById(R.id._main_).animate().setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                finish();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        })
                .translationY(height)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(0);
    }
}
