package com.webdoc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.SinchError;
import com.webdoc.messenger.BaseActivityLauncher;
import com.webdoc.messenger.SinchService;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LauncherActivity extends BaseActivityLauncher implements SinchService.StartFailedListener {



    @Override
    protected void onServiceConnected() {
        int a = BuildConfig.VERSION_CODE;
        GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                String res = (String) result;
                if (res.equals("\"True\"")) {
                    new SweetAlertDialog(LauncherActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    finish();

                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }

                                }
                            })
                            .setContentText("Your application is outdated, kindly download the latest version")
                            .show();
                } else {
                    if (!getSinchServiceInterface().isStarted()) {
                        getSinchServiceInterface().setStartListener(LauncherActivity.this);
                    }
                    if (MySingleton.getInstance().isConnectedButton(LauncherActivity.this)) {
                        checkInfo();
                    } else {
                        startActivity(new Intent(LauncherActivity.this, _Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        finish();
                    }
                }

            }
        }, "VersionCheck/" + a + "__" + "1");
        asyncTask.execute();

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        com.batch.android.Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        com.batch.android.Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

    }

    @Override
    protected void onPause() {
        super.onPause();

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        com.batch.android.Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        com.batch.android.Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        com.batch.android.Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        com.batch.android.Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        Window w = getWindow();

        w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        //setContentView(R.layout.activity_launcher);
    }

    private void checkInfo() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String uname = sharedpref.getString("UserName", "0");
        String pass = sharedpref.getString("Password", "0");

        if (uname != "0" && pass != "0") {
            if (!getSinchServiceInterface().isStarted()) {
                getSinchServiceInterface().startClient(uname);
            }
            MySingleton.getInstance().setUserId(uname);
            startActivity(new Intent(this, _MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        } else {
            startActivity(new Intent(this, _Splash.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
          /*  /////////////////////////////////////////////////////////////////////////////////
            /// Batch service : push notification
            com.batch.android.Batch.Push.setGCMSenderId("554280630926");

            // TODO : switch to live Batch Api Key before shipping
            com.batch.android.Batch.setConfig(new Config("DEV5A9F7B9DC1FADA6227D4E6C8C99")); // devloppement
            // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////
*/
        }
    }
}
