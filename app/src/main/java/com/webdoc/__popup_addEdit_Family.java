package com.webdoc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __popup_addEdit_Family extends AppCompatActivity {

    //    DatePicker Date;
    Spinner spinner;

    ArrayList<String> listC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_add_edit__family);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);



        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.popup_title)).setTypeface(tf);
            ((TextView) findViewById(R.id.popup_title)).setText("خاندان کی رجسٹریشن");


            ((TextView) findViewById(R.id.notext)).setTypeface(tf);
            ((TextView) findViewById(R.id.notext)).setText("موبائل نمبر");

            ((TextView) findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) findViewById(R.id.reltext)).setText("رشتہ");

            ((Button) findViewById(R.id.clinicType)).setTypeface(tf);
            ((Button) findViewById(R.id.clinicType)).setText("سیو");

        }
        else
            ((TextView) findViewById(R.id.popup_title)).setText("Family Registration");


//        Date = (DatePicker) findViewById(R.id.datePicker);
        spinner = (Spinner) findViewById(R.id.spinner_hv);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        if (getIntent().getStringExtra("Title").equals("Family Registration")) {
            getWindow().setLayout((int) (width * .9), (int) (height * .5));
            ((RelativeLayout) findViewById(R.id.hiddenView)).setVisibility(View.VISIBLE);


        } else {
            getWindow().setLayout((int) (width * .9), (int) (height * .5));
        }

        //((TextView) findViewById(R.id.popup_title)).setText(getIntent().getStringExtra("Title"));
        ((EditText) findViewById(R.id.editText_Condition_Edit)).setText(getIntent().getStringExtra("Value"));

        listC = new ArrayList<String>();

        if (MySingleton.getInstance().getLang())
            listC.add("- منتخب کریں -");

        else

        listC.add("- Select -");

        JSONArray jarr = MySingleton.getInstance().getArray(12);

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                listC.add(jobj.get(1) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayAdapter<String> adapterC = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listC);
            adapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapterC);

        }
    }

    public void AddFamilyMember(View view) {

        String ans = ((EditText) findViewById(R.id.editText_Condition_Edit)).getText() + "";
        ans = Uri.encode(ans.trim().replaceAll("\n", " "));


        if (spinner.getSelectedItemPosition() == 0) {
            new SweetAlertDialog(__popup_addEdit_Family.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Please select a Relationship")
                    .show();
            return;

        }
        String status = Uri.encode(spinner.getSelectedItemPosition() + "");


        WCFHandler wcf = new WCFHandler(this);

        String id = getIntent().getStringExtra("Id");
        JSONArray jarr;

        final Intent returnIntent = new Intent();

        if (getIntent().getStringExtra("Title").equals("Family Registration")) {
            if (ans.equals("")) {
                new SweetAlertDialog(__popup_addEdit_Family.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("No number entered")
                        .show();
                return;
            }
            else if (ans.length()<11){
                new SweetAlertDialog(__popup_addEdit_Family.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText("Incorrect number")
                        .show();
                return;

            }
            ///ViewFamilyMember/{email}
            //**if (id != null) {
            String function__ = "AddFamilyMember/" + MySingleton.getInstance().getUserId() + "/" + ans + "/" + status;

            final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONArray jarr = (JSONArray) result;

                    if (jarr == null) {
                        new SweetAlertDialog(__popup_addEdit_Family.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    } else {
                        MySingleton.getInstance().updateFamilyArray(0, jarr);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }
            }, function__ + "__" + "0");
            asyncTask.execute();
            ///RemoveFamilyMember/{email}/{MobileNo}
        } else {
        }


    }
}