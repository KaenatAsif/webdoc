package com.webdoc.trophyscreen;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.R;
import com.webdoc.messenger.AudioPlayer;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class TrophyScreen extends AppCompatActivity {

    KonfettiView konfettiView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trophy);

        /////////////////////////////////////////////////////////////////////////////////
//        Batch service : push notification
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        konfettiView = (KonfettiView) findViewById(R.id.konfettiView);

        String message = getIntent().getStringExtra("_TEXT_");
        message = message.replace("-", "\n");

        ((TextView) findViewById(R.id.text_trophy)).setText(message);

        //-50f, konfettiView.getWidth() + 50f, -50f, -50f
        //

        //((ImageView) findViewById(R.id.im)).getHeight();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                burst(0);
            }
        }, 500);

        new AudioPlayer(TrophyScreen.this).playCheers();
    }

    public void claimReward(View view) {
        finish();
    }

    private void burst(int i) {
        if (i == 3)
            return;
        konfettiView.build()
                .addColors(Color.WHITE, Color.YELLOW, Color.RED, Color.YELLOW, Color.BLUE, Color.CYAN, Color.MAGENTA, Color.GREEN)
                .setDirection(0.0, 359.0)
                .setSpeed(0f, 12f)
                .setFadeOutEnabled(true)
                .setTimeToLive(3000L)
                .addShapes(Shape.RECT, Shape.CIRCLE, Shape.RECT, Shape.CIRCLE)
                .addSizes(new Size(12, 5f))
                .setPosition(konfettiView.getX() + konfettiView.getWidth() / 2, konfettiView.getY() + konfettiView.getHeight() / 4f)
                .burst(85);
        //.stream(100, 3000L);
        final int j = i + 1;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                burst(j);
            }
        }, 4000);
    }
}
