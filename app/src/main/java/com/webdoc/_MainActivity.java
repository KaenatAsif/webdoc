package com.webdoc;

import android.animation.Animator;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.webdoc.fragment.FragmentDoctorList;
import com.webdoc.fragment.navigation.FragmentNavigationManager;
import com.webdoc.fragment.navigation.NavigationManager;
import com.webdoc.mainPage.MainPage;
import com.webdoc.messenger.BaseActivity;
import com.webdoc.trophyscreen.TrophyScreen;
import com.wooplr.spotlight.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class _MainActivity extends BaseActivity {

    @Override
    protected void onStart()
    {
        super.onStart();
        Batch.onStart(this);
    }

    @Override
    protected void onStop()
    {
        Batch.onStop(this);

        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        Batch.onDestroy(this);

        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Batch.onNewIntent(this, intent);
        super.onNewIntent(intent);
    }


    static LinearLayout belowDoctors;
    static RelativeLayout roundLogo;

    private void initDoctorAnimation() {
        final ImageButton button = (ImageButton) findViewById(R.id.doctor_im);

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        final Animation myAnimOut = AnimationUtils.loadAnimation(this, R.anim.bounce_out);
        final Animation myAnimIn = AnimationUtils.loadAnimation(this, R.anim.bounce_in);



        myAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.startAnimation(myAnimIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        myAnimOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.startAnimation(myAnimIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        myAnimIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                button.startAnimation(myAnimOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        button.startAnimation(myAnim);
    }


    String[] SharedPrefNames = {"PatientProfile", "Conditions", "Allergies", "Medications", "Immunizations", "Procedures", "Insurance", "Corporate", "Consultations"};
    static final String[] Fragments = {"GeneralProfile","BuyOnline", "DoctorList", "Conditions","Allergies","Immunizations","Medications","Procedures","Corporate","Insurance",
            "Claims","Consultation Report","Call Summary","Family Registration"};


    String function__;
    View HOME, logoView;
    int actionbarSize;

    private NavigationManager mNavigationManager;

    Toolbar topToolBar;

    private static final int ANIM_DURATION_TOOLBAR = 300;
    private static final int ANIM_DURATION_FAB = 1000;

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, 15).show();
            } else {
                Log.e("MainActivity", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);

        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.reltext)).setTypeface(tf);
            ((TextView) findViewById(R.id.reltext)).setText("ڈاکٹر");
        }


            belowDoctors = (LinearLayout) findViewById(R.id.linearLayout);

        roundLogo= (RelativeLayout) findViewById(R.id.RoundImageView);


        checkPlayServices();

        mNavigationManager = FragmentNavigationManager.obtain(this);

        topToolBar = (Toolbar) findViewById(R.id.toolbar);
        MySingleton.getInstance().setToolbar(topToolBar);

        setSupportActionBar(topToolBar);
        topToolBar.setTitle("");
        /*topToolBar.setSubtitle("");
        topToolBar.setLogoDescription("");*/

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setHomeButtonEnabled(true);

        HOME = getToolbarNavigationIcon(topToolBar);

        actionbarSize = Utils.dpToPx(56);
        topToolBar.setTranslationY(-actionbarSize);

        logoView = topToolBar.getChildAt(0);

        HOME.setTranslationY(-actionbarSize);
        logoView.setTranslationY(-actionbarSize);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        findViewById(R.id.activity_controller).setTranslationY(Utils.dpToPx(height));

        //functionCall(savedInstanceState);

        toolbarAnimation();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[0]))
            selectGeneralProfile();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[1]))
            showBuyOnline();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[2]))
            showDoctorList();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[3]))
            selectConditions();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[4]))
            showAllergies();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[5]))
            showImmunizations();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[6]))
            showMedications();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[7]))
            showProcedures();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[8]))
            openCorporate();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[9]))
            openInsurance();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[10]))
            openClaims();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[11]))
            showConsultation();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[12]))
            showCallsummary();
        if (getIntent().getStringExtra("Fragment").equals(Fragments[13]))
            openFamily();

    }

    private void openFamily() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Family Registration";
            mNavigationManager.showFragmentFamilyRegistration(firstActionMovie);

        }
    }

    private void showCallsummary() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Call Summary";
            mNavigationManager.showFragmentCallSummary(firstActionMovie);

        }
    }

    private void showConsultation() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Consultation Report";
            mNavigationManager.showFragmentConsultationReports(firstActionMovie);

        }
    }

    private void openClaims() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Claims";
            mNavigationManager.showFragmentClaims(firstActionMovie);

        }
    }

    private void openInsurance() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Insurance";
            mNavigationManager.showFragmentInsuranceProducts(firstActionMovie);

        }

    }


    private void openCorporate() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Corporate";
            mNavigationManager.showFragmentCorporatePanel(firstActionMovie);

        }
    }

    private void selectGeneralProfile() {
        if (mNavigationManager != null) {
            String firstActionMovie = "General Profile";
            mNavigationManager.showFragmentGeneralProfile(firstActionMovie);

            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    private void selectConditions() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Conditions";
            mNavigationManager.showFragmentConditions(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    private void showBuyOnline() {
        if (mNavigationManager != null) {
            String firstActionMovie = "Buy Online";
            mNavigationManager.showFragmentHealthCards(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

        private void showAllergies() {
            if (mNavigationManager != null) {
                String firstActionMovie = "Allergies";
                mNavigationManager.showFragmentAllergies(firstActionMovie);
                //        getSupportActionBar().setTitle(firstActionMovie);
            }
        }

        private void showImmunizations() {

            if (mNavigationManager != null) {
                String firstActionMovie = "Immunizations";
                mNavigationManager.showFragmentImmunizations(firstActionMovie);
                //        getSupportActionBar().setTitle(firstActionMovie);
            }
        }
        private void showMedications() {
            if (mNavigationManager != null) {
                String firstActionMovie = "Medications";
                mNavigationManager.showFragmentMedications(firstActionMovie);
                //        getSupportActionBar().setTitle(firstActionMovie);
            }
        }
        private void showProcedures() {
            if (mNavigationManager != null) {
                String firstActionMovie = "Procedures";
                mNavigationManager.showFragmentProcedures(firstActionMovie);
                //        getSupportActionBar().setTitle(firstActionMovie);
            }
        }


    private void showDoctorList() {
        if (mNavigationManager != null) {
            String firstActionMovie = "DoctorList";
            mNavigationManager.showFragmentDoctorList(firstActionMovie);
            //        getSupportActionBar().setTitle(firstActionMovie);
        }
    }

    @Override
    protected void onServiceConnected() {
        Log.d("", "SINCH CONNECTED MAINACTIVITY");
        //logout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.doctor_search);

        MenuItem ItemD = menu.findItem(R.id.doctor_list);
        ItemD.setIcon(null);

        /*if (MySingleton.getInstance().getRegisterStatus() || true) {
            MenuItem ItemD = menu.findItem(R.id.doctor_list);
            ItemD.setIcon(null);
        }*/
        SearchManager searchManager = (SearchManager) _MainActivity.this.getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {
                    Toast.makeText(getApplicationContext(), "Our word : " + s, Toast.LENGTH_SHORT).show();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    FragmentDoctorList.getInstance().onDoctorSearch(s);
                    return false;
                }
            });
            EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setTextColor(Color.WHITE);
            searchEditText.setHintTextColor(Color.parseColor("#eeeeee"));

        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(_MainActivity.this.getComponentName()));
        }

        MenuItem mi = menu.findItem(R.id.doctor_list);

        /*CALL = menu.findItem(R.id.doctor_list).getActionView();
        CALL.setTranslationY(-actionbarSize);*/

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {

        /*FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();

        android.support.v4.app.Fragment myFragment = new Fr();//my custom fragment

        fragTrans.replace(android.R.id.content, myFragment,"");
        fragTrans.addToBackStack(null);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();

        Fragment tag = (Fragment) getSupportFragmentManager().findFragmentByTag();
      //  if(Fragments.equals("GeneralProfile"))*/
        if (MySingleton.getInstance().getPro()) {
            MySingleton.getInstance().setGprofile(true);
            MySingleton.getInstance().showLoadingPopup(_MainActivity.this, "Please Wait");

           /* Intent gp= new Intent(_MainActivity.this,MainPage.class);
            startActivity(gp);
            MySingleton.getInstance().setPro(false);

*/
            MySingleton.getInstance().getUserId();
            startActivity(new Intent(getApplicationContext(), MainPage.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            MySingleton.getInstance().dismissLoadingPopup();
            MySingleton.getInstance().setPro(false);
        }


else


        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Activate the navigation drawer toggle
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static View getToolbarNavigationIcon(Toolbar toolbar) {
        // check if contentDescription previously was set
        boolean hadContentDescription = TextUtils.isEmpty(toolbar.getNavigationContentDescription());
        String contentDescription = !hadContentDescription ? (String) toolbar.getNavigationContentDescription() : " navigationIcon ";
        toolbar.setNavigationContentDescription(contentDescription);
        ArrayList<View> potentialViews = new ArrayList<View>();
        // find the view based on it's content description, set programatically or with android:contentDescription
        toolbar.findViewsWithText(potentialViews, contentDescription, View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);
        // Nav icon is always instantiated at this point because calling setNavigationContentDescription ensures its existence
        View navIcon = null;
        if (potentialViews.size() > 0) {
            navIcon = potentialViews.get(0);  // navigation icon is ImageButton
        }
        // Clear content description if not previously present
        if (hadContentDescription)
            toolbar.setNavigationContentDescription(null);
        return navIcon;
    }

    public void logout() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        MySingleton.getInstance().setBoolean(true);
        MySingleton.getInstance().MainList.clear();
        getSinchServiceInterface().stopClient();
        startActivity(new Intent(_MainActivity.this, _Splash.class));
        finish();
    }

    void toolbarAnimation() {

        topToolBar.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(300);

        HOME.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(600);

        logoView.animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_TOOLBAR)
                .setStartDelay(750);

        try {
            findViewById(R.id.doctor_list).setTranslationY(-actionbarSize);
            findViewById(R.id.doctor_list).animate()
                    .translationY(0)
                    .setDuration(ANIM_DURATION_TOOLBAR)
                    .setStartDelay(1000);
        } catch (NullPointerException e) {
        }

        findViewById(R.id.activity_controller).animate()
                .translationY(0)
                .setDuration(ANIM_DURATION_FAB)
                .setStartDelay(1000).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                initDoctorAnimation();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    private void functionCall(final Bundle savedInstanceState) {
        if (getGoOnline()) {
            if (MySingleton.getInstance().isConnected(this)) {
                MySingleton.getInstance().showLoadingPopup(this, "Fetching Profile - Please Wait");
                function__ = "GetPatientProfile/" + MySingleton.getInstance().getUserId();
                final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        JSONArray jsn = (JSONArray) result;
                        if (jsn == null) {
                            MySingleton.getInstance().dismissLoadingPopup();
                            new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Something went wrong!")
                                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                            "If the problem persists, please contact us at support@webdoc.com.pk")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                            logout();
                                        }
                                    })
                                    .show();
                            return;
                        }
                        try {
                            for (int i = 0; i < jsn.length(); i++) {
                                if (i == 0) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    MySingleton.getInstance().setArray(Arr);
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setGprofile(false);
                                    } else
                                        MySingleton.getInstance().setGprofile(true);
                                } else if (i == 6) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setIproduct(false);
                                    } else
                                        MySingleton.getInstance().setIproduct(true);
                                    MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());
                                } else if (i == 7) {
                                    JSONArray Arr = jsn.getJSONArray(i).getJSONArray(0);
                                    if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                                        MySingleton.getInstance().setCpanel(false);
                                    } else
                                        MySingleton.getInstance().setCpanel(true);
                                    MySingleton.getInstance().setArray(Arr);
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], Arr.toString());
                                } else {
                                    MySingleton.getInstance().setArray(jsn.getJSONArray(i));
                                    MySingleton.getInstance().addSharedPrefEntry(SharedPrefNames[i], jsn.getJSONArray(i).toString());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        function__ = "DoctorList";
                        final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                            @Override
                            public void processFinish(Object result) {
                                JSONArray jarr = (JSONArray) result;
                                MySingleton.getInstance().addSharedPrefEntry("DoctorList", jarr.toString());
                                MySingleton.getInstance().setArray(jarr);

                                function__ = "CountryList";
                                final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                                    @Override
                                    public void processFinish(Object result) {
                                        JSONArray jarr = (JSONArray) result;
                                        MySingleton.getInstance().addSharedPrefEntry("CountryList", jarr.toString());
                                        MySingleton.getInstance().setArray(jarr);

                                        function__ = "CorporateCompaines";
                                        final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                                            @Override
                                            public void processFinish(Object result) {
                                                JSONArray jarr = (JSONArray) result;
                                                MySingleton.getInstance().addSharedPrefEntry("CorporateCompanies", jarr.toString());
                                                MySingleton.getInstance().setArray(jarr);

                                                function__ = "CorporateRelationShip";
                                                final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                                                    @Override
                                                    public void processFinish(Object result) {
                                                        JSONArray jarr = (JSONArray) result;
                                                        MySingleton.getInstance().addSharedPrefEntry("CorporateRelationShip", jarr.toString());
                                                        MySingleton.getInstance().setArray(jarr);

                                                        function__ = "FreeReward/" + MySingleton.getInstance().getUserId();
                                                        final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
                                                            @Override
                                                            public void processFinish(Object result) {
                                                                String res = ((String) result).replace("\"", "");
                                                                String link = res.split(",")[0];
                                                                final String text = res.split(",")[1];
                                                                //http:\/\/webdoc.com.pk\/imagesofsystem\/PrizeMoney.png
                                                                link = link.replaceAll("\\\\", "");

                                                                //UrlImageViewHelper.setUrlDrawable((ImageView) findViewById(R.id.awardIM), link);

                                                                toolbarAnimation();
                                                                if (savedInstanceState == null) {
                                                                    selectConditions();
                                                                }
                                                                JSONArray j1 = MySingleton.getInstance().getArray(0);

                                                                setGoOnline(false);
                                                                MySingleton.getInstance().dismissLoadingPopup();

                                                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        if (!text.equals("False")) {
                                                                            startActivity(new Intent(_MainActivity.this, TrophyScreen.class).putExtra("_TEXT_", text));
                                                                        }
                                                                    }
                                                                }, 3500);

                                                            }
                                                        }, function__ + "__" + "1");
                                                        asyncTask.execute();
                                                    }
                                                }, function__ + "__" + "0");
                                                asyncTask.execute();
                                            }
                                        }, function__ + "__" + "0");
                                        asyncTask.execute();
                                    }
                                }, function__ + "__" + "0");
                                asyncTask.execute();
                            }
                        }, function__ + "__" + "0");
                        asyncTask.execute();
                    }
                }, function__ + "__" + "0");
                asyncTask.execute();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "No Network Available", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        } else {
            MySingleton.getInstance().Contextt_ = getApplicationContext();
            try {
                for (int i = 0; i < 9; i++) {
                    if (i == 0) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setGprofile(false);
                        } else
                            MySingleton.getInstance().setGprofile(true);
                    } else if (i == 6) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setIproduct(false);
                        } else
                            MySingleton.getInstance().setIproduct(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else if (i == 7) {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                            MySingleton.getInstance().setCpanel(false);
                        } else
                            MySingleton.getInstance().setCpanel(true);
                        MySingleton.getInstance().setArray(Arr);
                    } else {
                        JSONArray Arr = MySingleton.getInstance().getSharedPrefEntry(SharedPrefNames[i]);
                        MySingleton.getInstance().setArray(Arr);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
           JSONArray jarr = MySingleton.getInstance().getSharedPrefEntry("DoctorList");
            MySingleton.getInstance().setArray(jarr);
            /*jarr = MySingleton.getInstance().getSharedPrefEntry("CountryList");
            MySingleton.getInstance().setArray(jarr);
            jarr = MySingleton.getInstance().getSharedPrefEntry("CorporateCompanies");
            MySingleton.getInstance().setArray(jarr);
            jarr = MySingleton.getInstance().getSharedPrefEntry("CorporateRelationShip");
            MySingleton.getInstance().setArray(jarr);*/

            function__ = "FreeReward/" + MySingleton.getInstance().getUserId();
            final GetSingle asyncTask = new GetSingle(_MainActivity.this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    String res = ((String) result).replace("\"", "");
                    MySingleton.getInstance().setFreeReward(res);
                    String link = res.split(",")[0];
                    final String text = res.split(",")[1];
                    //http:\/\/webdoc.com.pk\/imagesofsystem\/PrizeMoney.png
                    link = link.replaceAll("\\\\", "");
                    JSONArray j1 = MySingleton.getInstance().getArray(0);
                    if (savedInstanceState == null) {
                        selectConditions();
                    }
                    toolbarAnimation();
                    setGoOnline(false);
                    MySingleton.getInstance().dismissLoadingPopup();
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!text.equals("False")) {
                                startActivity(new Intent(_MainActivity.this, TrophyScreen.class).putExtra("_TEXT_", text));
                            }
                        }
                    }, 3500);

                }
            }, function__ + "__" + "1");
            asyncTask.execute();
        }
    }

    private Boolean getGoOnline() {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        boolean b = sharedpref.getBoolean("GoOnline", true);
        return b;
    }

    private void setGoOnline(boolean b) {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putBoolean("GoOnline", b);
        editor.apply();
    }




    public static void hideDoctorLayout() {
        belowDoctors.findViewById(R.id.doctor_im).clearAnimation();
        belowDoctors.setVisibility(View.GONE);
    }

public static void hideLogoRound(){
    roundLogo.setVisibility(View.GONE);
}

    public void openDoctor(View view)
    {
       /* function__ = "DoctorList";
        final GetProfile asyncTask = new GetProfile(_MainActivity.this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                MySingleton.getInstance().addSharedPrefEntry("DoctorList", jarr.toString());
                MySingleton.getInstance().setArray(jarr);
                JSONArray j1 = MySingleton.getInstance().getArray(0);

                setGoOnline(false);
                MySingleton.getInstance().dismissLoadingPopup();


            }
        }, function__ + "__" + "0");
        asyncTask.execute();*/
        if (!MySingleton.getInstance().getGprofile()) {
            new SweetAlertDialog(_MainActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Please complete your general profile first")
                    .show();
            return;
        }
        showDoctorList();

    }

}
