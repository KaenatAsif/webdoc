package com.webdoc;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupEditProfile extends AppCompatActivity {

    ArrayList<String> list;

    EditText FN, LN, CNIC;
    EditText DOB;
    Spinner GEN;
    EditText ADDR;
    Spinner COUN, CITY;
    EditText MOB;

    String function__;

    private int mYear, mMonth, mDay;

    ArrayList Months = new ArrayList(Arrays.asList("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"));

    static boolean first_time = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.___popup_edit_profile);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);




        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .85));





        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);



        if (MySingleton.getInstance().getLang()) {
            ((TextView) findViewById(R.id.heading)).setTypeface(tf);
            ((TextView) findViewById(R.id.heading)).setText("پروفائل کی تفصیلات");


            ((TextView) findViewById(R.id.fntext)).setTypeface(tf);
            ((TextView) findViewById(R.id.fntext)).setText("پہلا نام");

            ((TextView) findViewById(R.id.ln)).setTypeface(tf);
            ((TextView) findViewById(R.id.ln)).setText("آخری نام");

            ((TextView) findViewById(R.id.cnic)).setTypeface(tf);
            ((TextView) findViewById(R.id.cnic)).setText("شناختی کارڈ نمبر");

            ((TextView) findViewById(R.id.dobtext)).setTypeface(tf);
            ((TextView) findViewById(R.id.dobtext)).setText("پیدائش کی تاریخ");

            ((TextView) findViewById(R.id.gentext)).setTypeface(tf);
            ((TextView) findViewById(R.id.gentext)).setText("صنف");

            ((TextView) findViewById(R.id.addtext)).setTypeface(tf);
            ((TextView) findViewById(R.id.addtext)).setText("پتہ");

            ((TextView) findViewById(R.id.counttext)).setTypeface(tf);
            ((TextView) findViewById(R.id.counttext)).setText("ملک");

            ((TextView) findViewById(R.id.citytext)).setTypeface(tf);
            ((TextView) findViewById(R.id.citytext)).setText("شہر");

            ((TextView) findViewById(R.id.mobiletext)).setTypeface(tf);
            ((TextView) findViewById(R.id.mobiletext)).setText("فون نمبر");

            ((Button) findViewById(R.id.clinicType)).setTypeface(tf);
            ((Button) findViewById(R.id.clinicType)).setText("سیو");



        }


        list = new ArrayList<String>();
        list.add("- Select -");

        JSONArray jarr = MySingleton.getInstance().getArray(10);

        for (int i = 0; i < jarr.length(); i++) {
            try {
                JSONArray jobj = jarr.getJSONArray(i);
                list.add(jobj.get(1) + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONArray Arr = MySingleton.getInstance().getArray(0);

        try {
            if (Arr.equals(new JSONArray("[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\"]"))) {
                function__ = "AddPatientProfile/";
                MySingleton.getInstance().setPro(true);
            } else
                function__ = "EditPatientProfile/";{
            MySingleton.getInstance().setPro(true);
                }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        String[] ddaattee = getIntent().getStringExtra("Date").split(" ");
        String D = ddaattee[0];
        D = D.replace("Jan", "1");
        D = D.replace("Feb", "2");
        D = D.replace("Mar", "3");
        D = D.replace("Apr", "4");
        D = D.replace("May", "5");
        D = D.replace("Jun", "6");
        D = D.replace("Jul", "7");
        D = D.replace("Aug", "8");
        D = D.replace("Sep", "9");
        D = D.replace("Oct", "10");
        D = D.replace("November", "11");
        D = D.replace("Dec", "12");

        String Gender = "", City = "", Country = "";
        try {
            Gender = Arr.get(4) + "";
            City = Arr.get(7) + "";
            Country = Arr.get(6) + "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Gender = Gender.replace("Male", "1");
        Gender = Gender.replace("male", "1");
        Gender = Gender.replace("Female", "2");
        Gender = Gender.replace("female", "2");
        City = City.replace("Islamabad", "1");
        City = City.replace("Rawalpindi", "2");
        City = City.replace("Lahore", "3");
        City = City.replace("Karachi", "4");
        City = City.replace("Other", "5");

        //Country = Country.replace("Pakistan", "1");


        FN = (EditText) findViewById(R.id.editText_FirstName);
        LN = (EditText) findViewById(R.id.editText_LastName);
        CNIC = (EditText) findViewById(R.id.editText_CNIC);

        DOB = (EditText) findViewById(R.id.editText_DOB);
        DOB.setFocusableInTouchMode(false);
        DOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Current Date
                String date = DOB.getText() + "";
                if (!date.equals("")) {
                    ArrayList current = new ArrayList(Arrays.asList(date.split(" ")));
                    if (current.get(1).equals(""))
                        current.remove(1);
                    mYear = Integer.parseInt(current.get(2) + "");
                    mMonth = Months.indexOf(current.get(0));
                    mDay = Integer.parseInt(current.get(1) + "");
                } else {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(__PopupEditProfile.this, R.style.DialogTheme,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                DOB.setText(Months.get(monthOfYear) + " " + dayOfMonth + " " + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.setCancelable(false);
            }
        });
        //DOB = (DatePicker) findViewById(R.id.datePickerDOB);

        GEN = (Spinner) findViewById(R.id.spinner_Gender);

        ADDR = (EditText) findViewById(R.id.editText_Address);

        COUN = (Spinner) findViewById(R.id.spinner_Country);
        CITY = (Spinner) findViewById(R.id.spinner_City);

        MOB = (EditText) findViewById(R.id.editText_MobileNumber);

        ArrayAdapter<String> adapterC = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        adapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        COUN.setAdapter(adapterC);

        try {
            FN.setText(Arr.get(0) + "");
            LN.setText(Arr.get(1) + "");
            CNIC.setText(Arr.get(2) + "");
            //DOB.updateDate(Integer.valueOf(ddaattee[2]), Integer.valueOf(D) - 1, Integer.valueOf(ddaattee[1]));
            DOB.setText((Arr.get(3) + "").replace(" 12:00AM", ""));

            try {
                GEN.setSelection(Integer.parseInt(Gender));
            } catch (NumberFormatException e) {
                GEN.setSelection(0);
            }

           ADDR.setText((Arr.get(5) + "").replace("@slash@","/"));



            try {
                CITY.setSelection(Integer.parseInt(City));
            } catch (NumberFormatException e) {
                CITY.setSelection(0);
            }

            int spinnerPosition = adapterC.getPosition(Country.toUpperCase());
            try {
                COUN.setSelection(spinnerPosition);
            } catch (NumberFormatException e) {
                COUN.setSelection(0);
            }

            if (Arr.getString(8).equals("")) {
                String input = MySingleton.getInstance().getUserId().split("@")[0];
                String regex = "^[0-9]+$";
                Matcher matcher = Pattern.compile(regex).matcher(input);
                if (matcher.find()) {
                    String result = matcher.group();
                    System.out.println("number=" + result);
                    MOB.setText(result);
                } else {
                    System.out.println("no match found");
                    MOB.setText("");
                }
            } else {
                MOB.setText(Arr.get(8) + "");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*if (CNIC.getText().toString().equals("")) {
            CNIC.setFocusable(true);
            CNIC.setClickable(true);
            CNIC.setEnabled(true);
        }
        if (MOB.getText().toString().equals("")) {
            MOB.setFocusable(true);
            MOB.setClickable(true);
            MOB.setEnabled(true);
        }*/

    }

    public void editProfile(View view) {
        MySingleton.getInstance().showLoadingPopup(this, "Updating Profile - Please Wait");

        String error = "";

        if (GEN.getSelectedItemPosition() == 0)
            error += "No gender selected";
        if (CITY.getSelectedItemPosition() == 0)
            error += "\nNo city selected";
        if (COUN.getSelectedItemPosition() == 0)
            error += "\nNo country selected";
        if (!error.equals("")) {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(__PopupEditProfile.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText(error)
                    .show();
            return;
        }


        /*String firstN_ = FN.getText() + "";
        String lastN_ = LN.getText() + "";
        String cnic_ = CNIC.getText() + "";

        *//*String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        int D = DOB.getDayOfMonth();
        int Mon = DOB.getMonth();
        int Y = DOB.getYear();*//*

        //String date_ = Y + "-" + Months[Mon] + "-" + D;
        String gender_ = GEN.getSelectedItem() + "";
        String address_ = ADDR.getText() + "";
        String addr_ = Uri.encode(address_);
        String city_ = CITY.getSelectedItem() + "";
        String country_ = COUN.getSelectedItem() + "";
        String mob_ = MOB.getText() + "";*/

        String firstN_ = Uri.encode(FN.getText() + "");
        String lastN_ = Uri.encode(LN.getText() + "");
        String cnic_ = Uri.encode(CNIC.getText() + "");

        String date_ = Uri.encode(DOB.getText() + "");
        String gender_ = Uri.encode(GEN.getSelectedItem() + "");

        String address_ = (ADDR.getText() + "").replace("/","@slash@");
        String addr_ = Uri.encode(address_.trim().replaceAll("\n", " "));

        String city_ = Uri.encode(CITY.getSelectedItem() + "");
        String country_ = Uri.encode(COUN.getSelectedItem() + "");
        String mob_ = Uri.encode(MOB.getText() + "");


        String _final_ = function__ + MySingleton.getInstance().getUserId() + "/" + firstN_ + "/" + lastN_ + "/" + cnic_ + "/" + date_ + "/" + gender_ + "/" + addr_ + "/" + country_ + "/" + city_ + "/" + mob_;
        final GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {
                JSONArray jarr = (JSONArray) result;
                if (jarr == null) {
                    MySingleton.getInstance().dismissLoadingPopup();
                    new SweetAlertDialog(__PopupEditProfile.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                    "If the problem persists, please contact us at support@webdoc.com.pk")
                            .show();
                    return;
                } else {
                    try {

                        jarr = jarr.getJSONArray(0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MySingleton.getInstance().updateArray(0, jarr);
                    Intent returnIntent = new Intent();
                    if (function__.split("/")[0].equals("EditPatientProfile"))
                        setResult(Activity.RESULT_OK, returnIntent);
                    else
                        setResult(50, returnIntent);
                    finish();
                    MySingleton.getInstance().dismissLoadingPopup();
                }
            }
        }, _final_ + "__" + "0");
        asyncTask.execute();





        /*WCFHandler wcf = new WCFHandler(this);
        JSONArray jarr = wcf.GetJsonResult("EditPatientProfile/" + MySingleton.getInstance().getUserId() + "/" + firstN_ + "/" + lastN_ + "/" + cnic_ + "/" + date_ + "/" + gender_ + "/" + addr_ + "/" + country_ + "/" + city_ + "/" + mob_);
        if (jarr == null) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                            "If the problem persists, please contact us at support@webdoc.com.pk")
                    .show();
            return;
        } else {
            try {

                jarr = jarr.getJSONArray(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            MySingleton.getInstance().updateArray(0, jarr);
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }*/
    }
}
