package com.webdoc;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.batch.android.Batch;
import com.batch.android.Config;

import org.json.JSONArray;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class __PopupInsuranceEditAdd extends AppCompatActivity {

    Spinner s;
    EditText et;
    String oldProduct, oldMSISDN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.___popup_insurance_edit_add);

        /////////////////////////////////////////////////////////////////////////////////
/// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////URDU////////////////////////////////////////////////
        // Give path to the Font location
        String Path2font = "DroidNaskh-Regular.ttf";
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);


        ((TextView) findViewById(R.id.popup_title)).setTypeface(tf);
        ((TextView) findViewById(R.id.popup_title)).setText("انشورنس پراڈکٹ");

        ((Button) findViewById(R.id.clinicType)).setTypeface(tf);
        ((Button) findViewById(R.id.clinicType)).setText("سیو");


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .75), (int) (height * .4));



        s = (Spinner) findViewById(R.id.spinner_hv);
        et = (EditText) findViewById(R.id.editText_msisdn);

        if (getIntent().getStringExtra("Value") != null && getIntent().getStringExtra("MSISDN") != null) {
            String product = getIntent().getStringExtra("Value");

            product = product.replace("Sehat Sahara Plus", "1");
            product = product.replace("Kamyab Mustaqbil", "2");
            product = product.replace("Muhafiz", "3");
            product = product.replace("Rakhwala", "4");

            s.setSelection(Integer.parseInt(product));


            String msisdn = getIntent().getStringExtra("MSISDN");

            et.setText(msisdn);

            oldProduct = product;
            oldMSISDN = msisdn;
        }

    }

    public void buttonClick(View view) {
        String Product = s.getSelectedItemPosition() + "";
        String mNo = et.getText() + "";

        Product = Product.replace("Sehat Sahara Plus", "1");
        Product = Product.replace("Kamyab Mustaqbil", "2");
        Product = Product.replace("Muhafiz", "3");
        Product = Product.replace("Rakhwala", "4");

        WCFHandler wcf = new WCFHandler(this);
        final Intent returnIntent = new Intent();
        String id = getIntent().getStringExtra("Id");
        JSONArray jarr;

        String result = "";
        if (mNo.equals("")) {
            result = "No Mobile Number";
        }
        if (Product.equals("- Select Product -")) {
            if (mNo.equals(""))
                result += " and ";
            result += "No Product Selected";
        }
        if (!result.equals("")) {
            new SweetAlertDialog(__PopupInsuranceEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error!")
                    .setContentText(result)
                    .show();
            return;
        }

        if (id != null) {
            String function__ = "EditInsurancePatient/" + MySingleton.getInstance().getUserId() + "/" + id + "/" + mNo + "/" + Product;
            final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONArray jarr = (JSONArray) result;
                    if (jarr == null || jarr.length() == 0 || jarr.equals(MySingleton.getInstance().getArray(6))) {
                        new SweetAlertDialog(__PopupInsuranceEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    } else {
                        setResult(Activity.RESULT_OK, returnIntent);
                        MySingleton.getInstance().updateArray(6, jarr);
                        finish();
                    }
                }
            }, function__ + "__" + "0");
            asyncTask.execute();

        } else {
            String function__ = "AddInsurancePatient/" + MySingleton.getInstance().getUserId() + "/" + mNo + "/" + Product;
            final GetSingle asyncTask = new GetSingle(this, new AsyncResponse() {
                @Override
                public void processFinish(Object result) {
                    JSONArray jarr = (JSONArray) result;
                    if (jarr == null || jarr.length() == 0 || (jarr.length() == MySingleton.getInstance().getArray(6).length())) {
                        new SweetAlertDialog(__PopupInsuranceEditAdd.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Something went wrong!")
                                .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                                        "If the problem persists, please contact us at support@webdoc.com.pk")
                                .show();
                        return;
                    } else {
                        setResult(50, returnIntent);
                        MySingleton.getInstance().updateArray(6, jarr);
                        finish();
                    }
                }
            }, function__ + "__" + "0");
            asyncTask.execute();
        }


    }
}
