package com.webdoc;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import com.webdoc.messenger.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by faheem on 03/07/2017.
 */

public class MySingleton extends BaseActivity {







    String[] SharedPrefNames = {"PatientProfile", "Conditions", "Allergies", "Medications", "Immunizations", "Procedures", "Insurance", "Corporate", "Consultations"};


    public static Context Contextt_;
    private Double ScreenSize;
    private String claimNo = "";

    public void setScreenSize(Double SC) {
        ScreenSize = SC;
    }

    public int getWidth() {
        if (ScreenSize > 4.3)
            return 600;
        else
            return 480;
    }

    public int getHeight() {
        if (ScreenSize > 4.3)
            return 380;
        else
            return 280;
    }

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String c) {
        claimNo = c;
    }

    /*public void logout(Context c) {
        MySingleton.getInstance().MainList.clear();
        getSinchServiceInterface().stopClient(c);
        //startActivity(new Intent(_MainActivity.this, _Splash.class));
    }*/

    private String FreeReward = "";
    private static final MySingleton ourInstance = new MySingleton();
    private boolean firstTime = true;
    private boolean MainAvailable = false;
    boolean isGeneralProfileComplete = false;
    boolean isInsuranceProductAvailable = false;
    boolean isCorporatePanelAvailable = false;
    boolean lang=false;
    private String mobileNo = "";
    public String fn="";

    boolean profile =false;

    public ArrayList<JSONArray> ArrayFamily = new ArrayList<JSONArray>();

    public ArrayList<JSONArray> MainList = new ArrayList<JSONArray>();
    public JSONArray CallList = null;

    private double DoctorRating = -1;
    private String DoctorStatus = "1";

    private String ActivityName = "";
    private SweetAlertDialog pDialog = null;
    private String recipient = "";

    Toolbar toolbar;

    boolean inCall = false;

    public static MySingleton getInstance() {
        return ourInstance;
    }

    public static MySingleton getInstance(Context c) {
        Contextt_ = c;
        return ourInstance;
    }

    private MySingleton() {
    }

    public boolean getBoolean() {
        return firstTime;
    }

    public void setBoolean(Boolean ft) {
        firstTime = ft;
    }

    public boolean getIncall() {
        return inCall;
    }

    public void setLang(Boolean lan){

        lang=lan;
    }
    public boolean getLang(){

        return lang;
    }


    public void setPro(Boolean pro){

        profile=pro;
    }
    public boolean getPro(){

        return profile;
    }


    public void setInCall(Boolean ic) {
        inCall = ic;
    }

    public boolean getGprofile() {
        return isGeneralProfileComplete;
    }

    public void setGprofile(Boolean igpc) {
        isGeneralProfileComplete = igpc;
    }

    public boolean getIproduct() {
        return isInsuranceProductAvailable;
    }

    public void setIproduct(Boolean iipa) {
        isInsuranceProductAvailable = iipa;
    }

    public boolean getCpanel() {
        return isCorporatePanelAvailable;
    }

    public void setCpanel(Boolean icpa) {
        isCorporatePanelAvailable = icpa;
    }

    public boolean getMainAvailable() {
        return MainAvailable;
    }

    public void setMainAvailable(Boolean ma) {
        MainAvailable = ma;
    }

    public String getUserId() {
        return mobileNo;
    }



    public void setUserId(String mobileNo) {
        this.mobileNo = mobileNo;
    }



    public String getFreeReward() {
        return FreeReward;
    }

    public void setFreeReward(String fr) {
        FreeReward = fr;
    }

    public JSONArray getArray(int index) {
        try {
            return MainList.get(index);
        } catch (IndexOutOfBoundsException exception) {
            //showDialog(Contextt_);
            return null;
        }
    }

    public void updateArray(int index, JSONArray list) {
        try {
            MainList.set(index, list);
            addSharedPrefEntry(SharedPrefNames[index],list.toString());

        } catch (IndexOutOfBoundsException e) {
            MainList.add(list);
        }
    }

    public void setArray(JSONArray list) {

        MainList.add(list);

    }

    public void updateFamilyArray(int index, JSONArray list) {
        try {
            ArrayFamily.set(index, list);
            //addSharedPrefEntry(SharedPrefNames[index],list.toString());

        } catch (IndexOutOfBoundsException e) {
            ArrayFamily.add(list);
        }
    }

    public void setFamilyArray(JSONArray list) {
        ArrayFamily.add(list);

    }

    public JSONArray getFamilyArray(int index) {
        try {
            return ArrayFamily.get(index);
        } catch (IndexOutOfBoundsException exception) {
            //showDialog(Contextt_);
            return null;
        }
    }

    public JSONArray getCallSummaryArray(int index) {
        try {
            try {
                return CallList.getJSONArray(index);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IndexOutOfBoundsException exception) {
            //showDialog(Contextt_);
            return null;
        }
        return null;
    }

    public void setCallSummaryArray(JSONArray list) {
        CallList = list;
    }

    public double getDoctorRating() {
        return DoctorRating;
    }

    public void setDoctorRating(double rat) {
        this.DoctorRating = rat;
    }

    public String getDoctorStatus() {
        return DoctorStatus;
    }

    public void setDoctorStatus(String s) {
        this.DoctorStatus = s;
    }

    public void changeDoctorStatusInJsonArray(String name) {
        JSONArray MainArr = MainList.get(8);
        int id = 0;
        try {
            for (int i = 0; i < MainArr.length(); i++) {
                if (MainArr.getJSONArray(i).getString(3).equals(name))
                    MainArr.getJSONArray(i).put(8, "2");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getActivityName() {
        return ActivityName;
    }

    public void setActivityName(String AN) {
        ActivityName = AN;
    }

    public boolean isConnected(Context context) {
        Contextt_ = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            showDialog(context);
            return false;
        }
    }

    public boolean isConnectedButton() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnectedButton(Context c) {
        ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifiInfo != null && wifiInfo.isConnected()) || (mobileInfo != null && mobileInfo.isConnected())) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConnectedNetwork(Context c) {
        //showLoadingPopup(c,"Please Wait");
        GetNetwork async = new GetNetwork(c, new AsyncResponse() {
            @Override
            public void processFinish(Object result) {

            }
        }, "");
        return false;
    }

    public void showLoadingPopup(Context con, String a) {
        pDialog = new SweetAlertDialog(con, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
        pDialog.setTitleText(a);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissLoadingPopup() {
        if (pDialog != null)
            try {
                pDialog.dismiss();
            } catch (IllegalArgumentException e) {
                return;
            }
    }

    private void showDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Connect to a network or quit")
                .setCancelable(false)
                .setPositiveButton("Connect to WIFI/Refresh", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!isConnected(context))
                            context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Quit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((Activity) context).finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void editPopup(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Result!")
                .setContentText("Edited Successfully")
                .show();
    }

    public void addPopup(Context context) {
        new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Result!")
                .setContentText("Added Successfully")
                .show();
    }

    public void setToolbar(Toolbar t) {
        toolbar = t;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public View getGroupView(ExpandableListView listView, int groupPosition) {
        long packedPosition = ExpandableListView.getPackedPositionForGroup(groupPosition);
        int flatPosition = listView.getFlatListPosition(groupPosition);
        int first = listView.getFirstVisiblePosition();
        return listView.getChildAt(groupPosition);
    }

    public String getNameFromUserName(String a) {
        try {
            JSONArray main = MainList.get(9);
            JSONArray item = null;
            String name = "";
            for (int i = 0; i < main.length(); i++) {
                try {
                    item = (JSONArray) main.get(i);
                    if (item.get(3).toString().toLowerCase().equals(a.toLowerCase())) {
                        name = item.get(0) + " " + item.get(1);
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return name;
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }

    public String getName(){
        try {
            JSONArray main = MainList.get(9);
            JSONArray item = null;
            String fname = "";
            for (int i = 0; i < main.length(); i++) {
                try {
                    item = (JSONArray) main.get(i);
                        fname = item.get(0) + " " + item.get(1);
                        break;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return fname;
        } catch (IndexOutOfBoundsException e) {
            return "";
        }
    }


    public boolean checkRecipient(String name) {
        if (recipient.equals(name))
            return true;
        else
            return false;
    }

    public void setRecipient(String name) {
        recipient = name;
    }

    public JSONArray getSharedPrefEntry(String name) {
        SharedPreferences sharedpref = Contextt_.getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        String s = sharedpref.getString(name, "");
        try {
            JSONArray arr = new JSONArray(s);
            return arr;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addSharedPrefEntry(String name, String value) {
        SharedPreferences sharedpref = Contextt_.getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString(name, value);
        editor.apply();
    }

    /*public void registerCallback(CallbackManager callbackManager, FacebookCallback<LoginResult> facebookCallback) {

    }*/
}



