package com.webdoc.cards;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.R;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CardItem> mData;
    private static List<CardItem> mData_;
    private float mBaseElevation;
    Resources a;
    Context ctx;

    public CardPagerAdapter(Resources a, Context ctx) {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
        this.a = a;
        this.ctx = ctx;
    }

    public void addCardItem(CardItem item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public CardItem getCardItemAt(int position) {
        return mData.get(position);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext())
                .inflate(R.layout.___healthcard_layout, container, false);

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
        container.addView(view);
        bind(mData.get(position), view, position);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);

        mData_ = mData;
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    private void bind(final CardItem item, final View view, final int p) {
        ((CardView) view).setCardBackgroundColor(Color.TRANSPARENT);
        final RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rmain);

        if (p == 0) {
            relativeLayout.setBackground(a.getDrawable(R.drawable.hc1));
        } else if (p == 1)
            relativeLayout.setBackground(a.getDrawable(R.drawable.hc2));
        else if (p == 2)
            relativeLayout.setBackground(a.getDrawable(R.drawable.hc3));
        else if (p == 3)
            relativeLayout.setBackground(a.getDrawable(R.drawable.hc4));

        view.setTag(p);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(ctx, view.getBackground() + "", 0).show();
                ImageViewAnimatedChange(((CardView) view), p);
            }
        });

        view.findViewById(R.id.b1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewb) {
                String nameCard = "";
                if (view.getTag() == (Object) 0) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Silver");
                    i_.putExtra("TYPE", "voucher_payment");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Silver Card";
                } else if (view.getTag() == (Object) 1) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Gold");
                    i_.putExtra("TYPE", "voucher_payment");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Gold Card";
                } else if (view.getTag() == (Object) 2) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Platinum");
                    i_.putExtra("TYPE", "voucher_payment");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Platinum Card";
                }
                else if (view.getTag() == (Object) 3) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Befikar Card");
                    i_.putExtra("TYPE", "voucher_payment");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Befikar Card";
                }
                Toast.makeText(ctx, nameCard, Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.b2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewb) {
                String nameCard = "";
                if (view.getTag() == (Object) 0) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Silver");
                    i_.putExtra("TYPE", "credit_debit");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Silver Card";
                } else if (view.getTag() == (Object) 1) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Gold");
                    i_.putExtra("TYPE", "credit_debit");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Gold Card";
                } else if (view.getTag() == (Object) 2) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Platinum");
                    i_.putExtra("TYPE", "credit_debit");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Platinum Card";
                }else if (view.getTag() == (Object) 3) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Befikar Card");
                    i_.putExtra("TYPE", "credit_debit");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Befikar Card";
                }
                Toast.makeText(ctx, nameCard, Toast.LENGTH_SHORT).show();
            }
        });

        view.findViewById(R.id.b3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewb) {
                String nameCard = "";
                if (view.getTag() == (Object) 0) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Silver");
                    i_.putExtra("TYPE", "mobile_wallet");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Silver Card";
                } else if (view.getTag() == (Object) 1) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Gold");
                    i_.putExtra("TYPE", "mobile_wallet");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Gold Card";
                } else if (view.getTag() == (Object) 2) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Platinum");
                    i_.putExtra("TYPE", "mobile_wallet");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Platinum Card";
                }else if (view.getTag() == (Object) 3) {
                    Intent i_ = new Intent(new Intent(ctx, __PopupPayment.class));
                    i_.putExtra("PRODUCT", "Befikar Card");
                    i_.putExtra("TYPE", "mobile_wallet");
                    i_.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    ctx.startActivity(i_);
                    nameCard = "Befikar Card";
                }
                Toast.makeText(ctx, nameCard, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public static void ImageViewAnimatedChange(final CardView card, final int p) {
        final RelativeLayout outer = (RelativeLayout) card.findViewById(R.id.rmain);
        final RelativeLayout inner = (RelativeLayout) card.findViewById(R.id.inner);
        System.out.println("??AKSJAKJSKJASKJ******" + outer.getAlpha());
        if (!(outer.getAlpha() <= 0.2f)) {  /*outer.getVisibility() == View.VISIBLE*/
            hideView_o(outer);
            card.setCardBackgroundColor(Color.WHITE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showView_i(inner);
                    inner.setVisibility(View.VISIBLE);
                    mData_.get(p).setPMShown(true);
                }
            }, 1000);

        } else {
            hideView_i(inner, card);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    inner.setVisibility(View.GONE);
                    card.setCardBackgroundColor(Color.TRANSPARENT);
                    showView_o(outer, card);
                    mData_.get(p).setPMShown(false);
                }
            }, 1000);
        }

    }

    public static void showView_o(final View root, final CardView card) {
        root.setVisibility(View.VISIBLE);
        root.animate().alpha(1.0f).setDuration(1000);
    }

    public static void showView_i(final View root) {
        //root.animate().alpha(1.0f).setDuration(2500);
        root.findViewById(R.id.top).animate().alpha(1.0f).setDuration(500);
        root.findViewById(R.id.b1).animate().alpha(1.0f).setDuration(1000);
        root.findViewById(R.id.b2).animate().alpha(1.0f).setDuration(1500);
        root.findViewById(R.id.b3).animate().alpha(1.0f).setDuration(2000);
    }

    public static void hideView_o(final View root) {
        root.animate().alpha(0.2f).setDuration(1000).withEndAction(new Runnable() {
            @Override
            public void run() {
                //root.setBackgroundColor(Color.parseColor("#CCFF0000"));
                //root.getBackground().setColorFilter();
            }
        });
    }

    public static void hideView_i(final View root, final CardView card) {
        //root.animate().alpha(1.0f).setDuration(2500);
        root.findViewById(R.id.b3).animate().alpha(0.0f).setDuration(500);
        root.findViewById(R.id.b2).animate().alpha(0.0f).setDuration(1000);
        root.findViewById(R.id.b1).animate().alpha(0.0f).setDuration(1500);
        root.findViewById(R.id.top).animate().alpha(0.0f).setDuration(2000);
    }
}
