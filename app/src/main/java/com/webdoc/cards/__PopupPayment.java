package com.webdoc.cards;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.MySingleton;
import com.webdoc.R;

public class __PopupPayment extends AppCompatActivity {

    private WebView wv1;

    private static final String Silver_VoucherPayment_Jazz = "https://portal.webdoc.com.pk/transection/HostedOTC.php?desc=Silver&brn=abc&amount=22500&email=";
    private static final String Gold_VoucherPayment_Jazz = "https://portal.webdoc.com.pk/transection/HostedOTC.php?desc=Gold&brn=abc&amount=35000&email=";
    private static final String Platinum_VoucherPayment_Jazz = "https://portal.webdoc.com.pk/transection/HostedOTC.php?desc=Platinum&brn=abc&amount=50000&email=";
    private static final String Befikar_VoucherPayment_Jazz = "https://portal.webdoc.com.pk/transection/HostedOTC.php?desc=Befikar&brn=abc&amount=75000&email=";


    private static final String Silver_CreditDebit_Jazz = "http://portal.webdoc.com.pk/transection/HostedCD.php?desc=Platinum&brn=abc&amount=22500&email=";
    private static final String Gold_CreditDebit_Jazz = "http://portal.webdoc.com.pk/transection/HostedCD.php?desc=Gold&brn=abc&amount=35000&email=";
    private static final String Platinum_CreditDebit_Jazz = "http://portal.webdoc.com.pk/transection/HostedCD.php?desc=Platinum&brn=abc&amount=50000&email=";
    private static final String Befikar_CreditDebit_Jazz = "http://portal.webdoc.com.pk/transection/HostedCD.php?desc=Befikar&brn=abc&amount=750000&email=";


    private static final String Silver_MobileWallet_Jazz = "http://portal.webdoc.com.pk/transection/HostedMW.php?desc=Silver&brn=abc&amount=22500&email=";
    private static final String Gold_MobileWallet_Jazz = "http://portal.webdoc.com.pk/transection/HostedMW.php?desc=Gold&brn=abc&amount=35000&email=";
    private static final String Platinum_MobileWallet_Jazz = "http://portal.webdoc.com.pk/transection/HostedMW.php?desc=Platinum&brn=abc&amount=50000&email=";
    private static final String Befikar_MobileWallet_Jazz = "http://portal.webdoc.com.pk/transection/HostedMW.php?desc=Befikar&brn=abc&amount=75000&email=";

    private static final String[] Jazz_Links = {
            Silver_VoucherPayment_Jazz, Silver_CreditDebit_Jazz, Silver_MobileWallet_Jazz,
            Gold_VoucherPayment_Jazz,Gold_CreditDebit_Jazz,Gold_MobileWallet_Jazz,
            Platinum_VoucherPayment_Jazz,Platinum_CreditDebit_Jazz,Platinum_MobileWallet_Jazz,
            Befikar_VoucherPayment_Jazz,Befikar_CreditDebit_Jazz,Befikar_MobileWallet_Jazz
           };


    private static final String Silver_VoucherPayment_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/OTC.php?amount=225&email=";
    private static final String Gold_VoucherPayment_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/OTC.php?amount=350&email=";
    private static final String Platinum_VoucherPayment_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/OTC.php?amount=500&email=";
    private static final String Befikar_VoucherPayment_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/OTC.php?amount=750&email=";

    private static final String Silver_CreditDebit_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/pay.php?amount=225.0&email="; //+"&msisdn=03145362496";
    private static final String Gold_CreditDebit_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/pay.php?amount=350.0&email="; //+"&msisdn=03145362496";
    private static final String Platinum_CreditDebit_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/pay.php?amount=500.0&email="; //+"&msisdn=03145362496";
    private static final String Befikar_CreditDebit_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/pay.php?amount=750.0&email="; //+"&msisdn=03145362496";


    private static final String Silver_MobileWallet_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/MA.php?amount=225&email=";
    private static final String Gold_MobileWallet_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/MA.php?amount=350&email=";
    private static final String Platinum_MobileWallet_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/MA.php?amount=500&email=";
    private static final String Befikar_MobileWallet_EasyPaisa = "https://portal.webdoc.com.pk/transection/easypaisa/MA.php?amount=750&email=";

    private static final String[] EasyPaisa_Links = {
            Silver_VoucherPayment_EasyPaisa, Silver_CreditDebit_EasyPaisa, Silver_MobileWallet_EasyPaisa,
            Gold_VoucherPayment_EasyPaisa,Gold_CreditDebit_EasyPaisa,Gold_MobileWallet_EasyPaisa,
            Platinum_VoucherPayment_EasyPaisa,Platinum_CreditDebit_EasyPaisa,Platinum_MobileWallet_EasyPaisa,
            Befikar_VoucherPayment_EasyPaisa,Befikar_CreditDebit_EasyPaisa,Befikar_MobileWallet_EasyPaisa
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.__popup_payment);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live

//////////////////////////////////////////////////////////////////////////////

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .9), (int) (height * .9));

        wv1 = (WebView) findViewById(R.id.webview);
        wv1.setWebViewClient(new MyBrowser());

        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        //wv1.loadUrl("http://portal.webdoc.com.pk/Home/TermsAndConditions");

        wv1.addJavascriptInterface(new MyJavaScriptInterface(this), "HtmlViewer");

        wv1.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                /*wv1.loadUrl("javascript:window.HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");*/
            }
        });
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    class MyJavaScriptInterface {

        private Context ctx;

        MyJavaScriptInterface(Context ctx) {
            this.ctx = ctx;
        }

        @JavascriptInterface
        public void showHTML(String html) {
            new AlertDialog.Builder(ctx).setTitle("HTML").setMessage(html)
                    .setPositiveButton(android.R.string.ok, null).setCancelable(false).create().show();
        }

    }

    public void clickJazz(View view) {
        int i = 0, j = 0;
        String product = getIntent().getStringExtra("PRODUCT");
        if (product.equals("Silver"))
            i = 0;
        else if (product.equals("Gold"))
            i = 3;
        else if (product.equals("Platinum"))
            i = 6;
        else if (product.equals("Befikar"))
            i = 9;


        String type = getIntent().getStringExtra("TYPE");
        if (type.equals("voucher_payment"))
            j = 0;
        else if (type.equals("credit_debit"))
            j = 1;
        else if (type.equals("mobile_wallet"))
            j = 2;

        findViewById(R.id.layout_buttons).setVisibility(View.GONE);
        findViewById(R.id.layout_webview).setVisibility(View.VISIBLE);

        String link = Jazz_Links[i + j] + MySingleton.getInstance().getUserId();
        wv1.loadUrl(link);
    }

 /*   public void clickEasypaisa(View view) {
        int i = 0, j = 0;
        String product = getIntent().getStringExtra("PRODUCT");
        if (product.equals("Silver"))
            i = 0;
        else if (product.equals("Gold"))
            i = 3;
        else if (product.equals("Platinum"))
            i = 6;
        else if (product.equals("Befikar"))
            i = 9;


        String type = getIntent().getStringExtra("TYPE");
        if (type.equals("voucher_payment"))
            j = 0;
        else if (type.equals("credit_debit"))
            j = 1;
        else if (type.equals("mobile_wallet"))
            j = 2;

        findViewById(R.id.layout_buttons).setVisibility(View.GONE);
        findViewById(R.id.layout_webview).setVisibility(View.VISIBLE);

        String link = EasyPaisa_Links[i + j] + MySingleton.getInstance().getUserId();
        wv1.loadUrl(link);
    }*/
}
