package com.webdoc.cards;


public class CardItem {

    private int mTextResource;
    private int mTitleResource;
    private boolean step2 = false;

    public CardItem(int title, int text) {
        mTitleResource = title;
        mTextResource = text;
    }

    public int getText() {
        return mTextResource;
    }

    public int getTitle() {
        return mTitleResource;
    }

    public void setPMShown(Boolean s) {
        step2 = s;
    }

    public boolean getPMShown() {
        return step2;
    }
}
