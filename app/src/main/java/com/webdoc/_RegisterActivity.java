package com.webdoc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.sinch.android.rtc.SinchError;
import com.webdoc.mainPage.MainPage;
import com.webdoc.messenger.BaseActivity;
import com.webdoc.messenger.SinchService;

import belka.us.androidtoggleswitch.widgets.ToggleSwitch;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class _RegisterActivity extends BaseActivity implements SinchService.StartFailedListener {

    String list[] = {"- Select Your Product -", "Muhafiz", "Sehat Sahara", "Corporate"};
    Spinner sp;
    CheckBox cb;

    ToggleSwitch toggleSwitch;

    String mobileNo, password;

    SweetAlertDialog pDialog;
    ToggleSwitch lang;

    @Override
    protected void onServiceConnected() {
        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().setStartListener(_RegisterActivity.this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeractivity);
        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////


        String Path2font = "DroidNaskh-Regular.ttf";
// Give label to TextView object defined in layout
// Load the Font and define typeface accordingly
        final Typeface tf = Typeface.createFromAsset(getAssets(), Path2font);
        final InputFilter[] filter11 = new InputFilter[]{new InputFilter.LengthFilter(11)};
        final InputFilter[] filter99 = new InputFilter[]{new InputFilter.LengthFilter(99)};

        lang = (ToggleSwitch) findViewById(R.id.lang);
        lang.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {

                if (position == 0) {
                    ((TextView) findViewById(R.id.regtext)).setText("Register to WebDoc");

                    ((EditText) findViewById(R.id.email)).setHint("Mobile Number");
                    ((EditText) findViewById(R.id.password)).setGravity(Gravity.LEFT);

                    ((EditText) findViewById(R.id.password)).setHint("Password");
                    ((EditText) findViewById(R.id.confirmpassword)).setGravity(Gravity.LEFT);

                    ((EditText) findViewById(R.id.confirmpassword)).setHint("Confirm Password");
                    ((Button) findViewById(R.id.register)).setText("Register");
                    ((TextView) findViewById(R.id.extext)).setText("Esisting user");
                    ((Button) findViewById(R.id.register_to_sign_in)).setText("Login here");
                    ((Button) findViewById(R.id.TandC)).setText("Terms and Conditions");
                    ((CheckBox) findViewById(R.id.chkWindows)).setText("I agree to WebDoc");

                    if (toggleSwitch.getCheckedTogglePosition() == 0) {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("Phone Number");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    } else {

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("Email Address");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }


                } else {
                    ((TextView) findViewById(R.id.regtext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.regtext)).setText("رجسٹر کریں");
                    ((EditText) findViewById(R.id.email)).setTypeface(tf);
                    ((EditText) findViewById(R.id.email)).setHint("موبائل فون نمبر");
                    ((EditText) findViewById(R.id.password)).setTypeface(tf);
                    ((EditText) findViewById(R.id.password)).setGravity(Gravity.RIGHT);
                    ((EditText) findViewById(R.id.password)).setHint("پاس ورڈ");
                    ((EditText) findViewById(R.id.confirmpassword)).setTypeface(tf);
                    ((EditText) findViewById(R.id.confirmpassword)).setGravity(Gravity.RIGHT);

                    ((EditText) findViewById(R.id.confirmpassword)).setHint("پاس ورڈ کی تصدیق کریں");
                    ((Button) findViewById(R.id.register)).setTypeface(tf);
                    ((Button) findViewById(R.id.register)).setText("رجسٹر");
                    ((TextView) findViewById(R.id.extext)).setTypeface(tf);
                    ((TextView) findViewById(R.id.extext)).setText("موجودہ صارف ");
                    ((Button) findViewById(R.id.register_to_sign_in)).setTypeface(tf);
                    ((Button) findViewById(R.id.register_to_sign_in)).setText("لاگ ان کریں ");
                    ((Button) findViewById(R.id.TandC)).setTypeface(tf);
                    ((Button) findViewById(R.id.TandC)).setText("شرائط و ضوابط");
                    ((CheckBox) findViewById(R.id.chkWindows)).setTypeface(tf);
                    ((CheckBox) findViewById(R.id.chkWindows)).setText("سے اتفاق کرتا ہوں");

                    if (toggleSwitch.getCheckedTogglePosition() == 0) {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("موبائل فون نمبر");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    } else {


                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("ای میل اڈریس");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }


                }
            }
        });


        pDialog = new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#cb3439"));
        pDialog.setTitleText("Logging In - Please Wait");
        pDialog.setCancelable(false);

        Window w = getWindow();
        w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        //sp = (Spinner) findViewById(R.id.spinner);
        toggleSwitch = (ToggleSwitch) findViewById(R.id.log);
        toggleSwitch.setAlpha(0.0f);

       /* sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 3) {*/
                    /*toggleSwitch.animate().alpha(1.0f).setDuration(1500);
                    //findViewById(R.id.ivLogo).animate().translationY(-80).setDuration(1500);
                    findViewById(R.id.moveAble).animate().translationY(125).setDuration(1500);
                    //toggleSwitch.animate().translationY(-80).setDuration(1500);
                    toggleSwitch.setVisibility(View.VISIBLE);*/

        findViewById(R.id.regtext).animate().alpha(0.0f).setDuration(1500);
        toggleSwitch.animate().alpha(0.0f).setDuration(1500);
        toggleSwitch.setVisibility(View.VISIBLE);
        lang.setAlpha(0.0f);
        lang.animate().alpha(0.0f).setDuration(1500);
        lang.setVisibility(View.VISIBLE);



                    /*toggleSwitch.animate().alpha(0.0f).setDuration(1500).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            toggleSwitch.setVisibility(View.GONE);
                            toggleSwitch.setCheckedTogglePosition(0);
                        }
                    });
                    //findViewById(R.id.textView3).animate().translationY(0).setDuration(1500);
                    findViewById(R.id.moveAble).animate().translationY(0).setDuration(1500);
                    toggleSwitch.animate().translationY(0).setDuration(1500);*/

        toggleSwitch.animate().alpha(1f).setDuration(1500);
        lang.animate().alpha(1f).setDuration(1500);
        findViewById(R.id.regtext).animate().alpha(1.0f).setDuration(1500);


            /*    }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        cb = (CheckBox) findViewById(R.id.chkWindows);

       /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, list);
        sp.setAdapter(adapter);*/

        /*final InputFilter[] filter11 = new InputFilter[]{new InputFilter.LengthFilter(11)};
        final InputFilter[] filter99 = new InputFilter[]{new InputFilter.LengthFilter(99)};*/

        toggleSwitch = (ToggleSwitch) findViewById(R.id.log);

        toggleSwitch.setOnToggleSwitchChangeListener(new ToggleSwitch.OnToggleSwitchChangeListener() {

            @Override
            public void onToggleSwitchChangeListener(int position, boolean isChecked) {
                if (position == 0) {
                    if (lang.getCheckedTogglePosition() == 0) {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("Phone Number");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    } else {
                        ((EditText) findViewById(R.id.email)).setText("");
                        ((EditText) findViewById(R.id.email)).setInputType(InputType.TYPE_CLASS_PHONE);
                        ((EditText) findViewById(R.id.email)).setHint("موبائل فون نمبر");
                        ((EditText) findViewById(R.id.email)).setFilters(filter11);
                    }


                } else {
                    if (lang.getCheckedTogglePosition() == 0) {

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("Email Address");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    } else {

                        ((EditText) findViewById(R.id.email)).setText("");
                        ((TextView) findViewById(R.id.email)).setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                        ((EditText) findViewById(R.id.email)).setHint("ای میل اڈریس");
                        ((EditText) findViewById(R.id.email)).setFilters(filter99);
                    }

                }
            }
        });
    }

    public void openLogin(View view) {
        MySingleton.getInstance().setBoolean(false);

        startActivity(new Intent(this, _Splash.class));
    }

    public void openTandC(View view) {
        startActivity(new Intent(this, __PopupTandC.class));
    }

    public void registerPatient(View view) {
        if (!cb.isChecked()) {
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("Please Accept The Terms And Conditions")
                    .show();
            return;
        } else {
            if ((((EditText) findViewById(R.id.password)).getText() + "").equals(((EditText) findViewById(R.id.confirmpassword)).getText() + "")) {

                if ((((EditText) findViewById(R.id.email)).getText() + "").equals("")) {
                    new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No username")
                            .show();
                    return;
                }

                if ((((EditText) findViewById(R.id.password)).getText() + "").length() < 7) {
                    new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("Password length must be greater than 7")
                            .show();
                    return;
                }
                MySingleton.getInstance().showLoadingPopup(this, "Registering - Please Wait");
                String ans = "";
                //String type = sp.getSelectedItem().toString();
                mobileNo = ((EditText) findViewById(R.id.email)).getText() + "";
                password = ((EditText) findViewById(R.id.password)).getText() + "";

                /*if (type.equals("- Select Your Product -")) {
                    MySingleton.getInstance().dismissLoadingPopup();
                    new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Something went wrong!")
                            .setContentText("No Registration Product Selected")
                            .show();
                    return;
                }*/
               /* if (type.equals("Muhafiz")) {
                    GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            String ans = result + "";
                            dataAvailable(ans);
                        }

                    }, "RegisterMuhafizPatient/" + mobileNo + "/" + password + "__" + "1");
                    asyncTask.execute();
                } else if (type.equals("Sehat Sahara")) {
                    GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                        @Override
                        public void processFinish(Object result) {
                            String ans = result + "";
                            dataAvailable(ans);
                        }
                    }, "RegisterSehatSaharaPatient/" + mobileNo + "/" + password + "__" + "1");
                    asyncTask.execute();
                }*/
//               if (type.equals("Corporate")) {
                if (toggleSwitch.getCheckedTogglePosition() == 0) {
                    mobileNo += "@webdoc.com.pk";
                } else {
                }
                GetProfile asyncTask = new GetProfile(this, new AsyncResponse() {
                    @Override
                    public void processFinish(Object result) {
                        String ans = result + "";
                        dataAvailable(ans);
                    }
                }, "/RegisterPatient/" + mobileNo + "/" + password + "__" + "1");
                asyncTask.execute();
            }
        } /*else
                new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Something went wrong!")
                        .setContentText("Password Mismatch")
                        .show();
*/
    }


    public void saveInfo(String name, String pass) {
        SharedPreferences sharedpref = getSharedPreferences("_LoginInfo_", Context.MODE_PRIVATE);
        sharedpref.edit().clear().commit();
        SharedPreferences.Editor editor = sharedpref.edit();
        editor.putString("UserName", name);
        editor.putString("Password", pass);
        editor.apply();
    }

    public void dataAvailable(String ans) {
        if (ans.equals("\"0\"")) {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Failed")
                    .setContentText("Please check details")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            finish();
                        }
                    })
                    .show();
        }
        if (ans.equals("\"1\"")) {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Registered Successfully")
                    .setContentText("Please Proceed Forward")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();

                            if (!getSinchServiceInterface().isStarted()) {
                                getSinchServiceInterface().startClient(mobileNo + "@webdoc.com.pk");
                            }
                            pDialog.show();

                            //MySingleton.getInstance().setUserId(mobileNo + "@webdoc.com.pk");
                            //saveInfo(mobileNo + "@webdoc.com.pk", password);
                            //startActivity(new Intent(getApplicationContext(), _MainActivity.class));
                            //pDialog.dismiss();
                            //finish();
                        }
                    })
                    .show();
        } else if (ans.equals("\"2\"")) {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Registered Successfully")
                    .setContentText("Please Complete Your Profile")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
//                            if (sp.getSelectedItemPosition() != 3) {
                                /*if (!getSinchServiceInterface().isStarted()) {
                                    getSinchServiceInterface().startClient(mobileNo + "@webdoc.com.pk");
                                }
                                pDialog.show();

                                //saveInfo(mobileNo + "@webdoc.com.pk", password);
                                //MySingleton.getInstance().setUserId(mobileNo + "@webdoc.com.pk");
                            } else {*/
                            if (!getSinchServiceInterface().isStarted()) {
                                getSinchServiceInterface().startClient(mobileNo);
                            }
                            pDialog.show();

                            //saveInfo(mobileNo, password);
                            //MySingleton.getInstance().setUserId(mobileNo);
                        }

                        //startActivity(new Intent(getApplicationContext(), _MainActivity.class));
                        //pDialog.dismiss();
                        //finish();

                    })
                    .show();
        } else if (ans.equals("\"3\"")) {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Already Registered")
                    .setContentText("Please Login")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();
                            finish();
                        }
                    })
                    .show();
        } else {
            MySingleton.getInstance().dismissLoadingPopup();
            new SweetAlertDialog(_RegisterActivity.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Something went wrong!")
                    .setContentText("There was an error while processing your request. Please check your request and try again. \n" +
                            "If the problem persists, please contact us at support@webdoc.com.pk")
                    .show();
        }
    }

    @Override
    public void onStartFailed(SinchError error) {
        if (pDialog != null)
            pDialog.dismiss();
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStarted() {
        if (!mobileNo.contains("@")) {
            mobileNo += "@webdoc.com.pk";
        }
        MySingleton.getInstance().setUserId(mobileNo);
        saveInfo(mobileNo, password);
        if (pDialog != null)
            pDialog.dismiss();
        startActivity(new Intent(getApplicationContext(), MainPage.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        finish();
    }
}
