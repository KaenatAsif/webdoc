package com.webdoc.spotLight;

/**
 * Created by alius on 2/21/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.batch.android.Batch;
import com.batch.android.Config;
import com.webdoc.AsyncResponse;
import com.wooplr.spotlight.SpotlightConfig;
import com.wooplr.spotlight.SpotlightView.Builder;
import com.wooplr.spotlight.prefs.PreferencesManager;
import com.wooplr.spotlight.utils.SpotlightListener;

import java.util.LinkedList;
import java.util.Queue;

public class SpotlightSequence {
    AsyncResponse delgate;

    private Activity activity;
    private SpotlightConfig config;
    private Queue<Builder> queue;
    private static SpotlightSequence instance;
    private final String TAG = "Tour Sequence";

    private String usageId;

    private SpotlightSequence(Activity activity, SpotlightConfig config) {
        Log.d("Tour Sequence", "NEW TOUR_SEQUENCE INSTANCE");
        this.activity = activity;
        this.setConfig(config);
        this.queue = new LinkedList();

        /////////////////////////////////////////////////////////////////////////////////
        /// Batch service : push notification
        /// Batch service : push notification
        Batch.Push.setGCMSenderId("554280630926");

        // TODO : switch to live Batch Api Key before shipping
        Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // devloppement
        // Batch.setConfig(new Config("5A9F7B9DC1B0CB4ADE782215BFA924")); // live
//////////////////////////////////////////////////////////////////////////////

    }

    public static SpotlightSequence getInstance(Activity activity, SpotlightConfig config) {
        if (instance == null) {
            instance = new SpotlightSequence(activity, config);
        }

        return instance;
    }

    public SpotlightSequence addSpotlight(View target, String title, String subtitle, String usageId) {
        this.usageId = usageId;
        Log.d("Tour Sequence", "Adding " + usageId);
        Builder builder = (new Builder(this.activity)).setConfiguration(this.config).headingTvText(title).usageId(usageId).subHeadingTvText(subtitle).target(target).setListener(new SpotlightListener() {
            public void onUserClicked(String s) {
                SpotlightSequence.this.playNext();
            }
        }).enableDismissAfterShown(true);
        this.queue.add(builder);
        return instance;
    }

    public SpotlightSequence addSpotlight(@NonNull View target, int titleResId, int subTitleResId, String usageId) {
        String title = this.activity.getString(titleResId);
        String subtitle = this.activity.getString(subTitleResId);
        Builder builder = (new Builder(this.activity)).setConfiguration(this.config).headingTvText(title).usageId(usageId).subHeadingTvText(subtitle).target(target).setListener(new SpotlightListener() {
            public void onUserClicked(String s) {
                SpotlightSequence.this.playNext();
            }
        }).enableDismissAfterShown(true);
        this.queue.add(builder);
        return instance;
    }

    public void startSequence(AsyncResponse a, Context c) {
        delgate = a;
        if (this.queue.isEmpty()) {
            Log.d("Tour Sequence", "EMPTY SEQUENCE");
        } else {
            if ((new PreferencesManager(c)).isDisplayed(usageId))
                delgate.processFinish("");
            else
                ((Builder) this.queue.poll()).show();
        }

    }

    private void resetTour() {
        instance = null;
        this.queue.clear();
        this.activity = null;
        this.config = null;
    }

    private void playNext() {
        Builder next = (Builder) this.queue.poll();
        if (next != null) {
            next.show().setReady(true);
        } else {
            delgate.processFinish("");
            Log.d("Tour Sequence", "END OF QUEUE");
            this.resetTour();
        }

    }

    public static void resetSpotlights(@NonNull Context context) {
        (new PreferencesManager(context)).resetAll();
    }

    private void setConfig(@Nullable SpotlightConfig config) {
        if (config == null) {
            config = new SpotlightConfig();
            config.setLineAndArcColor(Color.parseColor("#eb273f"));
            config.setDismissOnTouch(true);
            config.setMaskColor(Color.argb(240, 0, 0, 0));
            config.setHeadingTvColor(Color.parseColor("#eb273f"));
            config.setHeadingTvSize(32);
            config.setSubHeadingTvSize(16);
            config.setSubHeadingTvColor(Color.parseColor("#ffffff"));
            config.setPerformClick(false);
            config.setRevealAnimationEnabled(true);
            config.setLineAnimationDuration(400L);
        }

        this.config = config;
    }
}
